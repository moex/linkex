/*
 * Copyright (C) 2014-2019 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it2 and/or modify
 * it2 under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey;

import fr.inrialpes.exmo.linkkey.normalizers.AbstractNormalizer;
import fr.inrialpes.exmo.linkkey.normalizers.StringNormalizer;
import fr.inrialpes.exmo.linkkey.partialorder.PartialOrder;
import fr.inrialpes.exmo.linkkey.utils.*;
import it.unimi.dsi.fastutil.ints.*;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap.Entry;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LinkkeyDiscoveryAlgorithm {
    
    // -1 superscript
    public final static String INVERSE_SUFFIX="\u207B\u00B9";
    
    /*
    * Sets that contains the instances to be domain of linkeys
    */
    private IntSet subjectsDS1 = new IntOpenHashSet();
    private IntSet subjectsDS2 = new IntOpenHashSet();    

    /*
     * String index that assigns an int identifier to each property of DS1
     */
    private final StringToId pIdxDS1 = new StringToId(true);

    /*
     * String index that assigns an int identifier to each property of DS2
     */
    private final StringToId pIdxDS2 = new StringToId(true);

    /**
     * String index that assigns an int identifier to each URI (or blank node)
     */
    public final StringToId uriIdx = new StringToId();

    /**
     *  String index that assigns an int identifier to each value
     */
    private final StringToId valueIdx = new StringToId();

    /**
     * Indexes object to predicate to subjects (o->p->s index) The indexes in
     * the ArrayList are the ids of objects, the keys in the Map are the ids of
     * the predicates, and the elements of the sets are id of subjects.
     */
    private TripleIndex opsIndexDS1URI = new TripleIndex();
    private TripleIndex opsIndexDS1Value = new TripleIndex();

    private SetOPSIndex setopsIndexDS1URI = new SetOPSIndex();
    private SetOPSIndex setopsIndexDS1Value = new SetOPSIndex();

    private TripleIndex opsIndexDS2URI = new TripleIndex();
    private TripleIndex opsIndexDS2Value = new TripleIndex();

    private TripleIndex spoIndexDS1URI = new TripleIndex();
    private TripleIndex spoIndexDS1Value = new TripleIndex();

    private TripleIndex spoIndexDS2URI = new TripleIndex();
    private TripleIndex spoIndexDS2Value = new TripleIndex();

    //private MapSet<Integer, Integer> spToRemoveDS2 = new MapSet();
    private Int2ObjectMap<IntSet> spToRemoveDS2 = new Int2ObjectOpenHashMap<>();
    
    // stores the In and Eq pairs of each pair instances
    private DescriptionsSet descriptions;
    
    
    private Int2ObjectMap<IntSet> instancesTypesDS1;
    private Int2ObjectMap<IntSet> instancesTypesDS2;
    
    private int subjectsDS1Size;
    private int subjectsDS2Size;

    private final ExtractionConfig config;
 
    private PartialOrder order;
    private final AbstractNormalizer normalize = new StringNormalizer();
    
    private LongSet referenceLinks;
    
    private IntSet blankNodes;
    

    private LuceneIndex luceneIdx;
    
    public LinkkeyDiscoveryAlgorithm(ExtractionConfig config) {
        this.config=config;
        blankNodes = config.filterBlankNodes?new IntOpenHashSet():null;
        this.referenceLinks = new LongOpenHashSet();
        if (config.classes!=ExtractionConfig.NO_CLASSES) {
            instancesTypesDS1=new Int2ObjectOpenHashMap<>();
            instancesTypesDS2=new Int2ObjectOpenHashMap<>();
        }
        if (config.classes==ExtractionConfig.CLASSES_FULL) {
            order = PartialOrder.PROPERTY_PAIRS_AND_CLASS_EXP;
        }
        else {
            order = PartialOrder.PROPERTY_PAIRS;
        }
    }

    public void mergeSameAs(String uri1, String uri2) {
        uriIdx.mergeSameAs(uri1, uri2);
    }
    
    public void addReferenceLink(String s1, String s2) {
        int id1 = uriIdx.getId(s1);
        int id2 = uriIdx.getId(s2);
        if (id1 != -1 && id2 != -1) {
            referenceLinks.add(IntPair.encode(id1, id2));
        }
    }

    /**
     * Add the triple in the spoIndex (generatesEq is true) and in the opsIndex
     * (if generatesIn is true)
     *
     *
     * @param sId the subject identifier
     * @param pId the predicate identifier
     * @param oId the object identifier
     * @param spoIndex the subject to predicate to object index in which the triple (sId,pId,oId) will be added
     * @param opsIndex the object to predicate to subject index in which the triple (sId,pId,oId) will be added
     */
    private void indexTriple(int sId, int pId, int oId, TripleIndex spoIndex, TripleIndex opsIndex) {
        //subjects.add(sId);
        if (config.eq) {
            spoIndex.addTriple(sId, pId, oId);
        }
        if (config.in || config.compose>1) {
            opsIndex.addTriple(oId, pId, sId);
        }
    }
    
    public void addBlankNode(String n) {
        if (blankNodes!=null) blankNodes.add(uriIdx.getId(n));
    }

    public void addURITripleDS1(String s, String p, String o) {
        int sId = uriIdx.getId(s);
        int pId = pIdxDS1.getId(p);
        int oId = uriIdx.getId(o);

        // index type
        if (instancesTypesDS1!=null && p.equals(RDF.type.getURI())) {
            IntSet types = instancesTypesDS1.get(sId);
            if (types==null) {
                types = new IntOpenHashSet();
                instancesTypesDS1.put(sId, types);
            }
            types.add(oId);
        }
        if (    (config.typesDS1==null || (p.equals(RDF.type.getURI()) && config.typesDS1.contains(o)) ) &&  
                (config.typesPrefixDS1 ==null || (p.equals(RDF.type.getURI()) && o.startsWith(config.typesPrefixDS1))) ) {
            subjectsDS1.add(sId);
        }

        indexTriple(sId, pId, oId, spoIndexDS1URI, opsIndexDS1URI);

        
        //Add the inverse of property
        if (config.inverse) {
            int pIdInverse = pIdInverse=pIdxDS1.getId(p + INVERSE_SUFFIX);
            indexTriple(oId, pIdInverse, sId,spoIndexDS1URI,opsIndexDS1URI);
        }
    }

    public void addValueTripleDS1(String s, String p, Object o) {
        if (o instanceof String && ((String)o).length() == 0) {
            return;
        }
        int sId = uriIdx.getId(s);
        int pId = pIdxDS1.getId(p);
        int oId = valueIdx.getId(normalize.normalize(o));
//        String oNorm = normalize.normalize(o);
//        int oId = valueIdx.getIdIfExists(oNorm);
//        if (oId==-1 && luceneIdx!=null) {
//            luceneIdx.index(oNorm);
//            oId = valueIdx.getId(oNorm);
//        }

        if (config.typesDS1==null && config.typesPrefixDS1 ==null) {
            subjectsDS1.add(sId);
        }

        indexTriple(sId, pId, oId, spoIndexDS1Value, opsIndexDS1Value/*, psoIndexDS1*/);
    }



    public void addURITripleDS2(String s, String p, String o)  {
        int sId = uriIdx.getId(s);
        int pId = pIdxDS2.getId(p);
        int oId = uriIdx.getId(o);


        // index type
        if (instancesTypesDS2!=null && p.equals(RDF.type.getURI())) {
            IntSet types = instancesTypesDS2.get(sId);
            if (types==null) {
                types = new IntOpenHashSet();
                instancesTypesDS2.put(sId, types);
            }
            types.add(oId);
        }
        if (    (config.typesDS2==null || (p.equals(RDF.type.getURI()) && config.typesDS2.contains(o)) ) &&
                (config.typesPrefixDS2 ==null || (p.equals(RDF.type.getURI()) && o.startsWith(config.typesPrefixDS2))) ) {
            subjectsDS2.add(sId);
        }

        indexTriple(sId, pId, oId, spoIndexDS2URI, opsIndexDS2URI/*, psoIndexDS2*/);


        
        // Add the inverse of property
        if (config.inverse) {
            int pIdInverse = pIdxDS2.getId(p + INVERSE_SUFFIX);
            indexTriple(oId, pIdInverse, sId, spoIndexDS2URI, opsIndexDS2URI/*, psoIndexDS2*/);
        }
    }

    public void addValueTripleDS2(String s, String p, Object o)  {
        if (o instanceof String && ((String)o).length() == 0) {
            return;
        }
        int sId = uriIdx.getId(s);
        int pId = pIdxDS2.getId(p);
        
        String oNorm = normalize.normalize(o);
        int oId = valueIdx.getIdIfExists(oNorm);
//        if (oId==-1 && luceneIdx!=null) {
//            String res = luceneIdx.getClosest(oNorm);
//            if (res!=null) {
//                oId = valueIdx.getIdIfExists(res);
//                if (oId==-1) throw new RuntimeException("Pb with Lucene search");
//                System.err.println(res+" = "+oNorm);
//                valueIdx.mergeSameAs(res, oNorm);
//            }
//        }
        if (config.typesDS2==null && config.typesPrefixDS1 ==null) {
            subjectsDS2.add(sId);
        }
        // if the value of this triple is not in D1 
        // then the pair suject-property will be removed from eq
        if (oId > -1) {
            indexTriple(sId, pId, oId, spoIndexDS2Value, opsIndexDS2Value);
        } else {
            //spToRemoveDS2.add(sId, pId);
            spToRemoveDS2.computeIfAbsent(sId, x->new IntOpenHashSet()).add(pId);
        }
    }

    
    /*
    * The 6 following methods are for building composite properties.
    * Map<IntList,Int2ObjectMap<IntSet>> associates paths of properties to their subject and then objects (i.e. a PSO index)
    */
    
    private static void addToPathSOIndex(Map<IntList,Int2ObjectMap<IntSet>> index, IntList p, int s, int o) {
        Int2ObjectMap<IntSet> so = index.get(p);
        if (so==null) {
            index.put(p, so = new Int2ObjectOpenHashMap<>());
        }
        IntSet objects = so.get(s);
        if (objects == null) {
            so.put(s, objects = new IntOpenHashSet());
        }
        objects.add(o);
    }
    
    private static void addToPathSOIndex(Map<IntList,Int2ObjectMap<IntSet>> index, IntList p, int s, IntSet o) {
        Int2ObjectMap<IntSet> so = index.get(p);
        if (so==null) {
            index.put(p, so = new Int2ObjectOpenHashMap<>());
        }
        
        IntSet objects = so.get(s);
        if (objects == null) {
            so.put(s, objects = new IntOpenHashSet());
        }
        objects.addAll(o);
    }

    // Need to be checked because
    // it may loop on the same instance and then generates useless paths
    /**
     * Algorithm that build composite properties.
     * It builds composition of properties up to maxPathLength.
     * The strategy consists in building compositions from the end
     * @param fc
     * @param opsIndexValue
     * @param opsIndexURI
     * @param subjects
     * @param pIdx
     * @param maxPathLength
     * @param maxOSFactor
     * @return
     * @throws IOException 
     */
    private int compose3(FileChannel fc, TripleIndex opsIndexValue, final TripleIndex opsIndexURI, IntSet subjects, StringToId pIdx, int maxPathLength, int maxOSFactor) throws IOException {

        
        ByteBuffer buf = fc.map(FileChannel.MapMode.READ_WRITE, 0, Integer.MAX_VALUE);
        int nbTriples=0;
        
        // The PSO index that will contain the property paths represented as IntList of property ids
        Map<IntList,Int2ObjectMap<IntSet>> currentPathsSO = new Object2ObjectOpenHashMap<>();

        // Build paths of length 1 that ends to a datavalue
        // REMOVED : It only index triples that start with subject that is not in the list of considered subjects
        for (Int2ObjectMap.Entry<Int2ObjectOpenHashMap<IntOpenHashSet>> ops : opsIndexValue.int2ObjectEntrySet()) {
            int o = ops.getIntKey();
            for (Int2ObjectMap.Entry<IntOpenHashSet> ps : ops.getValue().int2ObjectEntrySet()) {
                int p = ps.getIntKey();
                IntIterator it = ps.getValue().iterator();
                while (it.hasNext()) {
                    int s = it.nextInt();
                    //  
                    if (/*!subjects.contains(s) &&*/ opsIndexURI.containsKey(s)) {
                        IntList path = IntLists.singleton(p);
                        addToPathSOIndex(currentPathsSO,path,s,o);
                    }
                }
            }
        }

        
       IntSet newProperties = new IntOpenHashSet();
       int length=1;
        while (!currentPathsSO.isEmpty() && length<maxPathLength) {
            length++;
            // index path -> subject -> objects
            Map<IntList,Int2ObjectMap<IntSet>> nextPathsSO = new Object2ObjectOpenHashMap<>();
            
            currentPathsSO.forEach((oldPath, soIndex) -> {
                soIndex.int2ObjectEntrySet().forEach( so -> { //(Integer oldS,IntSet oSet)
                    int oldS = so.getIntKey();
                    IntSet oSet = so.getValue();
                    if (opsIndexURI.containsKey(oldS))
                    opsIndexURI.get(oldS).int2ObjectEntrySet().forEach( ps -> { //
                        int p = ps.getIntKey();
                        IntSet sSet = ps.getValue();
                        if (!checkLoopProperty(oldPath,p,pIdx)){
                            IntList newPath = new IntArrayList(oldPath.size()+1);
                            newPath.addAll(oldPath);
                            newPath.add(p);

                            sSet.forEach( (int newS) -> {
                                addToPathSOIndex(nextPathsSO,newPath,newS,oSet);
                            });
                        }
                    });

                });
            });
            Iterator<Map.Entry<IntList,Int2ObjectMap<IntSet>>> it = nextPathsSO.entrySet().iterator();
            while (it.hasNext()) {     
                Map.Entry<IntList,Int2ObjectMap<IntSet>> entry = it.next();
                Int2ObjectMap<IntSet> soIndex = entry.getValue();
                IntList path = entry.getKey();
                int sum=0;
                for (IntSet oSet : soIndex.values()) {
                    sum+=oSet.size();
                }
                // Check the OSFactor
                if (maxOSFactor==-1 || sum/soIndex.size() < maxOSFactor) {
                    
                    for (Int2ObjectMap.Entry<IntSet> ent2 : soIndex.int2ObjectEntrySet()) {
                        int s = ent2.getIntKey();
                        IntSet oSet = ent2.getValue();
                        if (subjects.contains(s)) {
                            oSet.forEach( (int o) -> { buildTriple(path,s,o,pIdx,buf,newProperties);});
                            nbTriples+=oSet.size();
                           
                        }
                    }
                }
                else {
                    it.remove();
                }
            }
            currentPathsSO=nextPathsSO;
        }
        return nbTriples;

    }
    
     private static void buildTriple(IntList path, int s, int o, StringToId pIdx, ByteBuffer buf, IntSet newProperties) {
        StringBuilder sb = new StringBuilder();
        for (int p : path) {
            sb.insert(0,pIdx.getString(p));
            sb.insert(0,'°');
        }
        sb.deleteCharAt(0);
        int pId = pIdx.getId(sb.toString());
        newProperties.add(pId);
        buf.putInt(s);
        buf.putInt(pId);
        buf.putInt(o);
    }
    
    

    private static int getInverseProp(int p, StringToId pIdx) {
        String pString = pIdx.getString(p);
        if (pString.endsWith(INVERSE_SUFFIX)) {
            return  pIdx.getId(pString.substring(0,pString.length()-2));
        }
        else {
            return  pIdx.getIdIfExists(pString+INVERSE_SUFFIX);
        }
    }

    
    private static boolean checkLoopProperty(IntList path , int p,StringToId pIdx) {
        int pInverse = getInverseProp(p,pIdx);
        for (int px : path) {
            if (px==pInverse) {
                return true;
            }
        }
        return false;
    }


    /**
     * Generates Set(object)->p->s index from s->p->o index
     *
     * @param spoIndex the s->p->o index which is read
     * @param setopsIndex he Set(object)->p->s index that will be filled
     */
    private static void generateSetOPSIndex(TripleIndex spoIndex, SetOPSIndex setopsIndex) {
        for (Int2ObjectMap.Entry<Int2ObjectOpenHashMap<IntOpenHashSet>> v1Map : spoIndex.int2ObjectEntrySet()) {
            for (Int2ObjectMap.Entry<IntOpenHashSet> v2Map : v1Map.getValue().int2ObjectEntrySet()) {
                setopsIndex.addSetOPS(v2Map.getValue(), v2Map.getIntKey(), v1Map.getIntKey());
            }
        }
    }

    /**
     * Removes from this index all data which are not about the given subjects
     */
    private static <T>  void intersectionWithSubjects(Map<T,Int2ObjectOpenHashMap<IntOpenHashSet>> map, IntSet subjects) {
        Iterator<Map.Entry<T,Int2ObjectOpenHashMap<IntOpenHashSet>>> it1 = map.entrySet().iterator();
        while (it1.hasNext()) {
        //for (Int2ObjectOpenHashMap<IntOpenHashSet> v : set) {
            Int2ObjectOpenHashMap<IntOpenHashSet> ps = it1.next().getValue();
            ObjectIterator<Entry<IntOpenHashSet>> it2 = ps.int2ObjectEntrySet().iterator();//v.int2ObjectEntrySet().iterator();
            while (it2.hasNext()) {
                IntSet s = it2.next().getValue();
                s.retainAll(subjects);
                if (s.isEmpty()) {
                    it2.remove();
                }
            }
            if (ps.isEmpty()) {
                it1.remove();
            }
        }
    }

    /**
     * Removes from the given index, the properties that not discriminant for at least one value
     */
    private static <T>  void removeNotDiscProperties(Map<T,Int2ObjectOpenHashMap<IntOpenHashSet>> map, IntSet subjects, double discThreshold) {
        IntSet propToRem = new IntOpenHashSet();
        Iterator<Map.Entry<T,Int2ObjectOpenHashMap<IntOpenHashSet>>> it1 = map.entrySet().iterator();
        while (it1.hasNext()) {
            Int2ObjectOpenHashMap<IntOpenHashSet> ps = it1.next().getValue();
            ObjectIterator<Entry<IntOpenHashSet>> it2 = ps.int2ObjectEntrySet().iterator();
            while (it2.hasNext()) {
                Entry<IntOpenHashSet> entry = it2.next();
                IntSet s = entry.getValue();
                int p = entry.getIntKey();
                if (propToRem.contains(p)){
                    it2.remove();
                }
                else if (s.size()>=subjects.size()*discThreshold) {//==subjects.size())  { 
                    propToRem.add(p);
                    it2.remove();
                    //System.out.println("Not disc property removed : "+pIdx.getString(p));
                }
            }
        }
        it1 = map.entrySet().iterator();
        while (it1.hasNext()) {
            Int2ObjectOpenHashMap<IntOpenHashSet> ps = it1.next().getValue();
            ps.keySet().removeAll(propToRem);
        }
    }


    private void generateInCandidates(TripleIndex opsIndexDS1, TripleIndex opsIndexDS2) throws IOException {
        for (Int2ObjectMap.Entry<Int2ObjectOpenHashMap<IntOpenHashSet>> opsDS2 : opsIndexDS2.int2ObjectEntrySet()) {
            int o2 = opsDS2.getIntKey();

            Int2ObjectOpenHashMap<IntOpenHashSet> mappsDS1 = opsIndexDS1.get(o2);
            if (mappsDS1 != null) {
                for (Int2ObjectMap.Entry<IntOpenHashSet> psDS1 : mappsDS1.int2ObjectEntrySet()) {
                    int p1 = psDS1.getIntKey();
                    for (Int2ObjectMap.Entry<IntOpenHashSet> psDS2 : opsDS2.getValue().int2ObjectEntrySet()) {
                        int p2 = psDS2.getIntKey();
                        IntIterator it2 = psDS2.getValue().iterator();
                        while (it2.hasNext()) {
                            int s2 = it2.nextInt();
                            IntIterator it1 = psDS1.getValue().iterator();
                            while (it1.hasNext()) {
                                int s1 = it1.nextInt();
                                //inCandidates.put(uris,s2, p1, p2);
                                descriptions.putIn(IntPair.encode(s1,s2), IntPair.encode(p1,p2));
                            }
                        }

                    }
                }
            }
        }
    }

    private void generateEqCandidates(SetOPSIndex setopsIndexDS1, TripleIndex spoIndexDS2) throws IOException {
        for (Int2ObjectMap.Entry<Int2ObjectOpenHashMap<IntOpenHashSet>> spoDS2 : spoIndexDS2.int2ObjectEntrySet()) {
            // Iterates over properties for a given subject
            for (Int2ObjectMap.Entry<IntOpenHashSet> poDS2 : spoDS2.getValue().int2ObjectEntrySet()) {
                Int2ObjectOpenHashMap<IntOpenHashSet> mappsDS1 = setopsIndexDS1.get(poDS2.getValue());
                if (mappsDS1 != null) {
                    // Iterates over all the properties-subjects in DS1 having the same set of values
                    for (Int2ObjectMap.Entry<IntOpenHashSet> psDS1 : mappsDS1.int2ObjectEntrySet()) {
                        IntIterator it = psDS1.getValue().iterator();
                        while (it.hasNext()) {
                            int ds1SId = it.nextInt();
                            //eqCandidates.put(ds1SId, spoDS2.getIntKey(), psDS1.getIntKey(), poDS2.getIntKey());
                            descriptions.putEq(IntPair.encode(ds1SId,spoDS2.getIntKey()), IntPair.encode(psDS1.getIntKey(),poDS2.getIntKey()));
                        }
                    }
                }
            }
        }
    }

    private <T> void supportThreshold(Map<T,Int2ObjectOpenHashMap<IntOpenHashSet>> xpsIndex, int nbInstances, StringToId pIdx) {
        Int2ObjectOpenHashMap<IntOpenHashSet> propCount = new Int2ObjectOpenHashMap<>();

        xpsIndex.values().forEach( psMap -> {
            for (Entry<IntOpenHashSet> ps : psMap.int2ObjectEntrySet()) {
                IntOpenHashSet subs = propCount.get(ps.getIntKey());
                if (subs==null) {
                    propCount.put(ps.getIntKey(),subs = new IntOpenHashSet());
                }
                subs.addAll( ps.getValue());
            }
        });

        Iterator<Entry<IntOpenHashSet>> it = propCount.int2ObjectEntrySet().iterator();
        while (it.hasNext()) {
            Entry<IntOpenHashSet> pStat = it.next();
            double support = ((double)pStat.getValue().size() / nbInstances);
            if (support >= config.supportThreshold) {
                it.remove();
            }
        }

        xpsIndex.values().forEach( ps -> {
            ps.keySet().removeAll(propCount.keySet());
        });
    }

    private void addOWLThingAsType(IntSet subjects, Int2ObjectMap<IntSet> subject2Types) {
        if (subject2Types==null) return;
        int owlThingId = uriIdx.getId(OWL.Thing.getURI());
        IntSet subjectsCopy = new IntOpenHashSet(subjects);
        subjectsCopy.removeAll(subject2Types.keySet());
        for (int i : subjectsCopy) {
            subject2Types.put(i, IntSets.singleton(owlThingId));
        }
    }
    
    /**
     * To be called when indexation is finished to compute candidates
     * @throws IOException
     */
    public void finishIndexDatasets() throws IOException {

        
        
        
        // Store URI and datacache on the disk
        uriIdx.storeToDisk();
        valueIdx.storeToDisk();

        if (config.in || config.compose>1) {
             // compact value index: retain only values shared by both DS
            // only done on DS1 because for DS2 it2 is made on the fly
            opsIndexDS1Value.keySet().retainAll(opsIndexDS2Value.keySet());
            if (config.discThreshold>0) {
                removeNotDiscProperties(opsIndexDS1URI, subjectsDS1, config.discThreshold);
                removeNotDiscProperties(opsIndexDS1Value, subjectsDS1, config.discThreshold);
                removeNotDiscProperties(opsIndexDS2URI, subjectsDS2, config.discThreshold);
                removeNotDiscProperties(opsIndexDS2Value, subjectsDS2, config.discThreshold);
            }
            // support threshold before composition
            //supportThreshold(opsIndexDS1URI,subjectsDS1.size(), pIdxDS1);
            //supportThreshold(opsIndexDS2URI,subjectsDS2.size(), pIdxDS2);
        }

        
        // filter blank nodes if specified
        if (blankNodes!=null) {
            subjectsDS1.removeAll(blankNodes);
            subjectsDS2.removeAll(blankNodes);
        }
        
        //Composition
        // Has to be done with compacted opsIndexValue, in order to avoid
        // generation of useless composition
        if (config.compose>1) {
            /*TripleIndex opsIndexDS1URI_NoMatch = new TripleIndex();
            opsIndexDS1URI_NoMatch.putAll(opsIndexDS1URI);
            opsIndexDS1URI_NoMatch.keySet().removeAll(opsIndexDS2URI.keySet());*/

           Thread t1 = new Thread() {
                public void run() {
                    try {
                        Path resFile = Files.createTempFile("composition1", ".dat");
                        FileChannel fc =FileChannel.open(resFile,StandardOpenOption.WRITE, StandardOpenOption.READ, StandardOpenOption.DELETE_ON_CLOSE);
                        // DS1
                        int nbRes =compose3(fc,opsIndexDS1Value, opsIndexDS1URI, subjectsDS1, pIdxDS1,config.compose,config.maxOSFactor);/*.forEach(t -> {
                            if (subjectsDS1.contains(t[0])) {
                                indexTriple(t[0], t[1], t[2], spoIndexDS1Value, opsIndexDS1Value, psoIndexDS1);
                            }
                        });*/
                        ByteBuffer buf = fc.map(FileChannel.MapMode.READ_ONLY, 0, Integer.MAX_VALUE);

                        for (int i=0 ; i<nbRes ; i++) {
                            indexTriple(buf.getInt(),buf.getInt(),buf.getInt(), spoIndexDS1Value, opsIndexDS1Value/*, psoIndexDS1*/);
                        }
                        fc.close();
                    } catch (IOException ex) {
                        Logger.getLogger(LinkkeyDiscoveryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };

            Thread t2 = new Thread() {
                public void run() {
                    try {
                        Path resFile = Files.createTempFile("composition2", ".dat");
                        FileChannel fc =FileChannel.open(resFile,StandardOpenOption.WRITE, StandardOpenOption.READ, StandardOpenOption.DELETE_ON_CLOSE);
                        // DS2
                        int nbRes = compose3(fc,opsIndexDS2Value, opsIndexDS2URI, subjectsDS2, pIdxDS2,config.compose,config.maxOSFactor);/*.forEach(t -> {
                            if (subjectsDS2.contains(t[0])) {
                                indexTriple(t[0], t[1], t[2], spoIndexDS2Value, opsIndexDS2Value, psoIndexDS2);
                            }
                        });*/
                        ByteBuffer buf = fc.map(FileChannel.MapMode.READ_ONLY, 0, Integer.MAX_VALUE);
                        for (int i=0 ; i<nbRes ; i++) {
                            indexTriple(buf.getInt(),buf.getInt(),buf.getInt(), spoIndexDS2Value, opsIndexDS2Value/*, psoIndexDS2*/);
                        }
                        fc.close();
                    } catch (IOException ex) {
                        Logger.getLogger(LinkkeyDiscoveryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };

            t1.start();
            t2.start();
            try {
                t1.join();
                t2.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(LinkkeyDiscoveryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            }
            
           // System.out.println("removeAllBranch not disc properties after composition");
           if (config.discThreshold>0) {
            removeNotDiscProperties(opsIndexDS1Value, subjectsDS1,config.discThreshold);
            removeNotDiscProperties(opsIndexDS2Value, subjectsDS2,config.discThreshold);
           }
        }
        

        
        supportThreshold(opsIndexDS1Value,subjectsDS1.size(), pIdxDS1);
        supportThreshold(opsIndexDS2Value,subjectsDS2.size(), pIdxDS2);
        supportThreshold(opsIndexDS1URI,subjectsDS1.size(), pIdxDS1);
        supportThreshold(opsIndexDS2URI,subjectsDS2.size(), pIdxDS2);

        

        /*removeNotDiscProperties(opsIndexDS1Value, subjectsDS1, pIdxDS1);
        removeNotDiscProperties(opsIndexDS2Value, subjectsDS2, pIdxDS2);*/

        //System.out.println("Free mem after compose : " + Runtime.getRuntime().freeMemory());

        if (config.in) {
            // compact URI indexes: retain only URI shared by both DS
            // should be done after composition
            opsIndexDS1URI.keySet().retainAll(opsIndexDS2URI.keySet());
            opsIndexDS2URI.keySet().retainAll(opsIndexDS1URI.keySet());

            // cleaning : removeAllBranch ops entries that are not about selected subjects
            intersectionWithSubjects(opsIndexDS1URI, subjectsDS1);
            intersectionWithSubjects(opsIndexDS1Value, subjectsDS1);
            intersectionWithSubjects(opsIndexDS2URI, subjectsDS2);
            intersectionWithSubjects(opsIndexDS2Value, subjectsDS2);
        }
        else { // case only composition without  In -> ops indexes are now useless
            opsIndexDS1URI=null;
            opsIndexDS1Value=null;
            opsIndexDS2URI=null;
            opsIndexDS2Value=null;
        }



        // Generates Set(o)ps index for DS1
        // Performs some cleaning on indexes for generating EQ candidate
        // Store them temporarily on a file for freeing memory
        Path store=null;
        if (config.eq) {

            // removeAllBranch all subject-property pairs for which at least one object is not in DS1
            // concern only values
            
            for (Int2ObjectMap.Entry<IntSet> e : spToRemoveDS2.int2ObjectEntrySet()) {
            //for (Map.Entry<Integer, Set<Integer>> e : spToRemoveDS2.entrySet()) {
                Int2ObjectOpenHashMap<IntOpenHashSet> v1Map = spoIndexDS2Value.get(e.getIntKey());
                if (v1Map != null) {
                    v1Map.keySet().removeAll(e.getValue());
                }
            }
            

            spToRemoveDS2 = null;

            // removeAllBranch not considered subjects
            spoIndexDS1URI.keySet().retainAll(subjectsDS1);
            spoIndexDS1Value.keySet().retainAll(subjectsDS1);
            spoIndexDS2URI.keySet().retainAll(subjectsDS2);
            spoIndexDS2Value.keySet().retainAll(subjectsDS2);

            // Reverse the spo index into Set(o)ps index
            generateSetOPSIndex(spoIndexDS1URI, setopsIndexDS1URI);
            generateSetOPSIndex(spoIndexDS1Value, setopsIndexDS1Value);
            spoIndexDS1URI = null;//new TripleIndex();
            spoIndexDS1Value = null;

            // useless
            //intersectionWithSubjects(setopsIndexDS1URI, subjectsDS1);
            //intersectionWithSubjects(setopsIndexDS1Value, subjectsDS1);

            File f = File.createTempFile(this.getClass().getName(), ".dat");
            f.deleteOnExit();
            store = f.toPath();
            ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(Files.newOutputStream(store)));
            oos.writeObject(setopsIndexDS1URI);
            oos.writeObject(spoIndexDS2URI);
            oos.writeObject(setopsIndexDS1Value);
            oos.writeObject(spoIndexDS2Value);
            oos.close();
            oos = null;
            setopsIndexDS1URI = null;
            setopsIndexDS1Value = null;
            spoIndexDS2URI = null;
            spoIndexDS2Value = null;
        }

        // Subject count before composition
        subjectsDS1Size = subjectsDS1.size();
        subjectsDS2Size = subjectsDS2.size();

        addOWLThingAsType(subjectsDS1,instancesTypesDS1);
        addOWLThingAsType(subjectsDS2,instancesTypesDS2);


        // generation of In candidates
        descriptions = new DescriptionsSet(); // uncomment the two lines
        if (config.in) {
            //inCandidates = new Long2LongSetFileMap();
            generateInCandidates(opsIndexDS1URI, opsIndexDS2URI);
            opsIndexDS1URI = null;
            opsIndexDS2URI = null;
            generateInCandidates(opsIndexDS1Value, opsIndexDS2Value);
            opsIndexDS1Value = null;
            opsIndexDS2Value = null;
        }

        if (config.eq) {
            // Generate the eq property pairs
            //eqCandidates = new Long2LongSetFileMap();
            try (  ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(Files.newInputStream(store)))) {
                setopsIndexDS1URI = (SetOPSIndex) ois.readObject();
                spoIndexDS2URI = (TripleIndex) ois.readObject();
                generateEqCandidates(setopsIndexDS1URI, spoIndexDS2URI);
                setopsIndexDS1URI = null;
                spoIndexDS2URI = null;

                setopsIndexDS1Value = (SetOPSIndex) ois.readObject();
                spoIndexDS2Value = (TripleIndex) ois.readObject();
                generateEqCandidates(setopsIndexDS1Value, spoIndexDS2Value);
                setopsIndexDS1Value = null;
                spoIndexDS2Value = null;
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(LinkkeyDiscoveryAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            }
            Files.deleteIfExists(store);
        }
    }


    /**
     * Compute the reverse description index: properties pairs to link set
     * @return
     */
    public RedescriptionTools computeRedescriptionTools() {
        Long2ObjectMap<LongSet> propertyIdx = new Long2ObjectOpenHashMap<>();
        Long2ObjectMap<LongSet> classesIdx = new Long2ObjectOpenHashMap<>();
        Iterator<Long2ObjectMap.Entry<LongSet[]>> it = descriptions.entryIterator();
        // iteration on the links
        while  (it.hasNext()) {
            Long2ObjectMap.Entry<LongSet []> e =it.next();
            // iteration of in properties pairs
            for (long p : e.getValue()[0]) {
                propertyIdx.computeIfAbsent(p, x -> new LongOpenHashSet()).add(e.getLongKey());
            }

            long link = e.getLongKey();
            IntSet c1 = instancesTypesDS1.get(IntPair.decodeI1(link));
            IntSet c2 = instancesTypesDS2.get(IntPair.decodeI2(link));
            for (int x1 : c1) {
                for (int x2 : c2) {
                    classesIdx.computeIfAbsent(IntPair.encode(x1,x2), x->new LongOpenHashSet()).add(link);
                }
            }

        }
        RedescriptionTools tool = new RedescriptionTools(propertyIdx,classesIdx);
        return tool;
    }
    
    
    /**
     * Gives the set of classes to which an instance belongs to
     * @param instanceId the instance identified
     * @param instance2Types the index associating each instance to its types
     * @return 
     */
    private Set<IntSet> getClassExpression(int instanceId, Int2ObjectMap<IntSet> instance2Types) {
        if (instance2Types==null || !instance2Types.containsKey(instanceId)) {
            //return Collections.singleton(IntSets.singleton(uriIdx.getId(OWL.Thing.getURI())));
            return Collections.emptySet();
        }
        return Collections.singleton(instance2Types.get(instanceId));
    }
    
    /*
    * Add Intent Algorithm
    */
    public ExtractionResult computeLinkkeyWithFCA(Map<String,String> prefixMap) {
        
        // DEBUG
        /*Int2ObjectMap<IntSet> classes2InstancesDS1 = getClasses2Instances(instancesTypesDS1);
        Int2ObjectMap<IntSet> classes2InstancesDS2 = getClasses2Instances(instancesTypesDS2);
        String[] uris = uriIdx.getStrings();*/
        //DEBUG
        
        
        LongSet in = descriptions.getInPairs();
        LongSet eq = descriptions.getEqPairs();
        
       CandidateLinkkey bottom = new CandidateLinkkey(eq,in, Collections.emptySet(), Collections.emptySet());
        
       Iterator<Long2ObjectMap.Entry<LongSet[]>> it = descriptions.entryIterator();
       while (it.hasNext()) {
           Long2ObjectMap.Entry<LongSet[]> entry = it.next();
           long link = entry.getLongKey();

            Set<IntSet> classes1 = getClassExpression(IntPair.decodeI1(link),instancesTypesDS1);   
            Set<IntSet> classes2 = getClassExpression(IntPair.decodeI2(link),instancesTypesDS2);
            
            CandidateLinkkey c = addIntent(order,entry.getValue()[1],entry.getValue()[0],classes1,classes2,bottom);
            // DEBUG
            /*for (CandidateLinkkey d : c.getAncestors()) {
                IntSet instances1 = EvalMeasures.computeClassExpInstances(d.getClasses1(),classes2InstancesDS1);
                if (!instances1.contains(IntPair.decodeI1(link))) {
                    System.err.println("PB!!!!");
                    System.out.println("CL Expression 1 :"+ d.getClasses1());
                    d.classes1.forEach(s -> s.forEach((int x)->System.out.print(uris[x]+", ")));
                    System.out.println();
                    System.out.println("instance classes :"+ instancesTypesDS1.get(IntPair.decodeI1(link)));
                    instancesTypesDS1.get(IntPair.decodeI1(link)).forEach((int x) -> System.out.print(uris[x]+", "));
                    System.out.println();
                }

                 IntSet instances2 = EvalMeasures.computeClassExpInstances(d.getClasses2(),classes2InstancesDS2);
                 if (!instances2.contains(IntPair.decodeI2(link))) {
                    System.err.println("PB!!!!");
                    System.out.println("CL Expression 2 :"+ d.getClasses2());
                    d.classes2.forEach(s -> s.forEach((int x)->System.out.print(uris[x]+", ")));
                    System.out.println();
                    System.out.println("instance classes :"+ instancesTypesDS2.get(IntPair.decodeI2(link)));
                    instancesTypesDS2.get(IntPair.decodeI2(link)).forEach((int x) -> System.out.print(uris[x]+", "));
                    System.out.println();
                }
                 d.clearCaches();
            }
            c.clearCaches();*/
            // DEBUG
            c.addSpecificLink(link);
       }
       
       
        ExtractionResult res =  new ExtractionResult(bottom.getTop(),
                pIdxDS1.getStrings() , pIdxDS2.getStrings(), uriIdx.getStrings(), 
                config.typesDS1, config.typesDS2,
                subjectsDS1, subjectsDS2,
                getClasses2Instances(instancesTypesDS1),getClasses2Instances(instancesTypesDS2),
                referenceLinks,prefixMap);
        
        
        descriptions=null;
        return res;
        
    }
    
    //int nb=0;
    public CandidateLinkkey addIntent(PartialOrder order, LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2, CandidateLinkkey generator) {
        generator = getMaximal(order,generator,eqPairs, inPairs, classes1, classes2);
        if (order.equals(eqPairs, inPairs, classes1, classes2,generator)) {//(generator.equals(eqPairs,inPairs, classes1, classes2)) {
            // Update class expressions in the case of partial order only on property pairs
            updateClassExpressions(Collections.singleton(generator),classes1,classes2);
            return generator;
        }
        Set<CandidateLinkkey> newParents = new HashSet<>();
        // search for the candidates to be the parent of the concept to be created
        for (CandidateLinkkey p : generator.getParents()) {
            //if (!p.isContained(eqPairs, inPairs) || p.equals(eqPairs, inPairs)) {
            // if a parent the generator is not a subsumer of intent
            // then search a subsumer within the hiearchy
            if (!order.contains(eqPairs, inPairs, classes1, classes2, p)) {//if (!p.isContained(eqPairs, inPairs, classes1, classes2)) {// || p.equals(eqPairs, inPairs, classes1, classes2)) {
                LongSet meetEqPairs = new LongOpenHashSet(eqPairs);
                LongSet meetInPairs = new LongOpenHashSet(inPairs);
                meetEqPairs.retainAll(p.getEqPairs());
                meetInPairs.retainAll(p.getInPairs());
                
                Set<IntSet> unionClasses1 = ClassExpressions.union(p.getClasses1(), classes1);
                Set<IntSet> unionClasses2 = ClassExpressions.union(p.getClasses2(), classes2);
               
                p=addIntent(order,meetEqPairs,meetInPairs,unionClasses1,unionClasses2,p);
            }
            boolean addParent=true;
            Iterator<CandidateLinkkey> itNewParents = newParents.iterator();
            while (itNewParents.hasNext()) {
                CandidateLinkkey np = itNewParents.next();
                if (order.contains(np, p)) {//if (p.moreGeneralThan(np)) {
                    addParent=false;
                    break;
                }
                else if (order.contains(p, np)) {//else if (np.moreGeneralThan(p)) {
                    itNewParents.remove();
                }
            }
            if (addParent) {
                newParents.add(p);
            }
        }
        CandidateLinkkey newCandidate = new CandidateLinkkey(eqPairs,inPairs,classes1,classes2);
        /*nb++;
        if (nb%100000==0) {
            System.out.println(nb);
            //if (nb%100000==0) System.out.println("Ancestors: "+generator.getBottom().getNbAncestors());
        }*/
        
        for (CandidateLinkkey np : newParents) {
            generator.removeParent(np);
            newCandidate.addParent(np);
        }
        
        // Update class expressions in the case of partial order only on property pairs
        newCandidate.classes1=ClassExpressions.union(newCandidate.getClasses1(), generator.classes1);
        newCandidate.classes2=ClassExpressions.union(newCandidate.getClasses2(), generator.classes2);
        updateClassExpressions(newParents,classes1,classes2);
        
        
        // union of classes to root
        /*CandidateLinkkey root = newCandidate.getTop();
        root.classes1=ClassExpressions.union(root.getClasses1(), classes1);
        root.classes2=ClassExpressions.union(root.getClasses2(), classes2);*/
        
        generator.addParent(newCandidate);
        
        return newCandidate;
        
    }
    
    public static void updateClassExpressions(Set<CandidateLinkkey> toUpdate, Set<IntSet> classes1,Set<IntSet> classes2) {
        while (!toUpdate.isEmpty()) {
            Set<CandidateLinkkey> next = new HashSet<>();
            for (CandidateLinkkey c : toUpdate) {
                boolean add=false;
                if (!ClassExpressions.contains(c.classes1,classes1)) {
                    c.classes1 = ClassExpressions.union(c.classes1, classes1);
                    add=true;
                }
                if (!ClassExpressions.contains(c.classes2,classes2)) {
                    c.classes2 = ClassExpressions.union(c.classes2,classes2);
                    add=true;
                }
                if (add) {
                    next.addAll(c.getParents());
                }
            }
            toUpdate=next;
        }
    }
    
    /* 
        returns a generator of a given pair of sets of eq and in property pairs
        a generator is a most general subsumee of the given intent
        This is used by addIntent algorithm
    */
    public static CandidateLinkkey getMaximal(PartialOrder order, CandidateLinkkey generator, LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2) {
        boolean parentIsMaximal = true;
        while (parentIsMaximal) {
            parentIsMaximal = false;
            for (CandidateLinkkey p : generator.getParents()) {
                // if parent is more specific or equals to the given intent
                // then p becomes generator and continue
                if (order.contains(p, eqPairs, inPairs, classes1, classes2)) { //if (p.contains(eqPairs, inPairs, classes1, classes2)) {
                    generator = p;
                    parentIsMaximal = true;
                    break;
                }
            }
        }
        return generator;
    }

    // old algorithm for extraction
    @Deprecated
    public ExtractionResult computeLinkRuleCandidates(Map<String,String> prefixMap) {
        //System.out.println("START Compute candidates");
        ExtractionResult res = new ExtractionResult(pIdxDS1.getStrings(), pIdxDS2.getStrings(), uriIdx.getStrings(), 
                config.typesDS1, config.typesDS2,subjectsDS1, subjectsDS2,referenceLinks,prefixMap);
        
        
        Iterator<Long2ObjectMap.Entry<LongSet[]>> it = descriptions.entryIterator();
       while (it.hasNext()) {
           Long2ObjectMap.Entry<LongSet[]> entry = it.next();
           long link = entry.getLongKey();
            
            Set<IntSet> classes1 = getClassExpression(IntPair.decodeI1(link),instancesTypesDS1);   
            Set<IntSet> classes2 = getClassExpression(IntPair.decodeI2(link),instancesTypesDS2);
            
            CandidateLinkkey c = res.getOrAddCandidate(entry.getValue()[1], entry.getValue()[0], classes1, classes2);
            
            c.addSpecificLink(link);

       }
        return res;//finalCandidates;
    }

    public String[] getUris() {
        return uriIdx.getStrings();
    }
    
    public String[] getPDS1() {
        return pIdxDS1.getStrings();
    }
    
    public String[] getPDS2() {
        return pIdxDS2.getStrings();
    }
    
    public IntSet getSubjectsDS1() {
        return subjectsDS1;
    }

    public IntSet getSubjectsDS2() {
        return subjectsDS2;
    }

    public Set<String> getTypesDS1() {
        return config.typesDS1;
    }

    public Set<String> getTypesDS2() {
        return config.typesDS2;
    }

    public StringToId getUriIdx() {
        return uriIdx;
    }

    public StringToId getValueIdx() {
        return valueIdx;
    }

    public int getSubjectsDS1Size() {
        return subjectsDS1Size;
    }

    public int getSubjectsDS2Size() {
        return subjectsDS2Size;
    }  
    
    private static Int2ObjectMap<IntSet> getClasses2Instances(Int2ObjectMap<IntSet> instancesTypes) {
        if (instancesTypes==null) return Int2ObjectMaps.emptyMap();
        Int2ObjectMap<IntSet> res = new Int2ObjectOpenHashMap<>();

        for (Entry<IntSet> pair : instancesTypes.int2ObjectEntrySet()) {
            pair.getValue().forEach((int c) -> {
                res.computeIfAbsent(c, x -> new IntOpenHashSet()).add(pair.getIntKey());
            });
        }
        return res;
    }
}
