/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey;

import fr.inrialpes.exmo.linkkey.utils.renderer.*;
import org.apache.commons.cli.*;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.lang.PipedRDFIterator;
import org.apache.jena.vocabulary.OWL;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

/**
 *
 * @author jdavid
 */
public class LinkkeyExtraction {

    String outputFilename;
    String c1;
    String c2;
    
    String p1;
    String p2;
    
    String eval;
    String format;
    int compos;
    boolean inverse;
    boolean blankNodes;
    double supportMin;
    int osMaxFactor;
    boolean in = true;
    boolean eq = true;
    
    int classes = ExtractionConfig.NO_CLASSES;
    
    CommandLine commandLine;
    String dataset1;
    String dataset2;
    Set<String> c1Uris = null;
    Set<String> c2Uris = null;
    
    double discThreshold;

    // Command's arguments functions
    private static Options buildOptions() {
        Option help = new Option("help", "print this message");
        Options options = new Options();

        options.addOption(help);

        Option format = new Option("f", "Format of the output: txt (default), edoal, html, bin, dot, txt2 (txt with links)");
        format.setArgName("format");
        format.setArgs(1);
        format.setOptionalArg(false);
        format.setType(java.lang.String.class);
        format.setLongOpt("format");
        options.addOption(format);

        Option output = new Option("o", "output filename. Default files: standard output for txt and edoal, 'result' for html and bin");
        output.setArgName("outputfile");
        output.setArgs(1);
        output.setOptionalArg(false);
        output.setType(java.lang.String.class);
        output.setLongOpt("output");
        options.addOption(output);

        Option c1 = new Option("c1", "Uri of the first class (if omitted, all instances are considered)");
        c1.setArgName("uri1");
        c1.setArgs(1);
        c1.setOptionalArg(false);
        c1.setType(java.lang.String.class);
        options.addOption(c1);

        Option c2 = new Option("c2", "Uri of the second class (if omitted, all instances are considered)");
        c2.setArgName("uri2");
        c2.setArgs(1);
        c2.setOptionalArg(false);
        c2.setType(java.lang.String.class);
        options.addOption(c2);
        
        Option classes = new Option("classes", "extracts link keys candidates with classes");
        classes.setRequired(false);
        classes.setArgs(0);
        options.addOption(classes);
        
        Option classesfull = new Option("classesfull", "extracts link keys candidates with classes full (may be very expensive)");
        classesfull.setRequired(false);
        classesfull.setArgs(0);
        options.addOption(classesfull);
        
        Option p1 = new Option("p1", "prefix of classes that have to be considered");
        p1.setArgName("uriprefix1");
        p1.setArgs(1);
        p1.setOptionalArg(false);
        p1.setType(java.lang.String.class);
        options.addOption(p1);
        
        Option p2 = new Option("p2", "prefix of classes that have to be considered");
        p2.setArgName("uriprefix1");
        p2.setArgs(1);
        p2.setOptionalArg(false);
        p2.setType(java.lang.String.class);
        options.addOption(p2);

        Option keyType = new Option("t", "types of extracted keys: eq or/and in (eq and in by default)");
        keyType.setArgName("eq or in");
        keyType.setArgs(1);
        keyType.setOptionalArg(false);
        keyType.setType(java.lang.String.class);
        options.addOption(keyType);

        Option sparql = new Option("sparql", "if given the datasets are considered as sparql endpoints");
        sparql.setArgName("query");
        sparql.setArgs(0);
        sparql.setType(java.lang.String.class);
        options.addOption(sparql);

        Option compos = new Option("c", "compose properties");
        compos.setRequired(false);
        compos.setArgName("composition length");
        compos.setArgs(1);
        compos.setOptionalArg(true);
        compos.setType(java.lang.Integer.class);
        options.addOption(compos);
        
        Option discThreshold = new Option("d", "property discriminability threshold");
        discThreshold.setRequired(false);
        discThreshold.setArgName("d");
        discThreshold.setArgs(1);
        discThreshold.setOptionalArg(false);
        discThreshold.setType(java.lang.Double.class);
        options.addOption(discThreshold);

        Option blankNodes = new Option("b", "find links between blank nodes (true by default)");
        blankNodes.setRequired(false);
        blankNodes.setArgName("b");
        blankNodes.setArgs(1);
        blankNodes.setOptionalArg(true);
        blankNodes.setType(java.lang.Boolean.class);
        options.addOption(blankNodes);

        Option eval = new Option("e", "use the given reference links for precision and recall evaluation."
                + "the links are given in RDF (i.e. a list of triples with predicate owl:sameAs)");
        eval.setRequired(false);
        eval.setArgName("e");
        eval.setArgs(1);
        eval.setOptionalArg(false);
        eval.setType(java.lang.String.class);
        options.addOption(eval);

        Option inverse = new Option("i", "considers inverse of properties (only useful with -c)");
        inverse.setArgName("i");
        inverse.setArgs(0);
        inverse.setType(java.lang.String.class);
        options.addOption(inverse);

        Option support = new Option("s", "a support threshold between [0;1] for properties (default:0)");
        support.setArgName("s");
        support.setArgs(1);
        support.setOptionalArg(false);
        support.setType(java.lang.Double.class);
        options.addOption(support);

        Option lazy = new Option("l", "Lazy mode, data will be loaded when needed (only available for bin)");
        lazy.setRequired(false);
        lazy.setArgName("l");
        lazy.setOptionalArg(true);
        lazy.setType(java.lang.Boolean.class);
        options.addOption(lazy);

        return options;
    }

    public void parseArguments(String[] args) {
        Options options = buildOptions();
        CommandLineParser parser = new DefaultParser();
        try {
            commandLine = parser.parse(options, args);
            this.checkArguments(commandLine);
            outputFilename = commandLine.getOptionValue("o", "result");
            c1 = commandLine.getOptionValue("c1", null);
            c2 = commandLine.getOptionValue("c2", null);
            p1 = commandLine.getOptionValue("p1", null);
            p2 = commandLine.getOptionValue("p2", null);
            eval = commandLine.getOptionValue("e", null);
            compos = Integer.parseInt(commandLine.getOptionValue('c', "1"));
            inverse = commandLine.hasOption('i');
            blankNodes = Boolean.parseBoolean(commandLine.getOptionValue('b', "false"));
            supportMin = Double.parseDouble(commandLine.getOptionValue('s', "0"));
            discThreshold = Double.parseDouble(commandLine.getOptionValue('d', "1"));
            osMaxFactor = 5;//Integer.parseInt(commandLine.getOptionValue('o', "5"));//10; //

            if (commandLine.hasOption("classes")) {
                classes = ExtractionConfig.CLASSES;   
            }
            else if (commandLine.hasOption("classesfull")) {
                classes = ExtractionConfig.CLASSES_FULL;
            }
            
            format = commandLine.getOptionValue("f", "txt");

            if (c1 != null) {
                c1Uris = new HashSet<>(Arrays.asList(c1.split(",")));
            }
            if (c2 != null) {
                c2Uris = new HashSet<>(Arrays.asList(c2.split(",")));
            }
            dataset1 = (String) commandLine.getArgList().get(0);
            dataset2 = (String) commandLine.getArgList().get(1);
        } catch (ParseException ex) {
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java " + LinkkeyDiscoveryAlgorithm.class.getName() + " [options] dataset1 dataset2", options);
            System.exit(1);
        }
    }

    private void checkArguments(CommandLine commandLine) throws ParseException {
        if (commandLine.getArgList().size() < 2) {
            throw new ParseException("Two datasets must be given as arguments");
        }
        if (commandLine.hasOption("sparql") && !(commandLine.hasOption("c1") && commandLine.hasOption("c2"))) {
            throw new ParseException("Option -sparql has to be used with options -c1 and -c2");
        }
        if (commandLine.hasOption("t")) {
            eq = false;
            in = false;
            for (String v : commandLine.getOptionValues("t")) {
                if (v.equals("eq")) {
                    eq = true;
                } else if (v.equals("in")) {
                    in = true;
                } else {
                    throw new ParseException(v + " is not value for option -t. Use eq or/and in.");
                }
            }
        }
    }
    


    // Render functions


    
    
  /*  public static void renderTXT2(ExtractionResult s,  boolean abbreviateUris, PrintWriter out) throws IOException {
        EvalMeasures eval = new EvalMeasures(s.getClass1Size(), s.getClass2Size());
        LinkKeyRenderer renderer = new LinkKeyRenderer(s);
        NumberFormat f = NumberFormat.getInstance();
        f.setMaximumFractionDigits(3);
        int id=0;
        LongSet links = new LongOpenHashSet();
        out.println("id"+ "\t" +"# links" + "\t" + "# c1" + "\t" + "# c2" + "\t" + "disc." + "\t" + "cov." + "\t" + "candidate");
        ArrayList<CandidateLinkkey> cand = new ArrayList(s.getCandidates());
        for (CandidateLinkkey r : cand) {
            out.println(id+ "\t" +f.format(eval.getSupport(r)) + "\t" + f.format(r.getInstances1Size()) + "\t" + f.format(r.getInstances2Size()) + "\t" + f.format(eval.discriminability(r)) + "\t" + f.format(eval.coverage(r)) + "\t" + renderer.toString(r, abbreviateUris));
            id++;
            links.addAll(r.getSpecificLinks());
        }
        out.println("### Links");
        for (long l : links) {
            out.print(renderer.decodeAbbrevS1(l) + "\t" + renderer.decodeAbbrevS2(l));
            id=0;
            for (CandidateLinkkey r : cand) {
                if (r.getLinks().contains(l)) {
                    out.print("\t" +id);
                }
                id+=1;
            }
            out.println();
        }
    }*/


    private void printResults(ExtractionResult res, String format, boolean lazyMode) throws IOException {
        if ("html".equals(format)) {
            HtmlRenderer renderer = new HtmlRenderer(Paths.get(outputFilename), true);
            renderer.renderHTML(res);
        } else if ("bin".equals(format)) {
            if (lazyMode) {
                RandomAccessFile file = new RandomAccessFile(outputFilename, "rw");
                res.writeLazy(file);
                file.close();
            } else {
                ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(Paths.get(outputFilename), StandardOpenOption.CREATE));
                res.write(oos);
                oos.close();
            }
        } else if ("redesc".equals(format)) {
            RenderForRedesc r = new RenderForRedesc(res, new TxtLinkKeyRenderer(res),
                                                       Paths.get(outputFilename), 
                                                       Paths.get(dataset1),
                                                       Paths.get(dataset2));
            r.run();
            
        } /*else if ("expeclust".equals(format)) {
            ExpeClust.clusteringExpe(res,Paths.get(outputFilename));
        }*/ else {
            PrintWriter out = new PrintWriter(System.out);
            if (commandLine.hasOption('o')) {
                out = new PrintWriter(Files.newBufferedWriter(Paths.get(outputFilename)));
            }
            if ("edoal".equals(format)) {
                EdoalLinkKeyRenderer r = new EdoalLinkKeyRenderer(res,dataset1, dataset2);
                r.render(out);
            } else if ("dot".equals(format)) {
                DotLinkKeyRenderer r = new DotLinkKeyRenderer(res);
                r.render(out);
            } /*else if ("testselect".equals(format)) { 
                CombinationExperiments.expe(res, out);
            }*//* else if ("txt2".equals(format)) {
                renderTXT2(res, true, out);
            }
            else if ("hclust".equals(format)) {
                ClusteringInterface.clustering(res);
            }*/
            else {
                TxtLinkKeyRenderer r = new TxtLinkKeyRenderer(res);
                r.render(out);
            }
            out.flush();
            out.close();
        }
    }



    // nodes functions
    public static Iterator<Triple> selectInstances(String classUri, String sparqlEndpoint) {
        Query query = QueryFactory.create("CONSTRUCT {?s ?p ?o.} WHERE {?s ?p ?o .?s a <" + classUri + ">}");
        QueryExecution qexec = QueryExecutionFactory.sparqlService(sparqlEndpoint, query);
        return qexec.execConstructTriples();

    }

    public static Iterator<Triple> querySameAs(String sparqlEndpoint) {
        Query query = QueryFactory.create(
                "CONSTRUCT {?s <http://www.w3.org/2002/07/owl#sameAs> ?o.} WHERE {?s <http://www.w3.org/2002/07/owl#sameAs> ?o}");
        QueryExecution qexec = QueryExecutionFactory.sparqlService(sparqlEndpoint, query);
        return qexec.execConstructTriples();
    }

    private void mergeSameAsNodes(LinkkeyDiscoveryAlgorithm disco, String dataset, Boolean isSparql) throws IOException {
        Iterator<Triple> it;
        if (isSparql) {
            it = querySameAs(dataset);
        } else {
            it = RDFDataMgr.createIteratorTriples(Files.newInputStream(Paths.get(dataset)),RDFLanguages.filenameToLang(dataset),"");
        }
        while (it.hasNext()) {
            Triple t = it.next();
            String s = t.getSubject().getURI();
            String o = t.getObject().getURI();
            if (OWL.sameAs.asNode().equals(t.getPredicate())) {
                disco.mergeSameAs(s, o);
            }
        }
    }

    // utils functions
    private void copyPrefixes(Iterator it, Map<String, String> pm) {
        if (it instanceof PipedRDFIterator) {
            pm.putAll(((PipedRDFIterator) it).getPrefixes().getMappingCopyStr());
        }
    }

    private Iterator<Triple> makeIterator(String class_, String dataset, boolean isSparql) throws IOException {
        if (isSparql) {
            return selectInstances(class_, dataset);
        }
        return RDFDataMgr.createIteratorTriples(Files.newInputStream(Paths.get(dataset)),RDFLanguages.filenameToLang(dataset),"");
    }

    // core functions
    public LinkkeyDiscoveryAlgorithm buildDiscovery(Map<String, String> prefixMap) throws IOException {
        Iterator<Triple> it;
        ExtractionConfig config = new ExtractionConfig(in, eq, classes, inverse, compos, c1Uris, c2Uris, p1, p2, supportMin, osMaxFactor, discThreshold, blankNodes);
        LinkkeyDiscoveryAlgorithm disco = new LinkkeyDiscoveryAlgorithm(config);

        // This is explained nowhere in the docs
        if (commandLine.getArgList().size() == 3) {
            this.mergeSameAsNodes(disco, (String) commandLine.getArgList().get(2), commandLine.hasOption("sparql"));
        }

        it = this.makeIterator(c1, dataset1, commandLine.hasOption("sparql"));
        while (it.hasNext()) {
            Triple t = it.next();
            String s;
            if (t.getSubject().isBlank()) {
                s = "BN_DS1:" + t.getSubject().getBlankNodeLabel();
                disco.addBlankNode(s);
            } else { //URI
                s = t.getSubject().getURI();
            }
            String p = t.getPredicate().getURI();
            Object o;

            if (t.getObject().isLiteral()) { // checking if it is string
                o = t.getObject().getLiteralValue();
                //if (t.getObject().getLiteralLexicalForm().length()<100){
                    disco.addValueTripleDS1(s, p, o);
                //}
            } else if (t.getObject().isURI()) {
                o = t.getObject().getURI(); // URI
                if ("http://purl.org/dc/terms/contributor".equals(p) || ("http://rdvocab.info/RDARelationshipsWEMI/expressionManifested".equals(p))) {
                    disco.addURITripleDS1(s, p, o.toString().replace("#about", "#foaf:Person"));
                } else {
                    disco.addURITripleDS1(s, p, o.toString());
                }
            } else { // Blank Node
                o = "BN_DS1:" + t.getObject().getBlankNodeLabel();
                disco.addURITripleDS1(s, p, o.toString());
            }
        }
        this.copyPrefixes(it, prefixMap);

        it = this.makeIterator(c2, dataset2, commandLine.hasOption("sparql"));
        while (it.hasNext()) {
            Triple t = it.next();
            String s;
            if (t.getSubject().isBlank()) {
                s = "BN_DS2:" + t.getSubject().getBlankNodeLabel();
                disco.addBlankNode(s);
            } else { //URI
                s = t.getSubject().getURI();
            }
            String p = t.getPredicate().getURI();
            Object o;

            if (t.getObject().isLiteral()) { // checking if it is string
                o = t.getObject().getLiteralValue();
                //if (t.getObject().getLiteralLexicalForm().length()<100){
                    disco.addValueTripleDS2(s, p, o);
                //}
            } else if (t.getObject().isURI()) {
                o = t.getObject().getURI();
                disco.addURITripleDS2(s, p, o.toString());
            } else { // Blank Node
                o = "BN_DS2:" + t.getObject().getBlankNodeLabel();
                disco.addURITripleDS2(s, p, o.toString());
            }
        }
        this.copyPrefixes(it, prefixMap);

        disco.finishIndexDatasets();

        // load ref links to be done before extraction...
        if (commandLine.hasOption("e")) {
            it = RDFDataMgr.createIteratorTriples(Files.newInputStream(Paths.get(eval)),RDFLanguages.filenameToLang(eval),"");
            it.forEachRemaining((t) -> {
                disco.addReferenceLink(t.getSubject().getURI(), t.getObject().getURI());
            });

        }
        return disco;
    }

    public void extract(String[] args) throws IOException {
        Map<String, String> prefixMap = new HashMap<>();

        this.parseArguments(args);

        LinkkeyDiscoveryAlgorithm disco = this.buildDiscovery(prefixMap);

        ExtractionResult res = disco.computeLinkkeyWithFCA(prefixMap);
        printResults(res, format, commandLine.hasOption("l"));
    }

    public static void main(String[] args) throws IOException {
        new LinkkeyExtraction().extract(args);
    }
}
