/*
 * Copyright (C) 2014-2018 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.eval;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.EvaluableRule;
import fr.inrialpes.exmo.linkkey.utils.IntPair;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class SupervisedEvalMeasures {
    
    private LongSet positiveRef;
    private LongSet negativeRef;
    
    private IntSet linkedInstancesDS1;
    private IntSet linkedInstancesDS2;
    
    
    private Object2IntMap<EvaluableRule> examplesCounts;
    private Object2IntMap<EvaluableRule> counterExamplesCounts;
    
    // used for evaluaing link key defined on a class expression
    private Object2IntMap<CandidateLinkkey> linksCounts;
    
    private Int2ObjectMap<IntSet> classes2InstancesDS1;
    private Int2ObjectMap<IntSet> classes2InstancesDS2;
    
    public SupervisedEvalMeasures(LongSet positiveRef, LongSet negativeRef,  Int2ObjectMap<IntSet> classes2InstancesDS1, Int2ObjectMap<IntSet> classes2InstancesDS2) {
        this.positiveRef=positiveRef;
        this.negativeRef=negativeRef;
        this.classes2InstancesDS1=classes2InstancesDS1;
        this.classes2InstancesDS2=classes2InstancesDS2;
        examplesCounts = new Object2IntOpenHashMap<>();
        counterExamplesCounts = new Object2IntOpenHashMap<>();
    }
    
    public SupervisedEvalMeasures(LongSet positiveRef, Int2ObjectMap<IntSet> classes2InstancesDS1, Int2ObjectMap<IntSet> classes2InstancesDS2) {
        this(positiveRef,null,classes2InstancesDS1,classes2InstancesDS2);
        linkedInstancesDS1 = new IntOpenHashSet();
        linkedInstancesDS2 = new IntOpenHashSet();
        positiveRef.forEach( (long l) -> {
            linkedInstancesDS1.add(IntPair.decodeI1(l));
            linkedInstancesDS2.add(IntPair.decodeI2(l));
        
        });
        
    }
     
    // could be more optimized with index on pair of classexp instead of linkkey
    private int computeRefLinks(CandidateLinkkey c) {
        if (classes2InstancesDS1==null || classes2InstancesDS2==null) return positiveRef.size();
        if (linksCounts==null) linksCounts = new Object2IntOpenHashMap<>();
        if (!linksCounts.containsKey(c)) {
            LongSet refLinks = new LongOpenHashSet(positiveRef);
            IntSet iDS1 = EvalMeasures.computeClassExpInstances(c.getClasses1(),classes2InstancesDS1);
            IntSet iDS2 = EvalMeasures.computeClassExpInstances(c.getClasses2(),classes2InstancesDS2);
            LongIterator it = refLinks.iterator();
            while (it.hasNext()) {
                long l = it.nextLong();
                if (!iDS1.contains(IntPair.decodeI1(l)) || !iDS2.contains(IntPair.decodeI2(l))) {
                    it.remove();
                }
            }
            // DEBUG
            /*for (long l : c.getLinks()) {
                if (!iDS1.contains(IntPair.decodeI1(l)) || !iDS2.contains(IntPair.decodeI2(l))) {
                    System.err.println("ERROR. Instance does not belbong to class exp");
                    return 0;
                }
            }*/
            // DEBUG
            linksCounts.put(c, refLinks.size());
        }
        return linksCounts.getInt(c);
    }
    
    private void computeCard(EvaluableRule c) {
        if (! examplesCounts.containsKey(c)) {
            int examplesCount =0;
            int counterExamplesCout =0;
            for (long l : c.getLinks()) {
                if (positiveRef.contains(l)) {
                    examplesCount++;
                }
                else if (negativeRef==null && (linkedInstancesDS1.contains(IntPair.decodeI1(l)) || linkedInstancesDS2.contains(IntPair.decodeI2(l)))) {
                    counterExamplesCout++;
                }
                else if (negativeRef!=null && negativeRef.contains(l)) {
                    counterExamplesCout++;
                }
            }

            examplesCounts.put(c,examplesCount);
            counterExamplesCounts.put(c, counterExamplesCout);
        }
    }
    
    
    public int getNbExamples(EvaluableRule c) {
        return examplesCounts.getInt(c);
    }
    
    public int getNbCounterExamples(EvaluableRule c) {
        return counterExamplesCounts.getInt(c);
    }
    
    public double estimatedPrecision(EvaluableRule c) {
        computeCard(c);
        int examples = examplesCounts.getInt(c);
        int counterExamples = counterExamplesCounts.getInt(c);
        
	return ((double) examples)/(examples+counterExamples);
    }
    
    public double estimatedRecall(EvaluableRule c) {
        computeCard(c);
        int examples = examplesCounts.getInt(c);
	return ((double) examples)/positiveRef.size();
    }
    
    public double estimatedFallout(EvaluableRule c) {
        computeCard(c);
        int examples = examplesCounts.getInt(c);
        int counterExamples = counterExamplesCounts.get(c);
	return ((double) counterExamples)/(examples+counterExamples);
    }
    
   /* private LongSet intersect=null;*/
    public double getPrecision(EvaluableRule c) {
        computeCard(c);
        int examples = examplesCounts.getInt(c);
        return ((double) examples/c.getLinksSize());
    }
    
    public double getRecall(EvaluableRule c) {
        computeCard(c);
        int examples = examplesCounts.getInt(c);
        return ((double) examples/this.positiveRef.size());
    }
    
    public double getRecallClass(CandidateLinkkey c) {
        computeCard(c);
        int examples = examplesCounts.getInt(c);
        return ((double) examples/computeRefLinks(c));
    }
    
    public void clear() {
        examplesCounts.clear();
        counterExamplesCounts.clear();
    }

}
