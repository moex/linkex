package fr.inrialpes.exmo.linkkey.eval;

import fr.inrialpes.exmo.linkkey.AntichainDisj;
import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.EvaluableRule;
import fr.inrialpes.exmo.linkkey.utils.IntPair;
import it.unimi.dsi.fastutil.ints.*;

import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class EvalMeasures {

    private int class1Size;
    private int class2Size;

    private Int2ObjectMap<IntSet> classes2InstancesDS1;
    private Int2ObjectMap<IntSet> classes2InstancesDS2;

    private Map<EvaluableRule, double[]> cacheFPrecRec = new WeakHashMap<>();
    private Map<EvaluableRule, Integer> cacheFunct2 = new WeakHashMap<>();

    public EvalMeasures(int class1Size, int class2Size) {
        this(class1Size, class2Size, Int2ObjectMaps.emptyMap(), Int2ObjectMaps.emptyMap());
    }

    public EvalMeasures(int class1Size, int class2Size, Int2ObjectMap<IntSet> classes2InstancesDS1, Int2ObjectMap<IntSet> classes2InstancesDS2) {
        this.class1Size = class1Size;
        this.class2Size = class2Size;
        this.classes2InstancesDS1 = classes2InstancesDS1;
        this.classes2InstancesDS2 = classes2InstancesDS2;
    }

    public int getClass1Size() {
        return class1Size;
    }

    public int getClass2Size() {
        return class2Size;
    }

    public static IntSet computeClassExpInstances(Set<IntSet> classExp, Int2ObjectMap<IntSet> classes2Instances) {
        if (classes2Instances.isEmpty()) {
            return IntSets.EMPTY_SET;
        }
        IntSet res = new IntOpenHashSet();
        IntSet intersect = new IntOpenHashSet();
        for (IntSet conjunction : classExp) {
            IntIterator it = conjunction.iterator();
            intersect.addAll(classes2Instances.get(it.nextInt()));
            while (it.hasNext()) {
                intersect.retainAll(classes2Instances.get(it.nextInt()));
            }
            res.addAll(intersect);
            intersect.clear();
        }
        return res;
    }

    private int computeClassExpSizeDS1(Set<IntSet> classExp) {
        return computeClassExpInstances(classExp, classes2InstancesDS1).size();
    }

    private int computeClassExpSizeDS2(Set<IntSet> classExp) {
        return computeClassExpInstances(classExp, classes2InstancesDS2).size();
    }

    public double functionalityPhi(EvaluableRule c) {

        int sum = 0;
        for (int v : c.getCounts1().values()) {
            sum += v == 1 ? 1 : 0;
        }
        for (int v : c.getCounts2().values()) {
            //sum+=(1/((double) v));
            sum += v == 1 ? 1 : 0;
        }
        return ((double) sum) / (getClass1Size() + getClass2Size());
    }

    public double functionalityGamma(EvaluableRule c) {
        return ((double) c.getInstances1Size() + c.getInstances2Size()) / (getClass1Size() + getClass2Size());
    }

    public double functionality2(EvaluableRule c) {
        Integer f = cacheFunct2.get(c);
        if (f == null) {
            int sum = 0;
            for (int v : c.getCounts1().values()) {
                sum += 1 - (Math.log(v) / Math.log(2));//2-(2<<v)/(2);//v==1?1:0;//2 - v;//
            }
            for (int v : c.getCounts2().values()) {
                //sum+=(1/((double) v));
                sum += 1 - (Math.log(v) / Math.log(2));//2-(2<<v)/(2);//v==1?1:0;//2 - v;//
            }
            cacheFunct2.put(c, sum);
            f = sum;
        }
        return f;
    }

    public double estPrec(EvaluableRule c) {

        double[] v = cacheFPrecRec.get(c);
        if (v == null) {
            v = new double[]{Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY};
            cacheFPrecRec.put(c, v);
        }
        if (v[0] == Double.NEGATIVE_INFINITY) {
            int sum = 0;
            Int2IntMap c1 = c.getCounts1();
            Int2IntMap c2 = c.getCounts2();
            for (long l : c.getLinks()) {
                boolean i1Func = c1.get(IntPair.decodeI1(l)) == 1;
                boolean i2Func = c2.get(IntPair.decodeI2(l)) == 1;
                if (i1Func && i2Func) {
                    sum += 2;
                } else if (i1Func || i2Func) {
                    sum += 1;
                }
            }
            v[0] = ((double) sum) / (2 * c.getLinksSize());
        }
        return v[0];
    }

    public double estRec(EvaluableRule c) {
        double[] v = cacheFPrecRec.get(c);
        if (v == null) {
            v = new double[]{Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY};
            cacheFPrecRec.put(c, v);
        }
        if (v[1] == Double.NEGATIVE_INFINITY) {
            int sum = 0;
            Int2IntMap c1 = c.getCounts1();
            Int2IntMap c2 = c.getCounts2();
            for (long l : c.getLinks()) {
                boolean i1Func = c1.get(IntPair.decodeI1(l)) == 1;
                boolean i2Func = c2.get(IntPair.decodeI2(l)) == 1;
                if (i1Func && i2Func) {
                    sum += 2;
                } else if (i1Func || i2Func) {
                    sum += 1;
                }
            }
            v[1] = ((double) sum) / (getClass1Size() + getClass2Size());

        }
        return v[1];
    }

    public void clearCaches() {
        cacheFPrecRec.clear();
        cacheFunct2.clear();
    }

    public void clearCaches(EvaluableRule c) {
        cacheFPrecRec.remove(c);
        cacheFunct2.remove(c);
    }

    public double coverageClassExp(EvaluableRule c) {
        int c1Size = this.computeClassExpSizeDS1(c.getClasses1());
        int c2Size = this.computeClassExpSizeDS2(c.getClasses2());
        if (c1Size + c2Size == 0) {
            return Double.NaN;
        }
        return ((double) (c.getInstances1Size() + c.getInstances2Size())) / (c1Size + c2Size);
    }

    public double coverageClassExpMin(EvaluableRule c) {
        int c1Size = this.computeClassExpSizeDS1(c.getClasses1());
        int c2Size = this.computeClassExpSizeDS2(c.getClasses2());
        if (c1Size + c2Size == 0) {
            return Double.NaN;
        }
        return Math.min(((double) c.getInstances1Size()) / c1Size, ((double) c.getInstances2Size()) / c2Size);
    }

    public double coverage(EvaluableRule c) {
        return ((double) (c.getInstances1Size() + c.getInstances2Size())) / (class1Size + class2Size);
    }

    public double coverageMin(EvaluableRule c) {
        return Math.max(((double) c.getInstances1Size()) / class1Size, ((double) c.getInstances2Size()) / class2Size);
    }

    public static double hmean(double n1, double n2) {
        if (Double.isNaN(n1) || Double.isNaN(n2)) {
            return 0;
        }

        double res = 2 * n1 * n2 / (n1 + n2);
        if (Double.isNaN(res)) {
            return 0;
        }
        return res;
    }

    public static double weightedHmean(double n1, double n2, double w1, double w2) {
        if (Double.isNaN(n1) || Double.isNaN(n2)) {
            return 0;
        }

        double res = (w1 + w2) / ((w1 / n1) + (w2 / n2));
        if (Double.isNaN(res)) {
            return 0;
        }
        return res;
    }

    public double hmeanDiscCov(EvaluableRule c) {
        return hmean(discriminability(c), coverage(c));
    }

    public double hmeanDiscCovClass(EvaluableRule c) {
        return hmean(discriminability(c), coverageClassExp((CandidateLinkkey) c));
    }

    public double hmeanDiscCovClassMin(EvaluableRule c) {
        return hmean(discriminability(c), coverageClassExpMin((CandidateLinkkey) c));
    }

    public double specificity(CandidateLinkkey c) {
        return ((double) getSpecificSupport(c)) / getSupport(c);
    }

    public double generality(CandidateLinkkey c) {
        return ((double) (getSupport(c) -getSpecificSupport(c))) / getSupport(c);
    }

    public double avgSharedPairs(CandidateLinkkey c) {

        long sum = c.getSpecificLinks().size()*c.getInPairs().size();
        long sum2 =  c.getSpecificLinks().size();
        for (CandidateLinkkey d : c.getDescendants()) {
            sum += d.getSpecificLinks().size()*d.getInPairs().size();
            sum2 +=  d.getSpecificLinks().size();
        }
        return ((double) sum/sum2);
    }

    public double avgSharedProperties(CandidateLinkkey c) {

        IntSet p1 = new IntOpenHashSet();
        IntSet p2 = new IntOpenHashSet();
        c.getInPairs().forEach((long p) -> { p1.add(IntPair.decodeI1(p)); p2.add(IntPair.decodeI2(p));});
        int cSize = Math.min(p1.size(),p2.size());
        long sum = c.getSpecificLinks().size()*cSize;

        long sum2 =  c.getSpecificLinks().size();
        for (CandidateLinkkey d : c.getDescendants()) {
            p1.clear();p2.clear();
            d.getInPairs().forEach((long p) -> {p1.add(IntPair.decodeI1(p)); p2.add(IntPair.decodeI2(p));});
            sum += d.getSpecificLinks().size()*Math.min(p1.size(),p2.size());
            sum2 +=  d.getSpecificLinks().size();
        }
        return ((double) sum/c.getLinksSize());
    }

    public double computeAvgLinkPerInstance(CandidateLinkkey c) {
        return ((double) (2 * c.getUnionLinks().size())) / (c.getInstances1().size() + c.getInstances2().size());
    }

    public double discriminability(EvaluableRule c) {
        if (c.getLinksSize() == 0) {
            return 1;
        }
        return ((double) Math.min(c.getInstances1Size(), c.getInstances2Size())) / c.getLinksSize();
    }

    public double overlap(CandidateLinkkey c) {
        int c1Size = this.computeClassExpSizeDS1(c.getClasses1());
        int c2Size = this.computeClassExpSizeDS2(c.getClasses2());
        if (Math.min(c1Size, c2Size) == 0) {
            return Double.NaN;
        }
        return (c.getLinksSize() * this.discriminability(c) / (double) Math.min(c1Size, c2Size));
    }

    public double jaccard(CandidateLinkkey c) {
        int c1Size = this.computeClassExpSizeDS1(c.getClasses1());
        int c2Size = this.computeClassExpSizeDS2(c.getClasses2());
        if (Math.min(c1Size, c2Size) == 0) {
            return Double.NaN;
        }
        return (c.getLinksSize() * this.discriminability(c) / ((double) (c1Size + c2Size) - (c.getLinksSize() * this.discriminability(c))));
    }

    public double hmoverlap(EvaluableRule c) {
        return hmean((overlap((CandidateLinkkey) c)), (hmeanDiscCovClass((CandidateLinkkey) c)));
    }

    public double hmoverlapMin(EvaluableRule c) {
        return hmean((overlap((CandidateLinkkey) c)), (hmeanDiscCovClassMin((CandidateLinkkey) c)));
    }

    public double hmjaccard(CandidateLinkkey c) {
        return hmean((jaccard((CandidateLinkkey) c)), (hmeanDiscCovClass((CandidateLinkkey) c)));
    }

    public double discUpperBound(AntichainDisj c) {
        int minLinksToAdd = Math.min(getClass1Size() - c.getInstances1Size(), getClass2Size() - c.getInstances2Size());
        if (getClass1Size() < getClass2Size()) {
            minLinksToAdd = getClass1Size() - c.getInstances1Size();
        } else if (getClass1Size() > getClass2Size()) {
            minLinksToAdd = getClass2Size() - c.getInstances2Size();
        }

        return ((double) Math.min(getClass1Size(), getClass2Size())) / (c.getLinksSize() + minLinksToAdd);
    }

    public double hmeanDiscCovMin(CandidateLinkkey c) {
        return hmean(discriminability(c), coverageMin(c));
    }

    public int getSpecificSupport(CandidateLinkkey c) {
        return c.getSpecificLinks().size();
    }

    public int getSupport(CandidateLinkkey c) {
        return c.getLinksSize();
    }

    public int deltaMeasure(CandidateLinkkey c) {
        int min = c.getLinksSize();
        for (CandidateLinkkey child : c.getChildren()) {
            int current = c.getLinksSize() - child.getLinksSize();
            if (current < min) {
                min = current;
            }
        }
        return min;
    }

    public int deltaMeasureD1(CandidateLinkkey c) {
        int min = c.getInstances1Size();
        for (CandidateLinkkey child : c.getChildren()) {
            int current = c.getInstances1Size() - child.getInstances1Size();
            if (current < min) {
                min = current;
            }
        }
        return min;
    }

    public int deltaMeasureD2(CandidateLinkkey c) {
        int min = c.getInstances2Size();
        for (CandidateLinkkey child : c.getChildren()) {
            int current = c.getInstances2Size() - child.getInstances2Size();
            if (current < min) {
                min = current;
            }
        }
        return min;
    }

    public double lift(CandidateLinkkey c) {
        int topSupp = c.getTop().getLinksSize();

        double pC = ((double) c.getLinksSize()) / topSupp;

        int product = 1;
        for (CandidateLinkkey mother : c.getParents()) {
            product *= mother.getLinksSize();
        }
        double pInd = ((double) product) / (c.getParents().size() * topSupp);

        return pC / pInd;

    }
}
