/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.ecaiexp;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import fr.inrialpes.exmo.linkkey.eval.SupervisedEvalMeasures;
import fr.inrialpes.exmo.linkkey.utils.Pair;
import fr.inrialpes.exmo.linkkey.utils.Utils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.OWL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class EcaiExperiments {

    private Comparator<CandidateLinkkey> comparator = new Comparator<CandidateLinkkey>() {
	    @Override
	    public int compare(CandidateLinkkey c1, CandidateLinkkey c2) {
		return c1.toString().compareTo(c2.toString());
	    }

	};
    
    public final static int INSTANCEREM = 1;
    public final static int TRIPLEREM = 2;
    public final static int SCRAMBLING = 4;

    private final Model dataset1;
    private final Model dataset2;


    private LinkkeyDiscovery disco;

    /**
     * @param datatset1
     * @param dataset2
     * @param usableCrossDSLinkset
     * @param partialReferenceLinkset
     * @param referenceLinkset
     */
    public EcaiExperiments(Model dataset1, Model dataset2, Model usableCrossDSLinkset) {
	super();
	this.dataset1 = dataset1;
	this.dataset2 = dataset2;
	disco = new LinkkeyDiscovery();
	// Object owlsame links loading
	// has to be done first
	disco.indexInputSameAsLinks(Utils.loadSameAsLinks(usableCrossDSLinkset));
    }

    public void altRobustnessExp(int alterationTypes, int startProba, int endProba, int step, int nbRuns, String prefixOutputFile) throws FileNotFoundException {
	int remInstance=0;
	int remTriple=0;
	int scrambleTriple=0;
	if ((alterationTypes&INSTANCEREM)==INSTANCEREM)
	    remInstance=1;
	if ((alterationTypes&TRIPLEREM)==TRIPLEREM)
	    remTriple=1;
	if ((alterationTypes&SCRAMBLING)==SCRAMBLING)
	    scrambleTriple=1;
	
	//System.out.println(remInstance+","+remTriple+","+scrambleTriple);
	
	Map<CandidateLinkkey, int[]> lrCount = new HashMap<CandidateLinkkey, int[]>();
	Map<CandidateLinkkey, double[]> lrDisc = new HashMap<CandidateLinkkey, double[]>();
	Map<CandidateLinkkey, double[]> lrCov = new HashMap<CandidateLinkkey, double[]>();
	Map<CandidateLinkkey, double[]> lrSpec = new HashMap<CandidateLinkkey, double[]>();

	int nbSteps = (endProba - startProba) / step;
	for (int i = 0; i < nbSteps; i++) {
	    double altProba = ((double) (startProba + (i * step))) / 100;
	    for (int j = 0; j < nbRuns; j++) {
		disco.indexDatasets(dataset1, dataset2, remTriple*altProba, remInstance*altProba, scrambleTriple*altProba);

		Collection<CandidateLinkkey> candidates = disco.computeLinkRuleCandidates();
                EvalMeasures eval = new EvalMeasures(disco.getDS1Size(),disco.getDS2Size());
		for (CandidateLinkkey c : candidates) {
		    int[] count = lrCount.get(c);
		    double[] disc = lrDisc.get(c);
		    double[] cov = lrCov.get(c);
		    double[] spec = lrSpec.get(c);
		    if (count == null) {
			lrCount.put(c, count = new int[nbSteps]);
			lrDisc.put(c, disc = new double[nbSteps]);
			lrCov.put(c, cov = new double[nbSteps]);
			lrSpec.put(c, spec = new double[nbSteps]);
		    }
		    count[i]++;
		    disc[i] += eval.discriminability(c);
		    cov[i] += eval.coverage(c);
		    spec[i] += eval.specificity(c);

		}
	    }
	}

	PrintStream out1 = new PrintStream(new File("disc_"+prefixOutputFile+".dat"));
	PrintStream out2 = new PrintStream(new File("cov_"+prefixOutputFile+".dat"));
	//PrintStream out3 = new PrintStream(new File("spec_"+prefixOutputFile+".dat"));

	out1.print("links");
	out2.print("links");
	//out3.print("links");

	ArrayList<CandidateLinkkey> candidates = new ArrayList<CandidateLinkkey>(lrCount.keySet());
	Collections.sort(candidates, comparator);

	for (CandidateLinkkey c : candidates) {
	    out1.print("\t" + c);
	    out2.print("\t" + c);
	    //out3.print("\t" + c);
	}

	for (int i = 0; i < nbSteps; i++) {
	    String level = String.valueOf(startProba + (i * step));
	    out1.println();
	    out2.println();
	    //out3.println();
	    out1.print(level);
	    out2.print(level);
	    //out3.print(level);
	    for (CandidateLinkkey c : candidates) {
		if (lrCount.get(c)[i] > 0) {
		    out1.print("\t" + lrDisc.get(c)[i] / lrCount.get(c)[i]);
		    out2.print("\t" + lrCov.get(c)[i] / lrCount.get(c)[i]);
		    //out3.print("\t" + lrSpec.get(c)[i] / lrCount.get(c)[i]);
		} else {
		    out1.print("\t" + Double.NaN);
		    out2.print("\t" + Double.NaN);
		    //out3.print("\t" + Double.NaN);
		}
	    }
	}
	out1.close();
	out2.close();
	//out3.close();
    }
    

    public void sameAsRobustnessExp(int startProba, int endProba, int step, int nbRuns, String prefixOutputFile, Model partialSameAsRef) throws FileNotFoundException {
	Map<CandidateLinkkey,double[]> precisions = new HashMap<CandidateLinkkey,double[]>();
	Map<CandidateLinkkey,int[]> precCounts = new HashMap<CandidateLinkkey,int[]>();
	
	Map<CandidateLinkkey,double[]> recalls = new HashMap<CandidateLinkkey,double[]>();
	Map<CandidateLinkkey,int[]> recCounts = new HashMap<CandidateLinkkey,int[]>();
	
	disco.indexDatasets(dataset1, dataset2);
	ArrayList<CandidateLinkkey> candidates = new ArrayList<CandidateLinkkey>(disco.computeLinkRuleCandidates());
	Collections.sort(candidates,comparator);
	
	int nbSteps = (endProba - startProba) / step;
	
	for (CandidateLinkkey c : candidates) {
	    precisions.put(c, new double[nbSteps]);
	    precCounts.put(c, new int[nbSteps]);
	    recalls.put(c, new double[nbSteps]);
	    recCounts.put(c, new int[nbSteps]);
	}
	Collection<Pair<String>> partialRefSameAsLinks = Utils.loadSameAsLinks(partialSameAsRef);
	
	for (int j=0 ; j<nbRuns ; j++) {
	for (int i = 0; i < nbSteps; i++) {
	    double removeProba = ((double) (startProba + (i * step))) / 100;

	    // to iterate
	    List<Pair<String>> alteratedPartialRefLinks = new ArrayList<Pair<String>>(partialRefSameAsLinks);
	    Utils.randomlyRemove(alteratedPartialRefLinks, removeProba);
	    disco.setPartialRef(alteratedPartialRefLinks);

	    SupervisedEvalMeasures eval = disco.getEvalMeasures();//disco.evaluateCandidates(candidates);
	    for (CandidateLinkkey c : candidates) {
		double prec = eval.estimatedPrecision(c);
		if (!Double.isNaN(prec)) {
		    precisions.get(c)[i]+=prec;
		    precCounts.get(c)[i]++;
		}
		double rec = eval.estimatedRecall(c);
		if (!Double.isNaN(rec)) {
		    recalls.get(c)[i]+=rec;
		    recCounts.get(c)[i]++;
		}
	    }
	}
	}
	
	PrintStream out1 = new PrintStream(new File("prec_"+prefixOutputFile+".dat"));
	PrintStream out2 = new PrintStream(new File("rec_"+prefixOutputFile+".dat"));
	out1.print("links");
	out2.print("links");
	for (CandidateLinkkey c : candidates) {
	    out1.print("\t"+c);
	    out2.print("\t"+c);
	}
	for (int i = 0; i < nbSteps; i++) {
	    String level = String.valueOf(startProba + (i * step));
	    out1.println();
	    out2.println();
	    out1.print(level);
	    out2.print(level);
	    for (CandidateLinkkey c : candidates) {   
		double estimtedP = precisions.get(c)[i] / precCounts.get(c)[i];
		double estimatedR = recalls.get(c)[i] / recCounts.get(c)[i];
		if (precCounts.get(c)[i]==0) {
		    estimtedP=Double.NaN;
		}
		if (recCounts.get(c)[i]==0) {
		    estimatedR=Double.NaN;
		}
		out1.print("\t" + estimtedP);
		out2.print("\t" + estimatedR);
	    }
	    
	}
	out1.close();
	out2.close();
    }
    
    
    private void linkrulePrecRecEval(String outputFile, Model fullSameAsRef) throws FileNotFoundException {
	PrintStream out = new PrintStream(new File(outputFile));
	disco.indexDatasets(dataset1, dataset2);
	
	Collection<CandidateLinkkey> candidates = disco.computeLinkRuleCandidates();
	
	Set<Pair<Integer>> refSameAsLinks = new HashSet<Pair<Integer>>();
	
	Iterator<Statement> it = fullSameAsRef.listStatements(null, OWL.sameAs, (RDFNode) null);
	while (it.hasNext()) {
	    Statement st = it.next();
	    Pair<Integer> p = new Pair<Integer>(disco.resIdx.getId(st.getSubject().getURI()),disco.resIdx.getId(st.getObject().asResource().getURI()));
	    refSameAsLinks.add(p);
	}
        EvalMeasures eval = new EvalMeasures(disco.getDS1Size(),disco.getDS2Size());
	Utils.printAndEvaluateLinkRuleLatex(candidates, eval,refSameAsLinks, out);
	
	
    }
    
    
    public static void main(String[] args) throws FileNotFoundException {
	if (args.length < 3) {
	    System.err.println("USAGE: java "+EcaiExperiments.class.getName()+" ds1_file ds2_file owlSameAsRef [objectowlsamelinks]");
	}
	
	// Object owlsame links loading
		// has to be done first
		Model objectSameAsLinksModel = ModelFactory.createDefaultModel();
		if (args.length>=4)
		    objectSameAsLinksModel.read(args[3]);
		
		// datasets loading
		Model ds1Model = ModelFactory.createDefaultModel();
		ds1Model.read(args[0]);

		Model ds2Model = ModelFactory.createDefaultModel();
		ds2Model.read(args[1]);
		
		EcaiExperiments exp = new EcaiExperiments(ds1Model,ds2Model,objectSameAsLinksModel);
		
		exp.altRobustnessExp(EcaiExperiments.INSTANCEREM, 0, 100, 10, 10, "instancerem");
		exp.altRobustnessExp(EcaiExperiments.TRIPLEREM, 0, 100, 10, 10, "triplerem");
		exp.altRobustnessExp(EcaiExperiments.SCRAMBLING, 0, 100, 10, 10, "scrambling");
		
		Model sameAsRef = ModelFactory.createDefaultModel();
		sameAsRef.read(args[2]);
		exp.sameAsRobustnessExp(0, 100, 10, 10, "robustness", sameAsRef);
		

		exp.linkrulePrecRecEval("table_prec_rec.txt", sameAsRef);
		
		
		
    }

}
