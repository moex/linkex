/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.ecaiexp;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import fr.inrialpes.exmo.linkkey.eval.SupervisedEvalMeasures;
import fr.inrialpes.exmo.linkkey.utils.*;
import it.unimi.dsi.fastutil.longs.LongArraySet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;

import java.util.*;
import java.util.Map.Entry;

public class LinkkeyDiscovery {

    final StringToId resIdx = new StringToId();
    private final StringToId propIdx = new StringToId();

   /* private int positiveRefLinkSize = 0;
    private MapSet<Integer, Integer> src2dstLinks;
    private MapSet<Integer, Integer> dst2srcLinks;*/
    
    private LongSet partialRef;
    private SupervisedEvalMeasures eval;
    private MapSet<Integer, IntPair> ds1Idx;
    private MapSet<Integer, IntPair> ds2Idx;

    private Set<Integer> ds1Instances;
    private Set<Integer> ds2Instances;

    public LinkkeyDiscovery() {

    }
    
    public int getDS1Size() {
        return ds1Instances.size();
    }
    
    public int getDS2Size() {
        return ds2Instances.size();
    }

    /**
     * Index resources for which a sameAs link is assessed in order that they
     * are associated to the same id (in resIdx)
     *
     * @param sameAsLinks
     */
    public void indexInputSameAsLinks(Iterable<Pair<String>> sameAsLinks) {
        MapSet<String, String> sameas = new MapSet<>();
        for (Pair<String> p : sameAsLinks) {
            sameas.add(p.e1, p.e2);
            sameas.add(p.e2, p.e1);
        }
        resIdx.setOwlSameAsLinks(sameas);
    }

    /**
     * Set the partial reference links that they are used to compute prec and
     * recall
     *
     * @param sameAsLinks
     */
    public void setPartialRef(Iterable<Pair<String>> sameAsLinks) {
        partialRef = new LongOpenHashSet();
         for (Pair<String> p : sameAsLinks) {
            int id1 = resIdx.getId(p.e1);
            int id2 = resIdx.getId(p.e2);
            
            partialRef.add(IntPair.encode(id1,id2));
         }
         eval = new SupervisedEvalMeasures(partialRef,null,null);
        /*src2dstLinks = new MapSet<>();
        dst2srcLinks = new MapSet<>();
        positiveRefLinkSize = 0;
        for (Pair<String> p : sameAsLinks) {
            int id1 = resIdx.getId(p.e1);
            int id2 = resIdx.getId(p.e2);
            src2dstLinks.add(id1, id2);
            dst2srcLinks.add(id2, id1);
            positiveRefLinkSize++;
        }*/
    }

    public void indexDatasets(Model ds1, Model ds2) {
        indexDatasets(ds1, ds2, 0, 0, 0);
    }

    public void indexDatasets(Model ds1, Model ds2, double probaToRemoveTriple, double probaToRemoveInstance, double probaToScramble) {
        ds1Instances = new HashSet<>();
        ds2Instances = new HashSet<>();

        ds1Idx = indexDSIn(ds1, ds1Instances, probaToRemoveTriple, probaToRemoveInstance, probaToScramble);
        ds2Idx = indexDSIn(ds2, ds2Instances, ds1Idx.keySet(), probaToRemoveTriple, probaToRemoveInstance, probaToScramble);

        /*
         * removeAllBranch useless entries of ds1Idx
         */
        Iterator<Entry<Integer, Set<IntPair>>> entriesIt = ds1Idx.entrySet().iterator();
        while (entriesIt.hasNext()) {
            if (!ds2Idx.containsKey(entriesIt.next().getKey())) {
                entriesIt.remove();
            }
        }
        resIdx.storeToDisk();
        propIdx.storeToDisk();
    }

    private MapSet<Integer, IntPair> indexDSIn(Model ds, Set<Integer> instances, double probaToRemoveTriple, double probaToRemoveInstance, double probaToScramble) {
        return indexDSIn(ds, instances, null, probaToRemoveTriple, probaToRemoveInstance, probaToScramble);
    }

    private MapSet<Integer, IntPair> indexDSIn(Model ds, Set<Integer> instances, Set<Integer> inclusionSet, double probaToRemoveTriple, double probaToRemoveInstance, double probaToScramble) {
        Map<String, String> scrambledValues = new HashMap<>();
        Map<String, Boolean> removedInstances = new HashMap<>();

        MapSet<Integer, IntPair> res = new MapSet<>();
        Iterator<Statement> it = ds.listStatements();
        while (it.hasNext()) {
            Statement st = it.next();
            String instance = st.getSubject().getURI();
            if (!Utils.isRemovedInstance(instance, probaToRemoveInstance, removedInstances)) {
                if (probaToRemoveTriple == 0 || 1.0 - Math.random() > probaToRemoveTriple) {

                    String value = st.getObject().isLiteral() ? st.getObject().asLiteral().getLexicalForm().toLowerCase() : st.getObject().asNode().getURI();
                    int oId = resIdx.getId(Utils.getScrambled(""/*st.getPredicate().getURI()*/, value, probaToScramble, scrambledValues));
                    int sId = resIdx.getId(st.getSubject().getURI());
                    instances.add(sId);
                    if (inclusionSet == null || inclusionSet.contains(oId)) {
                        int pId = propIdx.getId(st.getPredicate().getURI());
                        res.add(oId, new IntPair(sId, pId));
                    }
                }
            }
        }
        return res;
    }

    /**
     * Compute Link to Linkrule index
     * Used in EcaiExperiements (Legacy)
     *
     * @return
     */
    public Collection<CandidateLinkkey> computeLinkRuleCandidates() {

        MapSet<Long, Long> instancesPairs2SetOfPropertyPairs = new MapSet<>();
        for (Entry<Integer, Set<IntPair>> ent : ds2Idx.entrySet()) {
            for (IntPair p1 : ds1Idx.get(ent.getKey())) {
                for (IntPair p2 : ent.getValue()) {
                    //IntPair sPair = new IntPair(p1.e1, p2.e1);
                    //IntPair pPair = new IntPair(p1.e2, p2.e2);
                    instancesPairs2SetOfPropertyPairs.add(IntPair.encode(p1.e1, p2.e1), IntPair.encode(p1.e2, p2.e2));
                }
            }
        }

        Map<Set<Long>, CandidateLinkkey> candidates = new HashMap<>();

        for (Map.Entry<Long, Set<Long>> ent : instancesPairs2SetOfPropertyPairs.entrySet()) {
            CandidateLinkkey c = candidates.get(ent.getValue());
            if (c == null) {
                LongSet pairs = new LongArraySet(ent.getValue());
                candidates.put(pairs, c = new CandidateLinkkey(pairs));
            }

            Long sPair = ent.getKey();

            c.addSpecificLink(sPair);

        }

        // with a list is would be better. now nsquare
        for (CandidateLinkkey c1 : candidates.values()) {
            for (CandidateLinkkey c2 : candidates.values()) {
                if (c1 != c2) {
                    if (c2.getInPairs().containsAll(c1.getInPairs())) {
                        //c1.support+=c2.getSpecificSupport();
                        //c1.addOtherLinks(c2.getSpecificLinks());
                        c1.addDescendant(c2);
                    }
                }
            }
        }
        return candidates.values();
    }

    /*public Collection<CandidateLinkkey> evaluateCandidates(Collection<CandidateLinkkey> candidates) {

        // could be easily optimized....
        for (CandidateLinkkey c : candidates) {
            c.resetStats(positiveRefLinkSize);
            for (Long sPair : c.getLinks()) {
                Set<Integer> linked1 = src2dstLinks.get(IntPair.decodeI1(sPair));
                Set<Integer> linked2 = dst2srcLinks.get(IntPair.decodeI2(sPair));
                if (linked1 == null && linked2 == null) {
                    // c.weDoNotKnow.add(sPair.e1);
                    c.weDoNotKnow.add(sPair);
                } else if (linked1 != null && linked1.contains(IntPair.decodeI2(sPair))) {
                    // c.examples.add(sPair.e1);
                    c.examples.add(sPair);
                } else {
                    // c.counterExamples.add(sPair.e1);
                    c.counterExamples.add(sPair);
                }
            }
        }

        return candidates;
    }*/
    
    public SupervisedEvalMeasures getEvalMeasures() {     
        return eval;
    }

    /*public void printLinkRuleStats(Collection<CandidateLinkkey> candidates, Set<IntPair> refLinks, EvalMeasures eval) {
        String[] propUris = propIdx.getStrings();

        System.out.println("ex,ce,avgLinkPerInst,specificSupport,support,precision,recall");

        for (CandidateLinkkey c : candidates) {

            HashSet<IntPair> commonLinks = new HashSet<>(refLinks);
            commonLinks.retainAll(c.getLinks());
            double precision = ((double) commonLinks.size()) / eval.getSupport(c);
            double recall = ((double) commonLinks.size()) / refLinks.size();

            System.out.print(eval.getNbExamples(c) + "," + eval.getNbCounterExamples(c)
                    + "," + eval.computeAvgLinkPerInstance(c)
                    + "," + eval.getSpecificSupport(c) + "," + eval.getSupport(c)
                    + "," + precision + "," + recall
                    + ", [");
            for (Long p : c.getInPairs()) {
                System.out.print("[" + propUris[IntPair.decodeI1(p)] + ";" + propUris[IntPair.decodeI2(p)] + "] ; ");
            }
            System.out.println("]");
        }
    }*/

    /*public void printLinkRule(Collection<CandidateLinkkey> candidates, Set<IntPair> refLinks) {
        String[] propUris = propIdx.getStrings();

        System.out.println("ex,ce,wdk,avgLinkPerInst,specificSupport,support,precision,recall");

        for (CandidateLinkkey c : candidates) {

            HashSet<IntPair> commonLinks = new HashSet<>(refLinks);
            commonLinks.retainAll(c.getLinks());
            double precision = ((double) commonLinks.size()) / EvalMeasures.getSupport(c);
            double recall = ((double) commonLinks.size()) / refLinks.size();

            double confidence = eval.estimatedPrecision(c);//(double) c.examples.size()) / (c.examples.size() + c.counterExamples.size());
            double reliability = ((double) eval.getNbExamples(c) + eval.getNbCounterExamples(c)) / (EvalMeasures.getSupport(c));

            System.out.print(confidence + "," + reliability
                    + "," + EvalMeasures.computeAvgLinkPerInstance(c)
                    + "," + EvalMeasures.getSpecificSupport(c) + "," + EvalMeasures.getSupport(c)
                    + "," + precision + "," + recall
                    + ", [");
            for (Long p : c.getInPairs()) {
                System.out.print("[" + propUris[IntPair.decodeI1(p)] + ";" + propUris[IntPair.decodeI2(p)] + "] ; ");
            }
            System.out.println("]");
        }
    }*/

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.err.println("USAGE: java " + LinkkeyDiscovery.class.getName() + " ds1_file ds2_file [objectowlsamelinks]");
        }

	// Object owlsame links loading
        // has to be done first
        Model objectSameAsLinksModel = ModelFactory.createDefaultModel();
        if (args.length >= 3) {
            objectSameAsLinksModel.read(args[2]);
        }

        // datasets loading
        Model ds1Model = ModelFactory.createDefaultModel();
        ds1Model.read(args[0]);

        Model ds2Model = ModelFactory.createDefaultModel();
        ds2Model.read(args[1]);

        LinkkeyDiscovery disco = new LinkkeyDiscovery();
        disco.indexInputSameAsLinks(Utils.loadSameAsLinks(objectSameAsLinksModel));
        disco.indexDatasets(ds1Model, ds2Model);

        Collection<CandidateLinkkey> candidates = disco.computeLinkRuleCandidates();
        EvalMeasures eval = new EvalMeasures(disco.getDS1Size(),disco.getDS2Size());
        Utils.printLinkRules(candidates, eval,System.out);

    }
}
