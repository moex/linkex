/*
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey;

import fr.inrialpes.exmo.linkkey.utils.ClassExpressions;
import fr.inrialpes.exmo.linkkey.utils.IntPair;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.longs.LongSets;
import org.apache.commons.collections4.collection.CompositeCollection;

import java.util.*;

public class CandidateLinkkey implements EvaluableRule {

    // pairs of properties of the link key candidate
    // pair of properties are represented by long, i.e.
    // concatenation of 2 int representing respectively
    // a property of the first dataset and a property of the second dataset
    protected final LongSet inPairs;
    protected final LongSet eqPairs;

    // class expressions of the candidate link key
    // the set of IntSet represents a disjunction of conjunctions of classes
    // classes are identified by int
    protected Set<IntSet> classes1;
    protected Set<IntSet> classes2;


    protected final LongSet specificLinks;

    transient private Set<CandidateLinkkey> children;
    transient private Set<CandidateLinkkey> parents;

    protected boolean writable;

    // transitive closure of the dual relation - to be cleared when structural change happen
    transient protected Set<CandidateLinkkey> descendants;
    transient protected Set<CandidateLinkkey> ancestors;

    // links stats - to be cleared when structural change or link change happen
    protected transient int nbLinks;
    protected transient int nbInstances1;
    protected transient int nbInstances2;

    protected transient Int2IntMap specificInstances1;
    protected transient Int2IntMap specificInstances2;

    // distances in the lattice - to be clear when structural change happen
    protected transient int minToBottom;
    protected transient int maxToBottom;
    protected transient int minToTop;
    protected transient int maxToTop;


    public CandidateLinkkey() {
        this(LongSets.EMPTY_SET, LongSets.EMPTY_SET, Collections.emptySet(), Collections.emptySet());
    }

    public CandidateLinkkey(LongSet inPairs /*String[] propUris,*/) {
        this(LongSets.EMPTY_SET, inPairs, Collections.emptySet(), Collections.emptySet());
    }

    public CandidateLinkkey(LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2) {
        this(eqPairs, inPairs, classes1, classes2, new LongOpenHashSet());
    }

    public CandidateLinkkey(LongSet eqPairs, LongSet inPairs, LongSet specificLinks) {
        this(eqPairs, inPairs, Collections.emptySet(), Collections.emptySet(), specificLinks);
    }

    CandidateLinkkey(LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2, LongSet specificLinks) {
        this.eqPairs = eqPairs;
        this.inPairs = inPairs;
        this.classes1 = classes1;
        this.classes2 = classes2;
        this.specificLinks = specificLinks;
        children = new HashSet<>();
        parents = new HashSet<>();
        writable = true;
        reset();
        clearCaches();
    }


    protected void resetAllComparable() {
        if (!writable) {
            getDescendants().forEach(CandidateLinkkey::reset);
            for (CandidateLinkkey c : getAncestors()) {
                c.reset();
            }
            reset();
        }
        writable = true;
    }

    protected void reset() {
        nbLinks = -1;
        nbInstances1 = -1;
        nbInstances2 = -1;
        ancestors = null;
        descendants = null;
        minToTop = -1;
        maxToTop = -1;
        minToBottom = -1;
        maxToBottom = -1;
    }

    public final void clearCaches() {
        specificInstances1 = null;
        specificInstances2 = null;
        ancestors = null;
        descendants = null;
    }

    public CandidateLinkkey getTop() {
        if (!parents.isEmpty()) {
            return parents.iterator().next().getTop();
        }
        return this;
    }

    public CandidateLinkkey getBottom() {
        if (!children.isEmpty()) {
            return children.iterator().next().getBottom();
        }
        return this;
    }


    public int getMaxToBottom() {
        if (maxToBottom == -1) {
            if (this == getBottom()) {
                return 0;
            }
            int max = 0;
            for (CandidateLinkkey c : this.getChildren()) {
                max = Math.max(max, c.getMaxToBottom());
            }
            maxToBottom = max + 1;
        }
        return maxToBottom;
    }

    public int getMinToBottom() {
        if (minToBottom == -1) {
            if (this == getBottom()) {
                return 0;
            }
            int min = 0;
            for (CandidateLinkkey c : this.getChildren()) {
                min = Math.min(min, c.getMinToBottom());
            }
            minToBottom = min + 1;
        }
        return minToBottom;
    }


    public int getMinToTop() {
        if (minToTop == -1) {
            if (this == getTop()) {
                return 0;
            }
            int min = Integer.MAX_VALUE;
            for (CandidateLinkkey c : this.getParents()) {
                min = Math.min(min, c.getMinToTop());
            }
            minToTop = min + 1;
        }
        return minToTop;
    }

    public int getMaxToTop() {
        if (maxToTop == -1) {
            if (this == getTop()) {
                return 0;
            }
            int max = 0;
            for (CandidateLinkkey c : this.getParents()) {
                max = Math.max(max, c.getMaxToTop());
            }
            maxToTop = max + 1;
        }
        return maxToTop;
    }

    /**
     * Compute the most specific common ancestor (supremum) of this and c
     * @param c
     * @return supremum of this and c
     */
    public CandidateLinkkey getSup(CandidateLinkkey c) {
        if (equals(c) || getDescendants().contains(c)) return this;
        Set<CandidateLinkkey> current = this.parents;
        Set<CandidateLinkkey> next = new HashSet<>();
        while (!current.isEmpty()) {
            for (CandidateLinkkey x : current) {
                if (x.equals(c) || x.getDescendants().contains(c))  {
                    return x;
                }
                next.addAll(x.parents);
            }
            current=next;
            next = new HashSet<>();
        }
        // should not happen
        return null;
    }

    /**
     * Compute the most general common descendant (infimum) of this and c
     * @param c
     * @return infimum of this and c
     */
    public CandidateLinkkey getInf(CandidateLinkkey c) {
        if (equals(c) || getAncestors().contains(c)) return this;
        Set<CandidateLinkkey> current = this.children;
        Set<CandidateLinkkey> next = new HashSet<>();
        while (!current.isEmpty()) {
            for (CandidateLinkkey x : current) {
                if (x.equals(c) || x.getAncestors().contains(c))  {
                    return x;
                }
                next.addAll(x.children);
            }
            current=next;
            next = new HashSet<>();
        }
        // should not happen
        return null;
    }

    public LongSet getInPairs() {
        return LongSets.unmodifiable(inPairs);
    }

    public LongSet getEqPairs() {
        return LongSets.unmodifiable(eqPairs);
    }

    public Set<IntSet> getClasses1() {
        return Collections.unmodifiableSet(classes1);
    }

    public Set<IntSet> getClasses2() {
        return Collections.unmodifiableSet(classes2);
    }


    public Set<CandidateLinkkey> getChildren() {
        return Collections.unmodifiableSet(children);
    }

    public Set<CandidateLinkkey> getDescendants() {
        if (descendants == null) {
            descendants = new HashSet<>();
            children.forEach(c -> c.addDescendants(descendants));
            writable = false;
        }

        return Collections.unmodifiableSet(descendants);
    }

    private void addDescendants(Set<CandidateLinkkey> descendants) {
        if (!descendants.contains(this)) {
            writable = false;
            descendants.add(this);
            children.forEach(c -> c.addDescendants(descendants));
        }
    }

    public Set<CandidateLinkkey> getParents() {
        return Collections.unmodifiableSet(parents);
    }

    public Set<CandidateLinkkey> getAncestors() {
        if (ancestors == null) {
            ancestors = new HashSet<>();
            parents.forEach(c -> c.addAncestors(ancestors));
            writable = false;
        }
        return Collections.unmodifiableSet(ancestors);
    }

    protected void addAncestors(Set<CandidateLinkkey> ancestors) {
        if (!ancestors.contains(this)) {
            writable = false;
            ancestors.add(this);
            parents.forEach(c -> c.addAncestors(ancestors));
        }
    }

    /*  Methods that modify the structure of the lattice 
        inherited information has to be cleared
    */
    public void removeParent(CandidateLinkkey p) {
        this.resetAllComparable();
        p.resetAllComparable();
        parents.remove(p);
        p.children.remove(this);
    }

    public void addParent(CandidateLinkkey p) {
        this.resetAllComparable();
        p.resetAllComparable();
        parents.add(p);
        p.children.add(this);
    }

    /**
     * Add a descendant to a candidate link key.
     * @param c the candidate link key that will be descendant
     */
    @Deprecated
    public void addDescendant(CandidateLinkkey c) {
        if (c.moreSpecificThanAndNotEquals(this)) {
            boolean toAdd = true;
            Iterator<CandidateLinkkey> it = children.iterator();
            while (it.hasNext()) {
                CandidateLinkkey d = it.next();
                if (d.moreSpecificThanAndNotEquals(c)) {
                    it.remove();
                    d.parents.remove(this);
                    //d.removeParent(this);
                    d.addParent(c);
                } else if (c.moreSpecificThanAndNotEquals(d)) {
                    d.addDescendant(c);
                    toAdd = false;
                }
            }
            if (toAdd) c.addParent(this);
        }
    }

    
    /*  Methods that modify the links of the concept 
        inherited information has to be cleared
    */

    public void addSpecificLink(long link) {
        this.resetAllComparable();
        specificLinks.add(link);
        if (specificInstances1 != null) {
            int i = IntPair.decodeI1(link);
            int nb = specificInstances1.get(i);
            specificInstances1.put(i, nb + 1);
        }
        if (specificInstances2 != null) {
            int i = IntPair.decodeI2(link);
            int nb = specificInstances2.get(i);
            specificInstances2.put(i, nb + 1);
        }
    }

    public LongSet getSpecificLinks() {
        return LongSets.unmodifiable(specificLinks);
    }

    public IntSet getSpecificInstances1() {
        return getSpecificCounts1().keySet();
    }

    public IntSet getSpecificInstances2() {
        return getSpecificCounts2().keySet();
    }

    public IntSet getInstances1() {
        IntOpenHashSet res = new IntOpenHashSet(getSpecificInstances1());
        getDescendants().forEach(c -> res.addAll(c.getSpecificInstances1()));
        res.trim();
        return res;
    }


    public IntSet getInstances2() {
        IntOpenHashSet res = new IntOpenHashSet(getSpecificInstances2());
        getDescendants().forEach(c -> res.addAll(c.getSpecificInstances2()));
        res.trim();
        return res;
    }


    public int getInstances1Size() {
        if (nbInstances1 == -1) {
            nbInstances1 = getInstances1().size();
        }
        return nbInstances1;
    }

    public int getInstances2Size() {
        if (nbInstances2 == -1) {
            nbInstances2 = getInstances2().size();
        }
        return nbInstances2;
    }

    protected Int2IntMap getSpecificCounts1() {
        if (specificInstances1 == null) {
            specificInstances1 = new Int2IntOpenHashMap();
            specificInstances1.defaultReturnValue(0);
            for (long l : this.getSpecificLinks()) {
                int i = IntPair.decodeI1(l);
                int nb = specificInstances1.get(i);
                specificInstances1.put(i, nb + 1);
            }
            ((Int2IntOpenHashMap) specificInstances1).trim();
        }
        return specificInstances1;
    }

    protected Int2IntMap getSpecificCounts2() {
        if (specificInstances2 == null) {
            specificInstances2 = new Int2IntOpenHashMap();
            specificInstances2.defaultReturnValue(0);
            for (long l : this.getSpecificLinks()) {
                int i = IntPair.decodeI2(l);
                int nb = specificInstances2.get(i);
                specificInstances2.put(i, nb + 1);
            }
            ((Int2IntOpenHashMap) specificInstances2).trim();
        }
        return specificInstances2;
    }

    @Override
    public Int2IntMap getCounts1() {
        Int2IntOpenHashMap res = new Int2IntOpenHashMap(getSpecificCounts1());
        getDescendants().forEach(c -> {
            for (Int2IntMap.Entry e : c.getSpecificCounts1().int2IntEntrySet()) {
                int nb = res.get(e.getIntKey());
                res.put(e.getIntKey(), nb + e.getIntValue());
            }
        });
        res.trim();
        return res;
    }

    @Override
    public Int2IntMap getCounts2() {
        Int2IntOpenHashMap res = new Int2IntOpenHashMap(getSpecificCounts2());
        getDescendants().forEach(c -> {
            for (Int2IntMap.Entry e : c.getSpecificCounts2().int2IntEntrySet()) {
                int nb = res.get(e.getIntKey());
                res.put(e.getIntKey(), nb + e.getIntValue());
            }
        });
        res.trim();
        return res;

    }

    public void addLinks(CompositeCollection<Long> l) {
        if (getSpecificLinks().isEmpty() || !l.getCollections().contains(specificLinks)) {
            if (!getSpecificLinks().isEmpty()) l.addComposited(specificLinks);
            children.forEach(c -> c.addLinks(l));
        }
    }

    public CompositeCollection<Long> getUnionLinks() {
        CompositeCollection<Long> res = new CompositeCollection<>();
        getDescendants().forEach(c -> res.addComposited(c.specificLinks));
        return res;

    }

    @Override
    public CompositeCollection<Long> getLinks() {
        CompositeCollection<Long> res = new CompositeCollection<>(this.getSpecificLinks());
        getDescendants().forEach(c -> res.addComposited(c.getSpecificLinks()));
        return res;
    }

    @Override
    public int getLinksSize() {
        if (nbLinks == -1) {
            nbLinks = getLinks().size();
        }
        return nbLinks;
    }


    // Methods for comparing linkkeys and pairs of set of properties     

    @Deprecated
    public boolean moreSpecificThanAndNotEquals(CandidateLinkkey o) {
        return moreSpecificThan(o) && !equals(o);
    }

    @Deprecated
    public boolean moreGeneralThanAndNotEquals(CandidateLinkkey o) {
        return o.moreSpecificThanAndNotEquals(this);
    }

    @Deprecated
    public boolean moreSpecificThan(CandidateLinkkey o) {
        return this.eqPairs.containsAll(o.eqPairs) && this.inPairs.containsAll(o.inPairs);
    }

    @Deprecated
    public boolean moreGeneralThan(CandidateLinkkey o) {
        return o.moreSpecificThan(this);
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.inPairs);
        hash = 29 * hash + Objects.hashCode(this.eqPairs);
        hash = 29 * hash + Objects.hashCode(this.classes1);
        hash = 29 * hash + Objects.hashCode(this.classes2);
        return hash;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CandidateLinkkey other = (CandidateLinkkey) obj;
        if (!Objects.equals(this.inPairs, other.inPairs)) {
            return false;
        }
        if (!Objects.equals(this.eqPairs, other.eqPairs)) {
            return false;
        }
        if (!ClassExpressions.equals(this.classes1, other.classes1)) {
            return false;
        }
        if (!ClassExpressions.equals(this.classes2, other.classes2)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "EQ" + this.eqPairs + ",IN" + this.inPairs + ",C1" + this.classes1 + ",C2" + this.classes2;
    }


}
