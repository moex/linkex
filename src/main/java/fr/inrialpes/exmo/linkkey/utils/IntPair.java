/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;


public class IntPair implements Externalizable {
    
    
    public final static int decodeI1(long p) {
        return (int) (p >>> 32);
    }
    
    public final static int decodeI2(long p) {
        return (int) p;
    }
    
    public final static long encode(int i1, int i2) {
        return (((long) i1 << 32)) | (((long) i2 << 32) >>> 32);
    }
    
    public int e1;
    public int e2;
    
    /**
     * @param e1
     * @param e2
     */
    public IntPair(int e1, int e2) {
	super();
	this.e1 = e1;
	this.e2 = e2;
    }
    
    public IntPair(){};
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + e1;
	result = prime * result + e2;
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	IntPair other = (IntPair) obj;
	if (e1 != other.e1)
	    return false;
	if (e2 != other.e2)
	    return false;
	return true;
    }
    
    @Override
    public String toString() {
	return "("+e1+","+e2+")";
    }
    
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
	out.writeInt(e1);
	out.writeInt(e2);
	
    }
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
	e1=in.readInt();//readInt();
	e2=in.readInt();
	
    }
}
