/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

public class Pair<T> {

    public final T e1;
    public final T e2;
    

    public Pair(T p1, T p2) {
	super();
	this.e1 = p1;
	this.e2 = p2;
    }
    
    
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((e1 == null) ? 0 : e1.hashCode());
	result = prime * result + ((e2 == null) ? 0 : e2.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Pair<?> other = (Pair<?>) obj;
	if (e1 == null) {
	    if (other.e1 != null)
		return false;
	} else if (!e1.equals(other.e1))
	    return false;
	if (e2 == null) {
	    if (other.e2 != null)
		return false;
	} else if (!e2.equals(other.e2))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "["+e1+","+e2+"]";
    }
    
}
