/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OISIterator<E extends Externalizable> implements Iterator<E> {

    private ObjectInputStream ois;
    
    private E next;
    private boolean hasNext;
    
    private Class<E> c;
    
    public OISIterator(Path p, Class<E> c) throws IOException {
	this.ois=new ObjectInputStream(new BufferedInputStream(Files.newInputStream(p)));
	this.c=c;
	readNext();
	
    }
    
    private void readNext() {
	try {
	    //next = c.newInstance();
	    //next.readExternal(ois);
            next = (E)ois.readObject();//next.readExternal(ois);

		    
	    hasNext=true;
	} catch (IOException e) {
	   if (e instanceof EOFException) {
	       hasNext=false;
	       next=null;
	   } else
	       throw new RuntimeException(e);
	   
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}
    }
    
    @Override
    public boolean hasNext() {
	if (hasNext) {
	    if (next==null) readNext();
	    return hasNext;
	}
        try {
            ois.close();
        } catch (IOException ex) {
            Logger.getLogger(OISIterator.class.getName()).log(Level.SEVERE, null, ex);
        }
	return false;
    }

    @Override
    public E next() {
	//if (next==null) 
	E res = next;
	next=null;
	return res;
    }

    @Override
    public void remove() {
	throw new UnsupportedOperationException();
	
    }
    
    @Override
    protected void finalize() throws Throwable {
	ois.close();
	super.finalize();
    }

}
