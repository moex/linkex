package fr.inrialpes.exmo.linkkey.utils.renderer;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.ExtractionResult;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class DotLinkKeyRenderer extends LinkKeyRenderer {


    public DotLinkKeyRenderer(ExtractionResult extraction) {
        super(extraction);
    }

    public void render(PrintWriter out) {
        Object2IntMap<CandidateLinkkey> idx = new Object2IntOpenHashMap<>();
        out.println("digraph G {");
        int i = 0;
        Collection<CandidateLinkkey> candidateLinkkeys = getExtractionResult().getCandidates();
        for (CandidateLinkkey c : candidateLinkkeys) {
            out.print("LK" + i + " [label = \"{" + toDotPropertyPairs(c, true));
            if (!(c.getClasses1().isEmpty() && c.getClasses2().isEmpty())) {
                out.print("|"+toDotClassExpression(c.getClasses1(),true)+","+toDotClassExpression(c.getClasses2(),true));
            }
            /*+ "|"+toStringSpecificLinks(c)*/
            out.println("}\" shape=Mrecord  fontname=Courier];");
            idx.put(c, i);
            i++;
        }
        i = 0;

        for (CandidateLinkkey c : candidateLinkkeys) {
            for (CandidateLinkkey p : c.getChildren()) {
                out.println("LK" + idx.getInt(c) + " -> LK" + idx.getInt(p) + "[arrowhead = none];");
            }
        }
        out.println("}");
    }

    public String toDotPropertyPairs(CandidateLinkkey c, boolean abbrev) {
        LongSet inPairs = new LongOpenHashSet(c.getInPairs());
        LongSet eqPairs = new LongOpenHashSet(c.getEqPairs());

        // \emptyset -> \u2205
        String resIn = "\\\u2205";
        if (!inPairs.isEmpty()) {
            inPairs.removeAll(c.getEqPairs());
            resIn = toStringDot(inPairs, abbrev);
        }

        String resEq = "\\\u2205";
        if (!eqPairs.isEmpty()) {
            resEq = toStringDot(eqPairs, abbrev);
        }



        return resEq + "," + resIn ;
    }

    private String toDotClassExpression(Set<IntSet> classesExp, boolean abbrev) {
        if (classesExp.isEmpty()) return "";
        StringBuilder toString = new StringBuilder();
        Iterator<IntSet> it = classesExp.iterator();
        for (int i=0 ; i<classesExp.size()-1; i++) {
            toString.append(toDotClassConjunction(it.next(),abbrev));
            // \sqcup -> \u2A06
            toString.append('\u2A06');
        }
        toString.append(toDotClassConjunction(it.next(),abbrev));
        return toString.toString();
    }


    private String toDotClassConjunction(IntSet classes, boolean abbrev) {
        String[] tmpString = new String[classes.size()];
        int i = 0;
        for (int c : classes) {
            tmpString[i++] = abbrev ? decodeAbbrevC(c) : decodeC(c);
        }
        Arrays.sort(tmpString);
        StringBuilder toString = new StringBuilder();
        if (tmpString.length>1) {
            toString.append("(");
        }
        for (String s : tmpString) {
            toString.append(s);

            // \sqcap -> \u2A05
            toString.append('\u2A05');
        }
        if (classes.size() > 0) {
            toString.deleteCharAt(toString.length() - 1);
        }
        if (tmpString.length>1) {
            toString.append(")");
        }

        return toString.toString();

    }

    private String toStringDot(LongSet pairs, boolean abbrev) {
        // Compute string representation
        String[] tmpString = new String[pairs.size()];
        int i = 0;
        for (long p : pairs) {
            String p1 = abbrev ? decodeAbbrevP1(p) : decodeP1(p);
            //p1=p1.substring(p1.indexOf('#')+1);

            String p2 = abbrev ? decodeAbbrevP2(p) : decodeP2(p);
            //p2=p2.substring(p2.indexOf('#')+1);
            // \u27E8 -> \langle and \u27E9 -> \rangle
            tmpString[i++] = "\\\u27E8" + p1 + ',' + p2 + "\\\u27E9";
        }
        Arrays.sort(tmpString);
        StringBuilder toString = new StringBuilder();
        toString.append("\\{");
        for (String s : tmpString) {
            toString.append(s);
            toString.append(',');
        }
        if (pairs.size() > 0) {
            toString.deleteCharAt(toString.length() - 1);
        }
        toString.append("\\}");
        return toString.toString();
        // end String representation
    }

}
