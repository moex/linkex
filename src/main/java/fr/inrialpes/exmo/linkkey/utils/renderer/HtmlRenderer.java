/*
 * Copyright (C) 2014-2018 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils.renderer;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.ExtractionResult;
import fr.inrialpes.exmo.linkkey.LinkkeyExtraction;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import fr.inrialpes.exmo.linkkey.eval.SupervisedEvalMeasures;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class HtmlRenderer {
  
    private NumberFormat f;
    private Path rootDir;
    private Path linkDir;

    private Object2IntMap<CandidateLinkkey> candidatesId;

    private HashSet<CandidateLinkkey> generated;
    private int nextId = 0;

    private SupervisedEvalMeasures supEval;
    private EvalMeasures unsupEval;
    private TxtLinkKeyRenderer renderer;

    private boolean renderLinks;

    public HtmlRenderer(Path directory, boolean renderLinks) throws IOException {
        candidatesId = new Object2IntOpenHashMap<>();
        generated = new HashSet<>();
        f = NumberFormat.getInstance(Locale.US);
        f.setMaximumFractionDigits(3);

        this.rootDir = directory;
        Files.createDirectory(directory);
        this.renderLinks = renderLinks;
        if (renderLinks) {
            linkDir = directory.resolve("links");
            Files.createDirectory(linkDir);
        }
    }

    private static void printHeader(PrintWriter out) {
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\">");

        out.println("<script src=\"../resources/js/jquery-latest.js\"></script>");
        out.println("<script src=\"../resources/js/jquery.tablesorter.min.js\"></script>");
        out.println("<script src=\"../resources/js/jquery.tablesorter.js\"></script>");
        out.println("<script src=\"../resources/js/common.js\"></script>");
        out.println("<link rel=\"stylesheet\" href=\"../resources/css/viewCandidates.css\" />");

        out.println("</head>");
    }

    public int getCandidateId(CandidateLinkkey c) {
        int id = candidatesId.getOrDefault(c, nextId);
        if (id == nextId) {
            candidatesId.put(c, id);
            nextId += 1;
        }
        return id;
    }

    private void renderLinksTable(Collection<Long> links, PrintWriter out) {
        out.println("<table class=\"tablesorter\" id=\"tableLinks\" >");
        out.println("<tr><th>DS1</th><th>DS2</th></tr>");
        for (long link : links) {
            out.print("<tr>");
            String u1 = renderer.decodeS1(link);
            String u2 = renderer.decodeS2(link);
            out.print("<td><a href=\"" + u1 + "\">" + u1 + "</a></td><td><a href=\"" + u2 + "\">" + u2 + "</a></td>");
            out.println("</tr>");
        }
        out.println("</table>");
    }

    public void renderSpecificLinks(CandidateLinkkey candidate) throws IOException {
        PrintWriter out = new PrintWriter(Files.newBufferedWriter(linkDir.resolve(getCandidateId(candidate) + "-speclinks.html"), StandardOpenOption.CREATE_NEW));
        out.println("<html>");

        printHeader(out);

        out.println("<body>");
        out.println("<h1>Specific links of link key "+ getCandidateId(candidate) + "</h1>");
        out.println("<p>" + renderer.toString(candidate, true) + "</p>");

        renderLinksTable(candidate.getSpecificLinks(), out);

        out.println("</body>");
        out.println("</html>");
        out.close();
    }
    
        public void renderAllLinks(CandidateLinkkey candidate) throws IOException {
        PrintWriter out = new PrintWriter(Files.newBufferedWriter(linkDir.resolve(getCandidateId(candidate) + "-links.html"), StandardOpenOption.CREATE_NEW));
        out.println("<html>");

        printHeader(out);

        out.println("<body>");
        out.println("<h1>All links of link key "+ getCandidateId(candidate) + "</h1>");
        out.println("<p>" + renderer.toString(candidate, true) + "</p>");

        renderLinksTable(candidate.getLinks(), out);

        out.println("</body>");
        out.println("</html>");
        out.close();
    }

    public void renderHTML(ExtractionResult ls) {
        this.unsupEval = ls.getUnsupervisedEval();
        this.supEval = ls.getSupervisedEval();
        this.renderer = new TxtLinkKeyRenderer(ls);
        renderHTML(ls.getTop());
    }

    private void renderHTML(CandidateLinkkey candidate) {

        generated.add(candidate);
        try {
            int candidateId = getCandidateId(candidate);
            PrintWriter out = new PrintWriter(Files.newBufferedWriter(rootDir.resolve(candidateId + ".html"), StandardOpenOption.CREATE_NEW));
            out.println("<html>");

            printHeader(out);
            
            out.println("<body>");

            out.println("<h1>Link key information</h1>");
            out.println("<p>" + renderer.toString(candidate, true) + "</p>");

            out.println("<p>Discriminability: " + f.format(unsupEval.discriminability(candidate)) + "</p>");
            out.println("<p>Coverage: " + f.format(unsupEval.coverageMin(candidate)) + "</p>");
            
            /*
            Print all links
            */
            out.print("<p>number of ");
            if (renderLinks) {
                renderAllLinks(candidate);
                out.print("<a href=\"links/" + candidateId + "-links.html\">links</a>:");
            }
            else {
                out.print("links:");
            }             
            out.println(f.format(unsupEval.getSupport(candidate)) + "</p>");

            /*
            Print specific links
            */
             out.print("<p>number of ");
            if (renderLinks) {
                renderSpecificLinks(candidate);
                out.print("<a href=\"links/" + candidateId + "-speclinks.html\">specific links</a>:");
            }
            else {
                out.print("specific links:");
            }             
            out.println(f.format(unsupEval.getSpecificSupport(candidate)) + "</p>");
            
            //out.println("<p># <a href=\"links/" + candidateId + "-speclinks.html\">specific links</a>: " + f.format(unsupEval.getSpecificSupport(candidate)) + " (i.e. not generated by a more specific link key)</p>");

            out.println("<h1>List of more specific link keys</h1>");
            out.println("<table class=\"tablesorter\" id=\"table\" columnClasses=\"ID,score,links,disc,cov,candidate1,candidate2\">");
            out.println("<thead>");
            out.print("<tr>"
                    + "<th>ID</th>"
                    + "<th id=\"score\">score"
                    + "<div class=\"slidecontainer\">\n"
                    + "                        <input type=\"range\" min=\"0\" max=\"1\" value=\"0.5\" step=\"0.1\" id=\"rangeSlider\"> </input>\n"
                    + "                        <p>Weight of disc. : <span id=\"val\"></span></p>\n"
                    + "                        <p>Weight of cov. : <span id=\"output2\"></span></p>\n"
                    + "                    </div>"
                    + "<label for=\"scorenum\"> threshold :</label>\n"
                    + "                    <input type=\"number\" class=\"th\" id=\"scorenum\" min=\"0\" max=\"1\" value=\"0\" step=\"0.05\" typeof=\"number\"> </input>\n"
                    + "                    <input type=\"button\" value=\"Reset\" class=\"rbutton\" id=\"scorerst\"> </input>"
                    + "</th>"
                    + "<th id=\"links\"># links"
                    + "<div class=\"threshold\">\n"
                    + "                        <label for=\"linksnum\"> threshold : </label>\n"
                    + "                        <input type=\"number\" class=\"th\" id=\"linksnum\" min=\"0\" step=\"1000\" value=\"0\" typeof=\"number\"> </input>\n"
                    + "                        <input type=\"button\" value=\"Reset\" class=\"rbutton\" id=\"linksrst\"> </input>\n"
                    + "                    </div>\n"
                    + "</th>"
                    + "<th id=\"disc\">disc."
                    + "<div class=\"threshold\">\n"
                    + "                        <label for=\"discnum\"> threshold : </label>\n"
                    + "                        <input type=\"number\" class=\"th\" id=\"discnum\" min=\"0\" max=\"1\" step=\"0.05\" value=\"0\" typeof=\"number\"> </input>\n"
                    + "                        <input type=\"button\" value=\"Reset\" class=\"rbutton\" id=\"discrst\"> </input>\n"
                    + "                    </div>"
                    + "</th>"
                    + "<th id=\"cov\">covmin."
                    + "<div class=\"threshold\">\n"
                    + "                        <label for=\"covnum\"> threshold : </label>\n"
                    + "                        <input type=\"number\" class=\"th\" id=\"covnum\" min=\"0\" max=\"1\" step=\"0.05\" value=\"0\" typeof=\"number\"> </input>\n"
                    + "                        <input type=\"button\" value=\"Reset\" class=\"rbutton\" id=\"covrst\"> </input>\n"
                    + "                    </div>"
                    + "</th>"
            );
            if (supEval != null) {
                out.print("<th>precsion</th><th>recall</th>");
            }
            out.println("<th>candidate</th></tr>");
            out.println("</thead>");
            out.println("<tbody>");
            for (CandidateLinkkey lk : candidate.getChildren()) {
                out.print("<tr>");
                out.print("<td><a href=\"" + getCandidateId(lk) + ".html\">LK" + getCandidateId(lk) + "</a></td><td>" + f.format(unsupEval.hmeanDiscCovMin(lk)) + "</td><td>" + unsupEval.getSupport(lk) + "</td><td>" + f.format(unsupEval.discriminability(lk)) + "</td><td>" + f.format(unsupEval.coverageMin(lk)) + "</td>");
                if (supEval != null) {
                    out.print("<td>" + f.format(supEval.getPrecision(lk)) + "</td>");
                    out.print("<td>" + f.format(supEval.getRecall(lk)) + "</td>");
                }

                out.println("<td>");
                if (!lk.getEqPairs().isEmpty()) {
                    out.println("<p>EQ pairs");
                    out.println("<table border=1>");
                    for (long p : lk.getEqPairs()) {
                        out.print("<tr><td>" + renderer.decodeAbbrevP1(p) + "</td><td>" + renderer.decodeAbbrevP2(p) + "</td></tr>");
                    }
                    out.println("</table></p>");
                }
                if (!lk.getInPairs().isEmpty()) {
                    out.println("<p>IN pairs");
                    out.println("<table border=1>");
                    for (long p : lk.getInPairs()) {

                        out.print("<tr><td>" + renderer.decodeAbbrevP1(p) + "</td><td>" + renderer.decodeAbbrevP2(p) + "</td></tr>");
                    }
                    out.println("</table></p>");
                }
                out.println("</tr>");

            }
            out.println("</tbody>");
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
            out.close();

            for (CandidateLinkkey lk : candidate.getChildren()) {
                if (!generated.contains(lk)) {
                    renderHTML(lk);
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(LinkkeyExtraction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
