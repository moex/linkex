/*
 * Copyright (C) 2014-2020 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import it.unimi.dsi.fastutil.longs.*;
import it.unimi.dsi.fastutil.objects.ObjectIterator;

import java.util.BitSet;
import java.util.Iterator;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class DescriptionsSet {
  
    /**
     * Associate each pair of properties to an int identifier
     */
    private final Long2IntMap propertyPairsIdx;
    
    /**
     * reverse index: the position "id" of this list 
     * gives the pair of property associated
     */
    private final LongList propertyPairs;
    
    /**
     * used to build the maximal description
     */
    private final LongSet inPairs;
    private final LongSet eqPairs;
    
    private final Long2ObjectMap<BitSet[]> descriptions;
    
    //private FileChannel fcIn;
    
    public DescriptionsSet() {
        propertyPairsIdx = new Long2IntOpenHashMap();
        descriptions = new Long2ObjectOpenHashMap<>();
        propertyPairs = new LongArrayList();
        inPairs = new LongOpenHashSet();
        eqPairs = new LongOpenHashSet();
        
        //fcIn.map(FileChannel.MapMode.READ_WRITE, 0, 0);
    }
    
    
    public void putIn(long subjectPair, long propertyPair) {
        inPairs.add(propertyPair);
        put(subjectPair,propertyPair, 0);//descriptionsIn);
    }
    
    public void putEq(long subjectPair, long propertyPair) {
        eqPairs.add(propertyPair);
        put(subjectPair,propertyPair,1);//descriptionsEq);
    }
    
    private void put(long subjectPair, long propertyPair, int pos) {
        BitSet[] tab = descriptions.computeIfAbsent(subjectPair, x -> new BitSet[2]);
        if (tab[pos]==null) {
            tab[pos]=new BitSet();
        }
        tab[pos].set(getPIdx(propertyPair));
    }
    
    private int getPIdx(long pair) {
        int res =  propertyPairsIdx.computeIfAbsent(pair, x-> propertyPairsIdx.size()); 
        if (res==propertyPairs.size()) {
            propertyPairs.add(pair);
        }
        return res;
    }  
    
    
    private void flush() {
        
    }

    /**
     * set[0] are InPairs, set[1] the EqPairs
     * @return
     */
    public Iterator<Long2ObjectMap.Entry<LongSet[]>> entryIterator() {
        return new Iterator<Long2ObjectMap.Entry<LongSet[]>>() {
            
            private ObjectIterator<Long2ObjectMap.Entry<BitSet[]>> it = descriptions.long2ObjectEntrySet().iterator();
            
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            
            LongSet getPairs(BitSet bs) {
                if (bs==null) return LongSets.EMPTY_SET;
                LongSet pairs = new LongOpenHashSet();
                for (int i=bs.nextSetBit(0); i>=0 ; i=bs.nextSetBit(i+1)) {
                    pairs.add(propertyPairs.getLong(i));
                }
                return pairs;
            }

            @Override
            public Long2ObjectMap.Entry<LongSet[]> next() {
                Long2ObjectMap.Entry<BitSet[]> e = it.next();    
                LongSet[] res = new LongSet[]{getPairs(e.getValue()[0]),getPairs(e.getValue()[1])};
                return new AbstractLong2ObjectMap.BasicEntry<>(e.getLongKey(),res);
            }
            
        };
    }
    
    public LongSet getEqPairs() {
        return eqPairs;
    }
    
    public LongSet getInPairs() {
        return inPairs;
    }
}
