/*
 * Copyright (C) 2014-2019 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils.renderer;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.ExtractionResult;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import fr.inrialpes.exmo.linkkey.utils.IntPair;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.ints.IntSets;
import it.unimi.dsi.fastutil.longs.*;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import org.apache.jena.graph.Triple;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.vocabulary.RDF;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class RenderForRedesc {

    private ExtractionResult result;
    private TxtLinkKeyRenderer renderer;
    private Path outputDir;

    private Path ds1;
    private Path ds2;

    private Object2IntMap<String> types1;
    private Object2IntMap<String> types2;

    private Map<String, BitSet> instanceTypes1;
    private Map<String, BitSet> instanceTypes2;

    public RenderForRedesc(ExtractionResult result, TxtLinkKeyRenderer renderer, Path outputDir, Path ds1, Path ds2) {
        this.result = result;
        this.renderer = renderer;
        this.outputDir = outputDir;
        this.ds1 = ds1;
        this.ds2 = ds2;
    }

    public void run() throws IOException {

        indexTypes();
        Files.createDirectories(outputDir);
        int i = 0;

        NumberFormat f = NumberFormat.getInstance();
        f.setMaximumFractionDigits(3);
        EvalMeasures eval = result.getUnsupervisedEval();

        Comparator<CandidateLinkkey> comp = (CandidateLinkkey o1, CandidateLinkkey o2) -> o1.getLinksSize() - o2.getLinksSize();
        ArrayList<CandidateLinkkey> l = new ArrayList<>(result.getCandidates());
        Collections.sort(l, comp.reversed());
        for (CandidateLinkkey c : l) {

            try {

                Path currentDir = outputDir.resolve(String.valueOf(i));
                Files.createDirectory(currentDir);
                //create output files
                PrintWriter linksOut = new PrintWriter(Files.newBufferedWriter(currentDir.resolve("links.txt"), StandardOpenOption.CREATE));
                PrintWriter view1Out = new PrintWriter(Files.newBufferedWriter(currentDir.resolve("view1.txt"), StandardOpenOption.CREATE));

                PrintWriter view2Out = new PrintWriter(Files.newBufferedWriter(currentDir.resolve("view2.txt"), StandardOpenOption.CREATE));

                // print the candidate
                //linksOut.println(renderer.toString(c, true));
                linksOut.println("id" + "\t" + "# links" + "\t" + "# c1" + "\t" + "# c2" + "\t" + "disc." + "\t" + "cov." + "\t" + "candidate");
                linksOut.println(i + "\t" + eval.getSupport(c) + "\t" + c.getInstances1Size() + "\t" + c.getInstances2Size() + "\t" + f.format(eval.discriminability(c)) + "\t" + f.format(eval.coverage(c)) + "\t" + renderer.toString(c, true));

                //print the classes
                renderClasses(types1, view1Out);
                renderClasses(types2, view2Out);

                // print the eqClasses
                List<IntSet[]> eqClasses = computeEqClasses(c);
                BitSet types = new BitSet();
                for (IntSet[] cl : eqClasses) {

                    for (int inst : cl[0]) {
                        // print the eq class
                        linksOut.print(renderer.decodeAbbrevS1((long) inst << 32));
                        linksOut.print(';');

                        String iUri = renderer.decodeS1((long) inst << 32);

                        //System.out.println(types1.get(iUri))
                        if (instanceTypes1.containsKey(iUri)) {
                            types.or(instanceTypes1.get(iUri));
                        }
                        // 
                    }
                    //view1Out.println(types);
                    renderBS(types, types1.size(), view1Out);

                    types.clear();

                    for (int inst : cl[1]) {
                        linksOut.print(renderer.decodeAbbrevS2(inst));
                        linksOut.print(';');

                        String iUri = renderer.decodeS2(inst);
                        if (instanceTypes2.containsKey(iUri)) {
                            types.or(instanceTypes2.get(iUri));
                        }
                    }
                    //view2Out.println(types);
                    renderBS(types, types2.size(), view2Out);
                    types.clear();

                    linksOut.println();
                }
                linksOut.close();
                view1Out.close();
                view2Out.close();

            } catch (IOException ex) {
                Logger.getLogger(RenderForRedesc.class.getName()).log(Level.SEVERE, null, ex);
            }
            i += 1;
        }

    }

    private static void renderBS(BitSet bs, int nb, PrintWriter out) {
        for (int i = 0; i < nb - 1; i++) {
            out.print(bs.get(i) ? '1' : '0');
            out.print(';');
        }
        if (nb > 0) {
            out.print(bs.get(nb - 1) ? '1' : '0');
        }
        out.println();
    }

    private static void renderClasses(Object2IntMap<String> classes, PrintWriter out) {
        if (classes.size() > 0) {
            String[] classesA = new String[classes.size()];
            classes.forEach((c, id) -> classesA[id] = c);

            out.print(classesA[0]);
            for (int i = 1; i < classesA.length; i++) {
                out.print(';');
                out.print(classesA[i]);
            }
        }
        out.println();
    }

    private void indexTypes() {
        try {
            types1 = new Object2IntOpenHashMap<>();
            instanceTypes1 = indexTypes(ds1, types1);
            types2 = new Object2IntOpenHashMap<>();
            instanceTypes2 = indexTypes(ds2, types2);
        } catch (IOException ex) {
            Logger.getLogger(RenderForRedesc.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static Map<String, BitSet> indexTypes(Path ds, Object2IntMap classes) throws IOException {
        int nextId = 0;
        Map<String, BitSet> res = new HashMap<>();
        Iterator<Triple> it = RDFDataMgr.createIteratorTriples(Files.newInputStream(ds), RDFLanguages.filenameToLang(ds.getFileName().toString()), "");
        while (it.hasNext()) {
            Triple t = it.next();

            if (t.getPredicate().getURI().equals(RDF.type.getURI()) && t.getSubject().isURI()) {
                String s = t.getSubject().getURI();
                BitSet types = res.get(s);
                if (types == null) {
                    types = new BitSet();
                    res.put(s, types);
                }
                String classUri = t.getObject().getURI();
                int currentId = classes.getOrDefault(classUri, -1);
                if (currentId == -1) {
                    classes.put(classUri, nextId++);
                }
                types.set(classes.getInt(classUri));
            }
        }
        return res;
    }

    public List<IntSet[]> computeEqClasses(CandidateLinkkey c) {
        List<IntSet[]> partition = new ArrayList<>();
        LongList links = new LongArrayList(c.getLinks());
        LongSet instances = new LongOpenHashSet();

        while (!links.isEmpty()) {
            LongListIterator it = links.iterator();
            IntSet[] eqClass = {new IntOpenHashSet(), new IntOpenHashSet()};
            long l = it.nextLong();
            int i1 = IntPair.decodeI1(l);
            int i2 = IntPair.decodeI2(l);
            eqClass[0].add(i1);
            eqClass[1].add(i2);
            instances.add(i1);
            instances.add(i2);
            it.remove();
            boolean added = false;
            do {
                added = false;
                it = links.iterator();
                while (it.hasNext()) {
                    l = it.nextLong();
                    if (eqClass[0].contains(IntPair.decodeI1(l))) {
                        eqClass[1].add(IntPair.decodeI2(l));
                        instances.add(IntPair.decodeI2(l));
                        it.remove();
                        added = true;
                    } else if (eqClass[1].contains(IntPair.decodeI2(l))) {
                        eqClass[0].add(IntPair.decodeI1(l));
                        instances.add(IntPair.decodeI1(l));
                        it.remove();
                        added = true;
                    }
                }
            } while (added);
            partition.add(eqClass);
        }
        for (int i : result.getInstances1()) {
            if (!instances.contains(i)) {
                partition.add(new IntSet[]{IntSets.singleton(i), IntSets.EMPTY_SET});
            }
        }
        for (int i : result.getInstances2()) {
            if (!instances.contains(i)) {
                partition.add(new IntSet[]{IntSets.EMPTY_SET, IntSets.singleton(i)});
            }
        }
        return partition;

    }

}
