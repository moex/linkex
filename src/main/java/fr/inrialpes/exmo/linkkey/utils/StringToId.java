/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import fr.inrialpes.exmo.linkkey.utils.trie.Trie;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Map Strings to integers ID
 * @author jdavid
 */
public class StringToId {

    private int nextId=0;
    private final Map<String,Integer> index=new Trie<>();
    
    private MapSet<String,String> owlsameas=null;
    
    private  String[] strings;
    private  List<String> stringsCache;
   
    private Path file=null;
    
    private boolean keepStringInMem;

    public StringToId() {
        this(false);
    }
    
    public StringToId(boolean keepStringInMem) {
        
            this.keepStringInMem=keepStringInMem;
            if (keepStringInMem) {
                stringsCache=new ArrayList<>();
            }
    }
    
    public void setOwlSameAsLinks(MapSet<String,String> owlsameas) {
	this.owlsameas=owlsameas;
    }
    
    public void mergeSameAs(String uri1, String uri2) {
   	int id1 = getIdIfExists(uri1);
   	int id2 = getIdIfExists(uri2);
   	if (id2==-1) {
   	    index.put(uri2,getId(uri1));
   	}
   	else if (id1==-1) {
   	    index.put(uri1,getId(uri2));
   	}
        else if (id1!=id2){
            throw new RuntimeException("Both "+uri1+" and "+uri2+" have been indexed before adding owl:sameAs");
        }
     }
    
    private void restoreIdx() {
	if (restoreFromDisk()) {
	    for (int i=0 ; i<strings.length ; i++) {
		index.put(strings[i], i);
	    }
    	}
    }
    
    
    /* SHOULD RECODE SAMEAS HANDLING*/
    public int getId(String str) {
	//restoreIdx();
	int id = getIdIfExists(str);
	if (id==-1) {
            if (nextId==Integer.MAX_VALUE-1) {
                throw new RuntimeException("Index overflow!!!");
            }
	    index.put(str, id=nextId);
            nextId++;
	    if (owlsameas != null) {
		Set<String> eq = owlsameas.get(str);
		if (eq!=null) {
		    for (String s : eq) {
        		index.put(s, id);
        		owlsameas.remove(s);
		    }
		    owlsameas.remove(str);
		}
	    }
            strings=null;
            if (keepStringInMem) {
                stringsCache.add(str);
            }
	}
	return id;
    }
    
    public String getString(int id) {
        if (keepStringInMem) {
            return stringsCache.get(id);
        }
        else if (strings!=null) {
            return strings[id];
        }
        throw new RuntimeException("String cache not activated in this index");
    }
    
    /*
     * Return the id if uri is in index otherwise it returns -1
     */
    public int getIdIfExists(String str) {
	restoreIdx();
	Integer id = index.get(str);
	if (id!=null) {
	    return id;
	}
	return -1;
    }
    
    public int size() {
	return nextId;
    }
    
    public void storeToDisk() {
       try {
	   getStrings();
           file= Files.createTempFile(this.getClass().getName(), ".dat");
           file.toFile().deleteOnExit();
           ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(Files.newOutputStream(file))); 
           oos.writeObject(strings);
           oos.reset();
           oos.close();
           index.clear();
           owlsameas=null;
           strings=null;
       }
       catch (Exception e) {
	   e.printStackTrace();
       }
   }

   @SuppressWarnings("unchecked")
   private boolean restoreFromDisk() {
       if (file==null) return false;
       try {
	ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(Files.newInputStream(file, StandardOpenOption.DELETE_ON_CLOSE)));
	strings=(String[]) ois.readObject();
	ois.close();
	Files.deleteIfExists(file);
        } catch (Exception e) {
    		e.printStackTrace();
        } 
       
       file=null;
       return true;
       
   }
    
    public String[] getStrings() {
	restoreFromDisk();
	if (strings==null)
	    strings = new String[nextId];
	for (Map.Entry<String, Integer> ent : index.entrySet()) {
	    strings[ent.getValue()]=ent.getKey();
	}
	return strings;
    }
    
    @Override
    protected void finalize() throws Throwable{
        if (file!=null)
            Files.deleteIfExists(file);
        super.finalize();
    }
}
