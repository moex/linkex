/*
 * Copyright (C) 2014-2018 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntMaps;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class Int2Id {
    
    private Int2IntMap idx;
    private int nextId;
    
    public Int2Id() {
        idx = new Int2IntOpenHashMap();
        nextId=0;
    }
    
    public int getId(int i) {
        int id = idx.getOrDefault(i, nextId);
        if (id==nextId) {
            idx.put(i,nextId);
            nextId +=1;
        }
        return id;
    }
    
    public Int2IntMap getIndex() {
        return Int2IntMaps.unmodifiable(idx);
    }
    
}
