package fr.inrialpes.exmo.linkkey.utils.renderer;

import fr.inrialpes.exmo.align.impl.Namespace;
import fr.inrialpes.exmo.align.impl.edoal.*;
import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;
import fr.inrialpes.exmo.align.parser.SyntaxElement;
import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.ExtractionResult;
import fr.inrialpes.exmo.linkkey.LinkkeyDiscoveryAlgorithm;
import fr.inrialpes.exmo.ontowrap.BasicOntology;
import it.unimi.dsi.fastutil.ints.IntSet;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.OWL2;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;

import java.io.PrintWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class EdoalLinkKeyRenderer extends LinkKeyRenderer{

    private Set<String> dataProperties;

    private String dataset1;
    private String dataset2;

    public EdoalLinkKeyRenderer(ExtractionResult extraction, String dataset1, String dataset2) {
        super(extraction);

        // useless until now, we should analyse the property to distinguish between data and object properties
        dataProperties = Collections.emptySet();
        this.dataset1=dataset1;
        this.dataset2=dataset2;
    }

    // inverse if needed
    public PathExpression toPathExpressionInverse(String property) {
        if (property.endsWith(LinkkeyDiscoveryAlgorithm.INVERSE_SUFFIX)) {
            return new RelationConstruction(SyntaxElement.Constructor.INVERSE,
                    Collections.singletonList(new RelationId(URI.create(property.substring(0,property.length()-2)))));
        }
        if (dataProperties.contains(property)) {
            return new PropertyId(URI.create(property));
        }
        return new RelationId(URI.create(property));
    }

    public PathExpression toPathExpressionCompose(String property) {
        int e  = property.indexOf('°');
        if (e==-1) {
            return toPathExpressionInverse(property);
        }
        else {
            ArrayList l = new ArrayList<>();
            int s=0;
            while (s<property.length()) {
                String prop = property.substring(s,e);
                l.add(toPathExpressionInverse(prop));
                s=e+1;
                e=property.indexOf('°',s);
                if (e==-1) e=property.length();
            }
            if (l.get(l.size()-1) instanceof RelationExpression) {
                return new RelationConstruction(SyntaxElement.Constructor.COMP,l);
            }
            return new PropertyConstruction(SyntaxElement.Constructor.COMP,l);

        }
    }

    public Linkkey toLinkkey(CandidateLinkkey c) {
        if (dataProperties == null) {
            dataProperties = Collections.emptySet();
        }
        Linkkey lk = new Linkkey();

        for (long p : c.getInPairs()) {//for (String[] p : inPairsString) {
            PathExpression pe1;
            PathExpression pe2;

            String p1 = decodeP1(p);
            String p2 = decodeP2(p);

            pe1 = toPathExpressionCompose(p1);
            pe2 = toPathExpressionCompose(p2);

            lk.addBinding(new LinkkeyIntersects(pe1, pe2));
        }
        for (long p : c.getEqPairs()) {//for (String[] p : eqPairsString) {
            PathExpression pe1;
            PathExpression pe2;

            String p1 = decodeP1(p);
            String p2 = decodeP2(p);

            pe1 = toPathExpressionCompose(p1);
            pe2 = toPathExpressionCompose(p2);
            lk.addBinding(new LinkkeyEquals(pe1, pe2));
        }
        // add namespace
        lk.setExtension(Namespace.EDOAL.prefix, "discriminability", String.valueOf(getEvalMeasures().discriminability(c)));
        lk.setExtension(Namespace.EDOAL.prefix, "coverage", String.valueOf(getEvalMeasures().coverage(c)));
        lk.setExtension(Namespace.EDOAL.prefix, "coverageClassExp", String.valueOf(getEvalMeasures().coverageClassExp(c)));
        return lk;
    }

    public ClassExpression toClassExpression(Set<IntSet> s) throws AlignmentException {
        ArrayList<ClassExpression> disj = new ArrayList<>();
        for (IntSet set : s) {
            ArrayList<ClassExpression> conj = new ArrayList<>();
            for (int i : set) {
                conj.add(new ClassId(decodeC(i)));
            }
            if (conj.size()>1) {
                disj.add(new ClassConstruction(SyntaxElement.Constructor.AND, conj));
            }
            else {
                disj.add(conj.get(0));
            }
        }
        if (disj.size()>1) {
            return new ClassConstruction(SyntaxElement.Constructor.OR, disj);
        }
        return disj.get(0);
    }

    public void render(PrintWriter pw) {
        Collection<CandidateLinkkey> rules = getExtractionResult().getCandidates();

        EDOALAlignment a = new EDOALAlignment();
        BasicOntology o1 = new BasicOntology();
        o1.setURI(URI.create(dataset1));
        o1.setFormalism("OWL2");
        o1.setFormURI(URI.create(OWL2.getURI()));
        BasicOntology o2 = new BasicOntology();
        o2.setURI(URI.create(dataset2));
        o2.setFormalism("OWL2");
        o2.setFormURI(URI.create(OWL2.getURI()));
        try {
            a.init(o1, o2);
            ClassExpression cl1 = new ClassId(OWL.Thing.getURI());
            ClassExpression cl2 = new ClassId(OWL.Thing.getURI());

            //int i=0;
            for (CandidateLinkkey r : rules) {

                Set<IntSet> ce1 = r.getClasses1();
                if (!ce1.isEmpty()) {
                    cl1 = toClassExpression(ce1);
                }
                Set<IntSet> ce2 = r.getClasses2();
                if (!ce2.isEmpty()) {
                    cl2 = toClassExpression(ce2);
                }

                Set<Cell> cells = null;

                // There is a bug in align API...
                try {
                    cells = a.getAlignCells(cl1, cl2);
                }
                catch (NullPointerException e) {
                    cells = Collections.emptySet();
                }
                EDOALCell c = null;
                if (cells.isEmpty()) {
                    c=a.addAlignCell(cl1, cl2);
                }else {
                    c=(EDOALCell)cells.iterator().next();
                }

                c.addLinkkey(toLinkkey(r));
            }
        } catch (AlignmentException ex) {
            throw new RuntimeException("Alignment cannot be initialized", ex);
        }
        RDFRendererVisitor r = new RDFRendererVisitor(pw);
        try {

            r.visit(a);
        } catch (AlignmentException ex) {
            throw new RuntimeException("Alignment cannot be rendered", ex);
        }
        pw.flush();
    }



}
