/*
 * Copyright (C) 2014-2018 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;

import java.util.Comparator;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class LinkkeyDiscComparator implements Comparator<CandidateLinkkey> {

    private EvalMeasures eval;
    
    public LinkkeyDiscComparator(EvalMeasures eval) {
        this.eval=eval;
    }
    @Override
    public int compare(CandidateLinkkey o1, CandidateLinkkey o2) {
        int comp = Double.compare(eval.hmeanDiscCovMin(o1), eval.hmeanDiscCovMin(o2));///o1.inPairs.size() - o2.inPairs.size();
        if (comp == 0) {
            comp = new Double(eval.discriminability(o1)).compareTo(eval.discriminability(o2));
            if (comp == 0) {
                return -1;
            }
        }
        return comp;
    }
}
