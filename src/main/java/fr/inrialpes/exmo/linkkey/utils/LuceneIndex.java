/*
 * Copyright (C) 2014-2019 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.IOUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class LuceneIndex {
    
    
    private Analyzer analyzer = new StandardAnalyzer();

    
    private Directory directory ;
    
    private IndexWriter iwriter;
    private Path indexPath;
    private DirectoryReader ireader;
    private IndexSearcher isearcher;

    private QueryParser qpHelper;
    
    public LuceneIndex() throws IOException {
        indexPath = Files.createTempDirectory("tempIndex");
        directory = FSDirectory.open(indexPath);
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        iwriter = new IndexWriter(directory, config);
        qpHelper = new QueryParser("",analyzer);
        //qpHelper.setSplitOnWhitespace(true);
        /*QueryConfigHandler configP =  qpHelper.getQueryConfigHandler();
        configP.setAllowLeadingWildcard(true);
        configP.setAnalyzer(new WhitespaceAnalyzer());*/
         
        
    }
    
    
    public void index(String s) {
        Document doc = new Document();
        doc.add(new Field("", s, TextField.TYPE_STORED));
        try {
            iwriter.addDocument(doc);
        } catch (IOException ex) {
            Logger.getLogger(LuceneIndex.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeIndex() {
        try {
            iwriter.close();
            ireader.close();
            directory.close();
            IOUtils.rm(indexPath);
        } catch (IOException ex) {
            Logger.getLogger(LuceneIndex.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getClosest(String s) {
        try {
            if (iwriter !=null) {
                iwriter.close();
                iwriter=null;
                ireader = DirectoryReader.open(directory);
                isearcher = new IndexSearcher(ireader);
            }
            //FuzzyQuery query = new FuzzyQuery(new Term("",s));
            s=s.replaceAll(" ", " AND ");
            Query query = qpHelper.createBooleanQuery("",s);//qpHelper.parse("\""+s+"\"~3", "");
             
            ScoreDoc[] hits = isearcher.search(query, 1).scoreDocs;
            if (hits.length>0) {
                return isearcher.doc(hits[0].doc).get("");
            }
           
            // Iterate through the results:
            /*for (int i = 0; i < hits.length; i++) {
              Document hitDoc = isearcher.doc(hits[i].doc);
              
            }*/
            //ireader.close();

        } catch (IOException ex) {
            Logger.getLogger(LuceneIndex.class.getName()).log(Level.SEVERE, null, ex);
        }
         return null;
    }
    
    
}
