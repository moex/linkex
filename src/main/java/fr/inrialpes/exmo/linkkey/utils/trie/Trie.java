/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils.trie;

import java.util.*;

abstract class AbstractNode<V> {
    final Node<V> father;
    Node<V> nextSlibing;

    
    public AbstractNode(Node<V> father, Node<V> nextSlibing) {
	super();
	this.father = father;
	this.nextSlibing = nextSlibing;
    }
    
    protected abstract void addLeaves(Collection<Leaf<V>> leaves);
}

class Node<V> extends AbstractNode<V>{
    final char k;
    AbstractNode<V> firstChild;
    short maxSize=0;

    public Node(char k, Node<V> father, Node<V> nextSlibing) {
	super(father,nextSlibing);
	this.k = k;
	this.firstChild = null;
    }

    @Override
    protected void addLeaves(Collection<Leaf<V>> leaves) {
        if (firstChild!=null) {
            firstChild.addLeaves(leaves);
        }
        if (nextSlibing!=null) {
            nextSlibing.addLeaves(leaves);
        }
    }
}

class Leaf<V> extends AbstractNode<V> {

    V value;
    
    public Leaf(V value, Node<V> father) {
	super(father, (Node<V>)father.firstChild);
	father.firstChild=this;
	this.value=value;
    }

    @Override
    protected void addLeaves(Collection<Leaf<V>> leaves) {
        leaves.add(this);
        if (nextSlibing!=null) {
            nextSlibing.addLeaves(leaves);
        }
    }

}

public class Trie<V> extends AbstractMap<String,V>{

    private int size;
    private Node<V> root;
    private List<Leaf<V>> values;
    private final String prefix;

    public Trie() {
	root=null;
	values=new ArrayList<Leaf<V>>();
	size=0;
        prefix=null;
    }
    
    protected Trie(String prefix, Node<V> root) {
        this.root=root;
        values=new ArrayList<Leaf<V>>();
        root.addLeaves(values);
        size=values.size();
        this.prefix=prefix;
    }
    
    @Override
    public int size() {
	return size;
    }

    @Override
    public boolean containsKey(Object key) {
        if (key !=null && key instanceof String) {
            return getNode((String)key)!=null;
        }
        return false;
    }

    @Override
    public V get(Object key) {
        if (key == null || !(key instanceof String)) return null;
        Node<V> res = getNode((String) key);
        if (res != null && res.firstChild instanceof Leaf) {
            return ((Leaf<V>)res.firstChild).value;
        } 
        return null;
    }
    
    private Node<V> getNode(String key) {
	if (key.length()==0) return null;
        if (prefix!=null) {
            if (!key.startsWith(key)) return null;
            key=key.substring(prefix.length());
        }
	Node<V> current=root;
	int idx=0;
	while (current!=null) {
	    char k = key.charAt(idx);
	    if (current.k==k) {
		if (current.maxSize<key.length()) return null;
		idx++;
		if (idx==key.length()) {
		    return current;
		}
		if (current.firstChild instanceof Leaf) {
		    current=current.firstChild.nextSlibing;
		}
		else {
		    current=(Node<V>)current.firstChild;
		}
	    }
	    else if (k>current.k) {
		current=current.nextSlibing;
	    }
	    else return null;
	}
	return null;
    }
    
    @Override
    public V put(String key, V value) {
	if (value==null) throw new RuntimeException("null values not allowed");
	if (key.length()==0)  throw new RuntimeException("Empty key not allowed");
        if (prefix !=null) {
            if (! key.startsWith(prefix)) throw new RuntimeException("This map only accepts keys with prefix "+prefix);
            key=key.substring(prefix.length());
        }
	if (root==null) {
	    root=new Node<V>(key.charAt(0),null,null);
	}
	Node<V> current=root;
	AbstractNode<V> previous=null;
	Node<V> currentFather=null;
	int idx=0;
	while (true) {
	    char k = key.charAt(idx);
	    if (current.k==k) {
		current.maxSize=(short)Math.max(key.length(), current.maxSize);
		idx++;
		if (idx==key.length()) {
		    if (current.firstChild instanceof Leaf) {
			Leaf<V> dataNode = (Leaf<V>) current.firstChild;
			V oldValue= dataNode.value;
			dataNode.value=value;
			return oldValue;
		    }
		    Leaf<V> dataNode = new Leaf<V>(value,current);
		    values.add(dataNode);
		    size++;
		    return null;
		}
		if (current.firstChild==null) {
		    current.firstChild=new Node<V>(key.charAt(idx),current,null);  
		}
		currentFather=current;
		previous=null;
		if (current.firstChild instanceof Leaf) {
		    if (current.firstChild.nextSlibing==null) {
			current.firstChild.nextSlibing=new Node<V>(key.charAt(idx),current,null);  
		    }
                    previous=current.firstChild;
		    current=current.firstChild.nextSlibing;
		}
		else {
		    current=(Node<V>)current.firstChild;
		}
	    }
	    else if (k>current.k) {
		if (current.nextSlibing==null) {
		    current.nextSlibing=new Node<V>(key.charAt(idx),currentFather,null);  
		}
		previous=current;
		current=current.nextSlibing;
	    }
	    else {
		Node<V> newNode = new Node<V>(k,current.father,current);
		//Node<V> newNode = new Node<V>(k,null,current);
		current=newNode;
		if (previous==null && currentFather!=null) {
		    currentFather.firstChild=current;
		}
		else if (previous!=null) {
		    previous.nextSlibing=current;
		}
		else {
		    root=current;
		}
	    }
	}
    }
    
    @Override
    public boolean containsValue(Object value) {
	throw new UnsupportedOperationException();
    }

    @Override
    public V remove(Object key) {
	throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Map<? extends String, ? extends V> m) {
	m.forEach((k,v) -> {this.put(k, v);});
	
    }

    @Override
    public void clear() {
	root=null;
	
    }

    @Override
    public Set<String> keySet() {
	throw new UnsupportedOperationException();
    }

    @Override
    public Collection<V> values() {
	throw new UnsupportedOperationException();
    }

    @Override
    public Set<Map.Entry<String, V>> entrySet() {
	return new AbstractSet<Map.Entry<String, V>>() {

	    @Override
	    public Iterator<Map.Entry<String, V>> iterator() {
		return new Iterator<Map.Entry<String, V>>() {
		    Iterator<Leaf<V>> it = values.iterator();
		    @Override
		    public boolean hasNext() {
			return it.hasNext();
		    }

		    @Override
		    public java.util.Map.Entry<String, V> next() {
			return new Map.Entry<String, V>() {
			    Leaf<V> node = it.next();
			    String key;
			    @Override
			    public String getKey() {
				if (key==null) {
				    StringBuilder k = new StringBuilder();
				    Node<V> current=node.father;
				    while (current!=null) {
					k.append(current.k);
					current=current.father;
				    }
				    key=k.reverse().toString();
				}
				// TODO Auto-generated method stub
				return key;
			    }

			    @Override
			    public V getValue() {
				return node.value;
			    }

			    @Override
			    public V setValue(V value) {
				V oldValue=node.value;
				node.value=value;
				return oldValue;
			    }
			    
			    @Override
			    public String toString() {
				return "<"+getKey()+","+getValue()+">";
			    }
			};
		    }

		    @Override
		    public void remove() {
			throw new UnsupportedOperationException();
		    }
		    
		};
	    }

	    @Override
	    public int size() {
		return Trie.this.size();
	    }
	
	};
       
    }
    
    public Map<String,V> subTrieView(String prefix) {
           Node<V> res = this.getNode(prefix);
           if (res!=null) {
               return Collections.unmodifiableMap(new Trie<V>(prefix,res));
           }
           return null;
       }
    
    public static void main(String[] args) {
	Trie<String> map = new Trie<String>();
	
	map.put("toto", "toto");
	map.put("titi", "titi");
        map.put("toutes", "toutes");
	map.put("tout", "tout");
	map.put("toutatis", "toutatis");
	map.put("tol", "tol");
	
	map.put("tot", "tit");
        map.put("toto", "tar");
	map.put("abruti", "abruti");
	map.put("zut", "zut");
	map.put("youpi", "youpi");
	map.put("zut", "zit");
	
	System.out.println(map.get("toutes"));
	System.out.println(map.get("tout"));
	System.out.println(map.get("tot"));
	System.out.println(map.get("toto"));
	System.out.println(map.size());
	
	
	System.out.println(map.entrySet());
        Map<String,String> sub = map.subTrieView("z");
        
        System.out.println(sub.entrySet());
        //sub.put("zap", "");
        
        System.out.println(map.entrySet());
        
    }
    

}
