/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

/**
 *
 * @author jdavid
 */
public class SetOPSIndex extends Object2ObjectOpenHashMap<IntOpenHashSet,Int2ObjectOpenHashMap<IntOpenHashSet>>{
    
    public boolean addSetOPS(IntOpenHashSet o, int p, int s) {
        Int2ObjectOpenHashMap<IntOpenHashSet> v1Map = get(o);
        if (v1Map==null) {
            put(o, v1Map=new Int2ObjectOpenHashMap<>());
        }
        IntOpenHashSet v2Set = v1Map.get(p);
        if (v2Set==null) {
            v1Map.put(p, v2Set=new IntOpenHashSet());
        }
        return v2Set.add(s);
    }
}
