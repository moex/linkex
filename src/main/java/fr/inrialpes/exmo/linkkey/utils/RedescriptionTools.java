package fr.inrialpes.exmo.linkkey.utils;

import it.unimi.dsi.fastutil.longs.*;

import java.util.Set;

public class RedescriptionTools {

    private Long2ObjectMap<LongSet> p2linksIdx;
    private Long2ObjectMap<LongSet> c2linksIdx;

    public RedescriptionTools(Long2ObjectMap<LongSet> p2linksIdx,Long2ObjectMap<LongSet> c2linksIdx) {
        this.c2linksIdx=c2linksIdx;
        this.p2linksIdx=p2linksIdx;
    }

    /**
     * Compute the weight of the links among a set of links
     */
    public double computeWeight(long link, LongCollection linkSet) {
        int a = IntPair.decodeI1(link);
        int b = IntPair.decodeI2(link);
        int sumA=0;
        int sumB=0;
        for (long l : linkSet) {
            if (a==IntPair.decodeI1(l)) sumA+=1;
            if (b==IntPair.decodeI2(l)) sumB+=1;
        }
        return 2.0/(sumA+sumB);
    }

    public double[] computeWeights(LongList linkVector) {
        double[] res = new double[linkVector.size()];
        int i=0;
        for (long l : linkVector) {
            res[i++]=computeWeight(l,linkVector);
        }
        return res;
    }


    /**
     * returns the links that are associated to an expression of property pairs.
     * The property pair expression is given in conjunctive normal for (conjunction of disjunction)
     * @return
     */
    public LongSet computePropertyLinks(Set<LongSet> propExp) {
        LongSet res=null;// = new LongOpenHashSet();
        for (LongSet s : propExp) {
            LongSet tmp = new LongOpenHashSet();
            for (long l : s) {
                tmp.addAll(p2linksIdx.get(l));
            }
            if (res==null) {
                res = tmp;
            } else {
                res.retainAll(tmp);
            }
        }
        return res;
    }

    /**
     * returns the links that are associated to an expression of pairs of classes.
     * The class pair expression is given in conjunctive normal for (conjunction of disjunction)
     * @return
     */
    public LongSet computeClassLinks(Set<LongSet> classExp) {
        LongSet res=null;// = new LongOpenHashSet();
        for (LongSet s : classExp) {
            LongSet tmp = new LongOpenHashSet();
            for (long l : s) {
                tmp.addAll(c2linksIdx.get(l));
            }
            if (res==null) {
                res = tmp;
            } else {
                res.retainAll(tmp);
            }
        }
        return res;
    }

    public LongSet getPropertyPairs() {
        return p2linksIdx.keySet();
    }

    public LongSet getClassPairs() {
        return c2linksIdx.keySet();
    }

}
