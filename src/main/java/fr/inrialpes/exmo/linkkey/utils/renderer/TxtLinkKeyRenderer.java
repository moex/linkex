package fr.inrialpes.exmo.linkkey.utils.renderer;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.ExtractionResult;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import fr.inrialpes.exmo.linkkey.eval.SupervisedEvalMeasures;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.longs.LongSet;

import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Set;

public class TxtLinkKeyRenderer extends LinkKeyRenderer {

    public TxtLinkKeyRenderer(ExtractionResult extraction) {
        super(extraction);
    }
    public void render(PrintWriter out) {
        NumberFormat f = NumberFormat.getInstance();
        f.setMaximumFractionDigits(3);

        //out.print("# links" + "\t" + "# c1" + "\t" + "# c2" + "\t" + "deltaMeasure" + "\t" + "deltaMeasure1" + "\t"+ "deltaMeasure2" + "\t"+ "lift" + "\t" + "hmeandc" + "\t" + "disc." + "\t" + "cov."+ "\t" + "covClass."+ "\t" +"fPrec." + "\t" + "fRec.");

        out.print("# links" + "\t" + "# c1" + "\t" + "# c2"+
                //"\t" + "deltaMeasure" + "\t" + "deltaMeasure1" + "\t"+ "deltaMeasure2" + "\t"+ "lift" +
                "\t" + "hmean" + "\t" + "dis" + "\t" + "cov"+ "\t" + "hmeanClass" + "\t" + "disClass" + "\t" + "covClass."+
                "\t" + "overlapClass"+ "\t" + "hmoverlap"+ "\t" + "jaccard"+ "\t" + "hmjaccard"+ "\t" + "covMin"+ "\t" + "hmOverMin"+
                "\t" +"fPrec." + "\t" + "fRec." +
                "\t" +"gene." +"\t" +"other."+"\t" +"other2." );
        if (!getExtractionResult().getReferenceLinks().isEmpty()) {
            out.print("\tf-meas\tprec.\trec.\tf-measClass\trecClass.\test. f-meas\test. prec.\test. rec.");
        }
        out.println("\t" + "EQ"+"\t" + "IN"+"\t" + "CL"+"\t"+"dist to B"+"\t"+"dist to T");

        EvalMeasures eval = getEvalMeasures();
        for (CandidateLinkkey r : getExtractionResult().getCandidates()) {
            //+ "\t" + f.format(eval.estPrec(r)) + "\t" + f.format(eval.estRec(r)) + "\t"+ EvalMeasures.hmean(eval.estPrec(r),eval.estRec(r))
            out.print(f.format(eval.getSupport(r)) + "\t" + f.format(r.getInstances1Size()) + "\t" + f.format(r.getInstances2Size()) +
                    //+ f.format(eval.deltaMeasure(r))+ "\t"+ f.format(eval.deltaMeasureD1(r))+ "\t"+ f.format(eval.deltaMeasureD2(r))+ "\t"+ f.format(eval.lift(r))+ "\t"
                    "\t" + f.format(eval.hmeanDiscCov(r)) + "\t" + f.format(eval.discriminability(r)) + "\t"
                    + f.format(eval.coverage(r)) + "\t" + f.format(eval.hmeanDiscCovClass(r)) + "\t" + f.format(eval.discriminability(r)) + "\t"
                    + f.format(eval.coverageClassExp(r)) + "\t"   + f.format(eval.overlap(r)) + "\t" + f.format(eval.hmoverlap(r)) + "\t"+ f.format(eval.jaccard(r)) + "\t"+ f.format(eval.hmjaccard(r)) + "\t"
                    + f.format(eval.coverageClassExpMin(r)) + "\t"+ f.format(eval.hmoverlapMin(r)) + "\t"

                    + f.format(eval.estPrec(r)) + "\t" + f.format(eval.estRec(r)) + "\t"
                    + f.format(eval.generality(r)) + "\t"+ f.format(eval.avgSharedPairs(r)) + "\t"+ f.format(eval.avgSharedProperties(r)) + "\t");
            if (getExtractionResult().getSupervisedEval() != null) {
                SupervisedEvalMeasures sm = getExtractionResult().getSupervisedEval();
                double prec = sm.getPrecision(r);
                double rec = sm.getRecall(r);
                double recClass = sm.getRecallClass(r);
                out.print(f.format(EvalMeasures.hmean(prec, rec)) + "\t" + f.format(prec) + "\t" + f.format(rec) + "\t");

                out.print(f.format(EvalMeasures.hmean(prec, recClass)) + "\t" + f.format(recClass) + "\t");

                double estPrec = sm.estimatedPrecision(r);
                double estRec = sm.estimatedRecall(r);
                out.print(f.format(EvalMeasures.hmean(estPrec, estRec)) + "\t" + f.format(estPrec) + "\t" + f.format(estRec) + "\t");

            }
            out.print(toString(r, true) + "\t"+r.getMinToBottom()+"\t"+r.getMinToTop());
            out.println();
        }
    }



    private String toString(LongSet pairs, boolean abbrev) {
        // Compute string representation
        String[] tmpString = new String[pairs.size()];
        int i = 0;
        for (long p : pairs) {
            String p1 = abbrev ? decodeAbbrevP1(p) : decodeP1(p);
            //p1=p1.substring(p1.indexOf('#')+1);

            String p2 = abbrev ? decodeAbbrevP2(p) : decodeP2(p);
            //p2=p2.substring(p2.indexOf('#')+1);

            tmpString[i++] = "(" + p1 + ',' + p2 + ")";
        }
        Arrays.sort(tmpString);
        StringBuilder toString = new StringBuilder();
        toString.append('{');
        for (String s : tmpString) {
            toString.append(s);
            toString.append(',');
        }
        if (toString.length() > 1) {
            toString.deleteCharAt(toString.length() - 1);
        }
        toString.append('}');
        return toString.toString();
        // end String representation
    }

    private String toString(IntSet classes, boolean abbrev) {
        String[] tmpString = new String[classes.size()];
        int i = 0;
        for (int c : classes) {
            tmpString[i++] = abbrev ? decodeAbbrevC(c) : decodeC(c);
        }
        Arrays.sort(tmpString);
        StringBuilder toString = new StringBuilder();
        toString.append('{');
        for (String s : tmpString) {
            toString.append(s);
            toString.append(',');
        }
        if (toString.length() > 1) {
            toString.deleteCharAt(toString.length() - 1);
        }
        toString.append('}');
        return toString.toString();
    }

    private String toString(Set<IntSet> classes, boolean abbrev) {
        StringBuilder toString = new StringBuilder();
        toString.append('{');
        for (IntSet s : classes) {
            toString.append(toString(s, abbrev));
            toString.append(',');
        }
        if (toString.length() > 1) {
            toString.deleteCharAt(toString.length() - 1);
        }
        toString.append('}');
        return toString.toString();
    }

    public String toString(CandidateLinkkey c) {
        return toString(c, false);
    }

    public String toString(CandidateLinkkey c, boolean abbrev) {
        return toString(c.getEqPairs(), abbrev) + "\t" + toString(c.getInPairs(), abbrev)
                + "\t[" + toString(c.getClasses1(), abbrev) + "," + toString(c.getClasses2(), abbrev) + "]";
    }
}
