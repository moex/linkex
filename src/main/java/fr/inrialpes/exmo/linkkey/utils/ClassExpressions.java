/*
 * Copyright (C) 2014-2020 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class ClassExpressions {
    
    //  public final static Set<IntSet> OWL_THING=Collections.singleton(IntSets.singleton(-1));
    
    /**
     * computes the union of two disjunctions of conjunctions.
     * It simplifies the result (i.e. removes redundancy)
     * @param s1
     * @param s2
     * @return 
     */
    public static Set<IntSet> union(Set<IntSet> s1, Set<IntSet> s2) {
        //if (s1==OWL_THING) return s1;
        //if (s2==OWL_THING) return s2;
        Set<IntSet> res = new HashSet<>();
        Set<IntSet> copyS2 = new HashSet<>(s2);
        
        nextC1:
        for (IntSet c1 : s1) {
            if (!copyS2.contains(c1)) {
                Iterator<IntSet> it2 = copyS2.iterator();
                while (it2.hasNext()) {
                    IntSet c2 = it2.next();
                    // c1 more specific than c2 -> c1 not added
                    if (c1.containsAll(c2)) {
                        continue nextC1;
                    }
                    else if (c2.containsAll(c1)) {
                        it2.remove();
                    }
                }
                res.add(c1);
            }
        }
        res.addAll(copyS2);
        return res;
    }
    
    /**
     * verifies that s1 contains s2 (s1 more general than s2)
     * @param s1
     * @param s2
     * @return 
     */
    public static boolean contains(Set<IntSet> s1, Set<IntSet> s2) {
        //if (s1==OWL_THING) return true;
        nextC2:
        for (IntSet c2 : s2) {
            if (!s1.contains(c2)) {
                // if s1 does not contains c2
                // we look if exists a c1 in s1 that is more general than c2
                for (IntSet c1 : s1) {
                    // c2 more specific than c1
                    if (c2.containsAll(c1)) {
                        continue nextC2;
                    }
                }
                return false;
            }
        }
        return true;
    }
    
    public static boolean equals(Set<IntSet> s1, Set<IntSet> s2) {
        return contains(s1,s2) && contains (s2,s1);
    }
    
    
    public static void main(String[] args) {
        IntSet x1 = new IntOpenHashSet();
        x1.add(1);
        IntSet x2 = new IntOpenHashSet();
        x2.add(2);
        x2.add(4);
        IntSet x3 = new IntOpenHashSet();
        x3.add(3);
        
        HashSet<IntSet> s1 = new HashSet<>();
        s1.add(x1);
        s1.add(x2);
        s1.add(x3);
        
        
        IntSet y1 = new IntOpenHashSet();
        y1.add(1);
        y1.add(2);
        IntSet y2 = new IntOpenHashSet();
        y2.add(2);
        y2.add(3);
        IntSet y3 = new IntOpenHashSet();
        y3.add(4);
        
        HashSet<IntSet> s2 = new HashSet<>();
        s2.add(y1);
        s2.add(y2);
        s2.add(y3);
        
        HashSet<IntSet> s3 = new HashSet<>();
        s3.add(y1);
        s3.add(y2);
        s3.add(y3);
        
        HashSet<IntSet> s4 = new HashSet<>();
        s4.add(x1);
        s4.add(x3);
        s4.add(y3);
        
        System.out.println("s1="+s1);
        System.out.println("s2="+s2);
        
        Set<IntSet> union = union(s1,s2);
        System.out.println("s1 u s2="+union);
        
        System.out.println(contains(s1,union));
        System.out.println(contains(s2,union));
        System.out.println(contains(s1,s2));
        System.out.println(contains(s2,s1));
        System.out.println(contains(union,s1));
        
        System.out.println(contains(s4,s1));
        System.out.println(contains(s4,s2));
        
        System.out.println(equals(s2,s3));
        
        
    }
    
}
