/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.OWL;

import java.io.PrintStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;


public class Utils {

    public static void randomlyRemove(List<?> list, double probaToRemove) {
	Collections.shuffle(list);
	Iterator<?> it2 = list.iterator();
	while (it2.hasNext()) {
	    it2.next();
	    if (1.0-Math.random()<=probaToRemove) {
		it2.remove();
	    }
	}
    }
    
    
    public static List<Pair<String>> loadSameAsLinks(Model model) {
	List<Pair<String>> links = new ArrayList<Pair<String>>();
	
	Iterator<Statement> it = model.listStatements(null, OWL.sameAs, (RDFNode) null);
	while (it.hasNext()) {
	    Statement st = it.next();
	    Pair<String> p = new Pair<String>(st.getSubject().getURI(),st.getObject().asResource().getURI());
	    links.add(p);
	}
	return links;
    }
    
    public static void tripleRemoval(Model m, double probaToRemove) {
	Iterator<Statement> it = m.listStatements();
	while (it.hasNext()) {
	    it.next();
	    if (1.0-Math.random()<=probaToRemove) {
		it.remove();
	    }
	}
    }
    
    public static void tripleObjectScrambling(Model m, double probaToScramble) {
	Iterator<Statement> it = m.listStatements();
	while (it.hasNext()) {
	    Statement st =  it.next();
	    if (1.0-Math.random()<=probaToScramble) {
		if (st.getObject().isLiteral()) {
		    st.changeObject(UUID.randomUUID().toString());
		}
		else {
		    st.changeObject(m.createResource("http://random.org/"+UUID.randomUUID().toString()));
		}
	    }
	}
    }
    
    public static boolean isRemovedInstance(String instance, double probaToRemove, Map<String,Boolean> removedInstances) {
	if (probaToRemove==0) return false;
	Boolean removed = removedInstances.get(instance);
	if (removed==null) {
	    removedInstances.put(instance, removed=(1.0-Math.random()<=probaToRemove));
	}
	return removed;
	
    }
    
    public static String getScrambled(String property, String value, double scramblingProba, Map<String,String> scrambledValues) {
	if (scramblingProba==0) return value;
	String scrambledValue = scrambledValues.get(property+value);
	if (scrambledValue==null) {
	    if (1.0-Math.random()<=scramblingProba) {
		scrambledValue=UUID.randomUUID().toString();
	    }
	    else {
		scrambledValue=value;
	    }
	    scrambledValues.put(property+value, scrambledValue);
	}
	return scrambledValue;
    }
    
    public static void printAndEvaluateLinkRuleLatex(Collection<CandidateLinkkey> candidates, EvalMeasures eval, Set<Pair<Integer>> refLinks, PrintStream out) {
   	out.println("\\hline \\textbf{Linkrule} & \\textbf{disc} & \\textbf{cov} & \\textbf{specificity} & \\textbf{precision} & \\textbf{recall} \\\\ \\hline ");;
   	
   	
   	for (CandidateLinkkey c : candidates) {
   	    
   	    HashSet<Pair<Integer>> commonLinks = new HashSet<Pair<Integer>>(refLinks);
   	    commonLinks.retainAll(c.getLinks());
   	    double precision = ((double) commonLinks.size())/c.getLinks().size();
   	    double recall = ((double) commonLinks.size())/refLinks.size();
     
   	    DecimalFormat df = new DecimalFormat("#.###");
   	    df.setRoundingMode(RoundingMode.CEILING);

   	   out.print(c.toString());
   	    
   	    out.println(//df.format(confidence)+"&"+df.format(reliability)+
   		    "&"+df.format(eval.discriminability(c))+
   		    "&"+df.format(eval.coverage(c))+
   		    "&"+df.format(eval.specificity(c))+
   		    
   		    "&"+df.format(precision)+"&"+df.format(recall)+//"&"+c.getSupport()+
   		    " \\\\ \\hline");
   	    
   	}
       }
    
    
    public static void printLinkRules(Collection<CandidateLinkkey> candidates, EvalMeasures eval, PrintStream out) {

	out.print("linkrules\t#instances\tdiscriminability\tcoverage\th-mean");
	
	for (CandidateLinkkey c : candidates) {
	   out.println();
	   out.print(c+"\t"+eval.getSupport(c)+"\t"+eval.discriminability(c)+"\t"+eval.coverage(c)+"\t"+eval.hmeanDiscCov(c));
	}
    }
  
}
