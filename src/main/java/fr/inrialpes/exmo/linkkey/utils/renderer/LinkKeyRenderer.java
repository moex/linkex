/*
 * Copyright (C) 2014-2020 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils.renderer;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.ExtractionResult;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import fr.inrialpes.exmo.linkkey.utils.IntPair;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongSet;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.reasoner.TriplePattern;
import org.apache.jena.reasoner.rulesys.ClauseEntry;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;

import java.util.*;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class LinkKeyRenderer {

    private Map<String, String> abbrev;
    private ExtractionResult extraction;
    private EvalMeasures eval;


    public LinkKeyRenderer(ExtractionResult extraction) {
        this.extraction = extraction;
        abbrev = new HashMap<>();
        eval =  new EvalMeasures(extraction.getClass1Size(), extraction.getClass2Size(), extraction.getClasses2InstancesDS1(), extraction.getClasses2InstancesDS2());

    }

    /**
     * Transform the given URI into its abbreviate form "prefix:localName"
     *
     * @param s
     * @return
     */
    private String abbreviate(String s) {
        if (extraction.getPrefixMap() == null) {
            return s;
        }
        String res = abbrev.get(s);
        if (res == null) {
            for (Map.Entry<String, String> e : extraction.getPrefixMap().entrySet()) {
                s = s.replace(e.getValue(), e.getKey() + ":");
            }
            abbrev.put(s, res);
            return s;
        }
        return res;
    }

    public String decodeP1(long p) {
        return extraction.getpIdxDS1()[IntPair.decodeI1(p)];
    }

    public String decodeP2(long p) {
        return extraction.getpIdxDS2()[IntPair.decodeI2(p)];

    }

    public String decodeAbbrevP1(long p) {
        return abbreviate(decodeP1(p));
    }

    public String decodeAbbrevP2(long p) {
        return abbreviate(decodeP2(p));
    }

    public String decodeS1(long p) {
        return extraction.getUris()[IntPair.decodeI1(p)];
    }

    public String decodeS2(long p) {
        return extraction.getUris()[IntPair.decodeI2(p)];
    }

    public String decodeAbbrevS1(long p) {
        return abbreviate(decodeS1(p));
    }

    public String decodeAbbrevS2(long p) {
        return abbreviate(decodeS2(p));
    }

    public String decodeC(int c) {
        return extraction.getUris()[c];
    }

    public String decodeAbbrevC(int c) {
        return abbreviate(decodeC(c));
    }

    public EvalMeasures getEvalMeasures() {
        return eval;
    }

    public ExtractionResult getExtractionResult()  {
        return extraction;
    }



    public Rule toRule(CandidateLinkkey c) {
        return toRule(c, null, null);
    }

    public Rule toRule(CandidateLinkkey c, String className1, String className2) {
        if (!c.getEqPairs().isEmpty()) {
            throw new RuntimeException("Rule that contains Eq properties cannot be transformed into rules");
        }
        List<ClauseEntry> body = new ArrayList<>();

        Node x = NodeFactory.createVariable("x");
        Node y = NodeFactory.createVariable("y");

        if (className1 != null) {
            Node c1 = NodeFactory.createURI(className1);
            body.add(new TriplePattern(x, RDF.type.asNode(), c1));
        }

        if (className2 != null) {
            Node c2 = NodeFactory.createURI(className2);
            body.add(new TriplePattern(y, RDF.type.asNode(), c2));
        }
        int i = 1;
        for (long p : c.getInPairs()) {//for (String[] p : inPairsString) {
            String p1 = decodeP1(p);
            String p2 = decodeP2(p);
            body.add(new TriplePattern(x, NodeFactory.createURI(p1), NodeFactory.createVariable("v" + i)));
            body.add(new TriplePattern(y, NodeFactory.createURI(p2), NodeFactory.createVariable("v" + i)));
            i++;
        }
        return new Rule(Collections.<ClauseEntry>singletonList(new TriplePattern(x, OWL.sameAs.asNode(), y)), body);
    }

    private String[][] getPairsOfStrings(LongSet pairs) {
        String[][] res = new String[pairs.size()][2];
        LongIterator it = pairs.iterator();
        for (int i = 0; i < res.length; i++) {
            long p = it.next();
            res[i][0] = decodeP1(p);
            res[i][1] = decodeP2(p);
        }
        return res;
    }

    public String[][] getEqPairsString(CandidateLinkkey c) {
        return getPairsOfStrings(c.getEqPairs());
    }

    public String[][] getInPairsString(CandidateLinkkey c) {
        return getPairsOfStrings(c.getInPairs());
    }


}
