/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class MapSet<K,T> extends HashMap<K,Set<T>> {

    private static final long serialVersionUID = -6054591065526499597L;

    public MapSet() {
	super();
    }
    

    public boolean add(K key, T value) {
	Set<T> c = this.get(key);
	if (c==null)
	   this.put(key,  c=new HashSet<T>());

	return c.add(value);
    }
    
    public boolean containsValue(K key, T value) {
	return this.get(key)!=null && this.get(key).contains(value);
    }
}
