    /* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.utils;

    import it.unimi.dsi.fastutil.longs.*;

    import java.io.File;
    import java.io.IOException;
    import java.nio.ByteBuffer;
    import java.nio.channels.FileChannel;
    import java.nio.file.StandardOpenOption;
    import java.util.logging.Level;
    import java.util.logging.Logger;

public class FileMapIntPairToSetOfIntPairs {

    private Long2LongMap offsets;
    private Long2IntMap pPairs;

    private File data;
    private FileChannel fc;
    private ByteBuffer buf;

    private long bufStart;
    private final int bufSize = Integer.MAX_VALUE;//2 << 15;

    public FileMapIntPairToSetOfIntPairs() throws IOException {
        data = File.createTempFile(this.getClass().getName(), ".dat");
        data.deleteOnExit();
        offsets = new Long2LongOpenHashMap();
        pPairs = new Long2IntOpenHashMap();
        pPairs.defaultReturnValue(0);
        fc = FileChannel.open(data.toPath(), StandardOpenOption.WRITE, StandardOpenOption.READ, StandardOpenOption.DELETE_ON_CLOSE);

        bufStart = 0;
        buf = fc.map(FileChannel.MapMode.READ_WRITE, bufStart, bufSize);

    }

   private void positionBuffer(long pos) throws IOException {
        if ((pos < bufStart) || (pos >= bufStart + bufSize - (10 * 2 * Integer.BYTES + Byte.BYTES + Long.BYTES))) {
            buf = fc.map(FileChannel.MapMode.READ_WRITE, pos, bufSize);
            bufStart = pos;
        }
        buf.position((int) (pos - bufStart));
    }


    public void put(int s1, int s2, int e1, int e2) throws IOException {
        long key = toLong(s1, s2);
        long previous = offsets.getOrDefault(key, -1l);
        // new Code

        /*int s = 0;
        if (previous == -1l) {
            s = 10;
        } else {
            positionBuffer(previous);
            s = buf.get();
        }
        if (s < 10) {
            //System.out.println("NOT FIRST TIME - key : "+key+"current : "+current);
            buf.position(buf.position() - 1);
            buf.put((byte) (s + 1));
            buf.position(buf.position() + s * 2 * Integer.BYTES);
            buf.putInt( e1);
            buf.putInt(e2);
        } else {
            long current = lastPos;//fc.size();
            lastPos += (10 * 2 * Integer.BYTES + Byte.BYTES + Long.BYTES);
            //System.out.println("FIRST TIME - key : "+key+"current : "+current);
            positionBuffer(current);
            buf.put((byte) 1);
            buf.putInt(e1);
            buf.putInt(e2);
            for (int i = 0; i < 9; i++) {
                buf.putLong(-1l);
            }
            buf.putLong(previous);
            offsets.put(key, current);
        }*/

        //old code
         long current = buf.position() + bufStart;
            pPairs.put(toLong(e1, e2), pPairs.get(toLong(e1, e2)) + 1);
            // if we cannot store 2 int + 1 long
            if (buf.remaining() < 2*Long.BYTES) {
                buf = fc.map(FileChannel.MapMode.READ_WRITE, current, bufSize);
                bufStart = current;
            }

            buf.putInt(e1);
            buf.putInt(e2);
            buf.putLong(previous);
            offsets.put(key, current);
         
    }

    public LongSet keySet() {
        return offsets.keySet();
    }

    public final static long toLong(int i1, int i2) {

        return (((long) i1 << 32)) | (((long) i2 << 32) >>> 32);
    }

    public final static IntPair toIntPair(long l) {
        int i1 = (int) (l >>> 32);
        int i2 = (int) l;
        return new IntPair(i1, i2);
    }

    ///public Set<IntPair> getSet(long key) throws IOException {

       /* HashSet<IntPair> res = new HashSet<>();
        long current = offsets.get(key);
        while (current != -1l) {
            positionBuffer(current);
            byte s = buf.get();
            for (int i = 0; i < s; i++) {
                res.add(new IntPair(buf.getInt(), buf.getInt()));
            }
            if (s < 10) {
                return res;
            } else {
                current = buf.getLong();
            }
        }
        return res;*/

     /*   if (!offsets.containsKey(key)) {
            return null;
        }
        long current = offsets.get(key);
        HashSet<IntPair> res = new HashSet<>();
        while (current != -1l) {
            if (current < bufStart || current + 16 >= bufStart + buf.capacity()) {
                bufStart = Math.max(0, current + 16 - bufSize);
                //System.out.println("reposition buffer at "+bufStart+" - "+current);
                buf = fc.map(FileChannel.MapMode.READ_WRITE, bufStart, bufSize);
            }
            buf.position((int) (current - bufStart));
            int e1 = buf.getInt();
            int e2 = buf.getInt();
            current = buf.getLong();
            if (pPairs.get(toLong(e1,e2))>1)
                res.add(new IntPair(e1, e2));//pPairCache.getPair(e1,e2));//
        }
        return res;
         
    }*/

    public void close() {
        try {
            //dos.close();
            fc.close();
            data.delete();
            //dos=null;
            fc = null;
            data = null;
        } catch (IOException ex) {
            Logger.getLogger(FileMapIntPairToSetOfIntPairs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void finalize() throws Throwable {
        if (fc != null) {
            //dos.close();
            fc.close();
            data.delete();
        }
        super.finalize();
    }
}
