/*
 * Copyright (C) 2014-2020 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey;
import java.util.Collections;
import java.util.Set;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class ExtractionConfig {

    /** Link key candidates with no classes expressions */
    public static final int NO_CLASSES=0;
    /** Link key candidates with classes expressions but subsumption defined
     * by the inclusion between the sets of pairs of properties only */
    public static final int CLASSES=1;
    /** Link key candidates with classes expressions but subsumption defined
     * by the inclusion between the sets of pairs of properties
     * and classes expressions */
    public static final int CLASSES_FULL=2;
    
    
    // extract IN candidates
    public final boolean in;
    // extract EQ candidates
    public final boolean eq;
    // extract classes candidates
    public final int classes;
    
    // generate inverse of properties
    public final boolean inverse;
    // composition length
    public final int compose;
    
    /*
    * types of instances to be added in subjectDSx
    * if type is null than any instance that appear as subjetc is added
    */
    public final Set<String> typesDS1;
    public final Set<String> typesDS2;
    
    /*
    * prefix of types of instances to be added in subjectDSx
    * if type is null then no filter is used
    */
    public String typesPrefixDS1;
    public String typesPrefixDS2;
            
    public final double supportThreshold;
    public final int maxOSFactor; 
    public final double discThreshold;
    public final boolean filterBlankNodes;
    
    
    public ExtractionConfig(boolean in, boolean eq, int classes,
            boolean inverse, int compose, 
            Set<String> typesDS1, Set<String> typesDS2,
            String typesPrefixDS1, String typesPrefixDS2,
            double supportThreshold, int maxOSFactor, double discThreshold, boolean filterBlankNodes) {
        this.in = in;
        this.eq = eq;
        this.classes=classes;
        this.inverse = inverse;
        this.compose = compose;
        this.typesDS1=typesDS1;
        this.typesDS2=typesDS2;
        this.typesPrefixDS1=typesPrefixDS1;
        this.typesPrefixDS2=typesPrefixDS2;
        this.supportThreshold=supportThreshold;
        this.maxOSFactor=maxOSFactor;
        this.discThreshold=discThreshold;
        this.filterBlankNodes=filterBlankNodes;
    }
    
    public ExtractionConfig(boolean in, boolean eq, int classes,
            boolean inverse, int compose, String typeDS1, String typeDS2, 
            double supportThreshold, int maxOSFactor, double discThreshold, boolean filterBlankNodes) {
        this(in, eq, classes, inverse, compose, Collections.singleton(typeDS1), Collections.singleton(typeDS2),null, null, supportThreshold, maxOSFactor, discThreshold, filterBlankNodes);
    }
    
    public ExtractionConfig(boolean in, boolean eq, int classes, boolean inverse, int compose) {
        this(in,eq,classes,inverse,compose,(Set)null,(Set)null,null,null,.0,10,1,false);
    }
}
