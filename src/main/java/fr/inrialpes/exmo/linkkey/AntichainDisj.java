/*
 * Copyright (C) 2014-2019 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey;

import fr.inrialpes.exmo.linkkey.utils.ClassExpressions;
import fr.inrialpes.exmo.linkkey.utils.IntPair;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;

import java.util.*;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class AntichainDisj implements EvaluableRule {

    private boolean isMaximal=false;
    
    private List<CandidateLinkkey> candidates;
    //private List<LongSet> links;

    private Int2IntMap counts1;
    private Int2IntMap counts2;
    
    private Set<LongSet> links;

    private int nbLinks;
    private int nbInstances1;
    private int nbInstances2;

    public AntichainDisj(CandidateLinkkey c) {
        candidates = Collections.singletonList(c);
        links = new HashSet<>();
        counts1 = new Int2IntOpenHashMap();
        counts2 = new Int2IntOpenHashMap();
        nbLinks=0;
        addLinksFromCandidate(c);
        ((Int2IntOpenHashMap) counts1).trim();
        ((Int2IntOpenHashMap) counts2).trim();
        nbInstances1=counts1.size();
        nbInstances2=counts2.size();
    }

    public AntichainDisj(AntichainDisj prefix, CandidateLinkkey c/*, boolean cache*/) {
        if (prefix.candidates.contains(c)) {
            throw new IllegalArgumentException("Candidate linkey already in the chain");
        }
        if (prefix.counts1==null) prefix.reload();;
        candidates = new ArrayList<>(prefix.candidates);
        candidates.add(c);
        
        links = new HashSet<>(prefix.links);
        counts1 = new Int2IntOpenHashMap(prefix.counts1);
        counts2 = new Int2IntOpenHashMap(prefix.counts2);
        nbLinks=prefix.nbLinks;
        addLinksFromCandidate(c);
        ((Int2IntOpenHashMap) counts1).trim();
        ((Int2IntOpenHashMap) counts2).trim();
        nbInstances1=counts1.size();
        nbInstances2=counts2.size();
    }
    
    public AntichainDisj(Collection<CandidateLinkkey> lks) {
        candidates= new ArrayList<>(lks);
        links = new HashSet<>();
        nbLinks=0;
        counts1 = new Int2IntOpenHashMap();
        counts2 = new Int2IntOpenHashMap();
        candidates.forEach(c->addLinksFromCandidate(c));
        ((Int2IntOpenHashMap) counts1).trim();
        ((Int2IntOpenHashMap) counts2).trim();
        nbInstances1=counts1.size();
        nbInstances2=counts2.size();
        
    }
    
    /*@Override
    public int getNbAncestors() {
        Iterator<CandidateLinkkey> it = candidates.iterator();
        int max=it.next().getNbAncestors();
        while (it.hasNext()) {
            max = Math.max(max, it.next().getNbAncestors());
        }
        return max;
    }
    
    @Override
    public int getMaxNbAncestors() {
        Iterator<CandidateLinkkey> it = candidates.iterator();
        return it.next().getBottom().getNbAncestors();
    
    }
    
    @Override
    public int getNbDescendants() {
        Iterator<CandidateLinkkey> it = candidates.iterator();
        int min=it.next().getNbAncestors();
        while (it.hasNext()) {
            min = Math.min(min, it.next().getNbDescendants());
        }
        return min;
    }
    
    @Override
    public int getMaxNbDescendants() {
        Iterator<CandidateLinkkey> it = candidates.iterator();
        return it.next().getBottom().getNbDescendants();
    
    }*/
    
    public void setIsMaximal(boolean b) {
        isMaximal=b;
    }
    
    public boolean isMaximal() {
        return isMaximal;
    }
    
    private void addLinksFromCandidate(CandidateLinkkey c) {      
        Set<CandidateLinkkey> s = new HashSet<>(c.getDescendants());
        s.add(c);
        for (CandidateLinkkey d : s) {
            LongSet specLinks=d.getSpecificLinks();
            if (links.add(specLinks)) {
                nbLinks+=specLinks.size();
                for (long l : specLinks) {
                    addLink(l);
                }
            }
        }
    }
    
    /**
     * Only add links for instances that are not already linked
     * @param c 
     */
    private void addLinksFromCandidate2(CandidateLinkkey c) {
        LongSet toAdd = new LongOpenHashSet();
        for (long l : c.getLinks()) {
            if (!counts1.containsKey(IntPair.decodeI1(l)) && !counts2.containsKey(IntPair.decodeI2(l))) {
                toAdd.add(l);
            }
        }
        if (links.add(toAdd)) {
            nbLinks += toAdd.size();
            for (long l : toAdd) {
                addLink(l);
            }
        }
    }

    private void addLink(long l) {
        int i1 = IntPair.decodeI1(l);
        counts1.put(i1, counts1.get(i1) + 1);
        int i2 = IntPair.decodeI2(l);
        counts2.put(i2, counts2.get(i2) + 1);
    }

    public List<CandidateLinkkey> getCandidates() {
        return candidates;
    }
    
    private void reload() {
        links = new HashSet<>();
        counts1 = new Int2IntOpenHashMap();
        counts2 = new Int2IntOpenHashMap();
        nbLinks=0;
        for (CandidateLinkkey c : candidates) {
            addLinksFromCandidate(c);
        }
        nbInstances1=counts1.size();
        nbInstances2=counts2.size();
        ((Int2IntOpenHashMap) counts1).trim();
        ((Int2IntOpenHashMap) counts2).trim();
    }

    public void freeMem(){
        counts1=null;
        counts2=null;
        links=null;
    }
    
    public IntSet getInstances1() {
        if (counts1==null) reload();
        return counts1.keySet();
    }
    
    public int getInstances1Size() {
        return nbInstances1;
    }

    public IntSet getInstances2() {
        if (counts2==null) reload();
        return counts2.keySet();
    }

    public int getInstances2Size() {
        return nbInstances2;
    }
    
    public Int2IntMap getCounts1() {
        if (counts1==null) reload();
        return counts1;
    }

    public Int2IntMap getCounts2() {
        if (counts2==null) reload();
        return counts2;
    }

    public double sumFunctionalities() {
        if (counts1==null || counts2==null) reload();
        double sum = 0;
        for (int v : counts1.values()) {
            sum += 1 - (Math.log(v) / Math.log(2));
        }
        for (int v : counts2.values()) {
            //sum+=(1/((double) v));
            sum += 1 - (Math.log(v) / Math.log(2));
        }
        return sum;
    }

    public int getLinksSize() {
        return nbLinks;
    }


    public LongSet getLinks() {
        if (links==null) reload();
        LongSet s = new LongOpenHashSet(nbLinks);
        //candidates.forEach(c->s.addAll(c.getLinks()));
        links.forEach(l->s.addAll(l));
        return s;
    }

    @Override
    public int hashCode() {
        return candidates.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AntichainDisj other = (AntichainDisj) obj;
        if (this.candidates.size() != other.candidates.size()) {
            return false;
        }
        for (CandidateLinkkey e : this.candidates) {
            if (!other.candidates.contains(e)) {
                return false;
            }
        }
        return true;
    }

    //TODO: implement this with cache
    @Override
    public Set<IntSet> getClasses1() {
        Set<IntSet> res = Collections.EMPTY_SET;
        for (CandidateLinkkey c : this.candidates) {
            res=ClassExpressions.union(res, c.getClasses1());
        }
        return res;
    }

    @Override
    public Set<IntSet> getClasses2() {
        Set<IntSet> res = Collections.EMPTY_SET;
        for (CandidateLinkkey c : this.candidates) {
            res=ClassExpressions.union(res, c.getClasses2());
        }
        return res;
    }



}
