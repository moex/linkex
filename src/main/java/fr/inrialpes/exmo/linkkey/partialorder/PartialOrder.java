/*
 * Copyright (C) 2014-2020 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.partialorder;

import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.longs.LongSet;

import java.util.Set;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public abstract class PartialOrder {
    
    public static final PartialOrder PROPERTY_PAIRS = new PropertyPairsOrder();
    public static final PartialOrder PROPERTY_PAIRS_AND_CLASS_EXP = new PropertyPairsAndClassExpOrder();
    
   
    public abstract boolean contains(LongSet eqPairsA, LongSet inPairsA, Set<IntSet> classes1A, Set<IntSet> classes2A,
                                     LongSet eqPairsB, LongSet inPairsB, Set<IntSet> classes1B, Set<IntSet> classes2B
    );
    
    public boolean contains(LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2, CandidateLinkkey c) {
        return contains(eqPairs,inPairs,classes1,classes2,c.getEqPairs(),c.getInPairs(),c.getClasses1(),c.getClasses2());
    }
    
    public boolean contains(CandidateLinkkey c, LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2) {
        return contains(c.getEqPairs(),c.getInPairs(),c.getClasses1(),c.getClasses2(),eqPairs,inPairs,classes1,classes2);
    }
    
    
    public boolean contains(CandidateLinkkey c1, CandidateLinkkey c2) {
        return contains(c1.getEqPairs(),c1.getEqPairs(),c1.getClasses1(),c1.getClasses2(),c2);
    }
    
    public boolean isContained(CandidateLinkkey c1, CandidateLinkkey c2) {
        return contains(c2,c2);
    }
    
    public boolean equals(LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2, CandidateLinkkey c) {
        return contains(eqPairs,inPairs,classes1,classes2,c) && contains(c,eqPairs,inPairs,classes1,classes2);
    }
    
    public boolean equals(CandidateLinkkey c1, CandidateLinkkey c2) {
        return contains(c1,c2) && contains(c2,c1);
    }
    
        
}
