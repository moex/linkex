/*
 * Copyright (C) 2014-2020 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.partialorder;

import fr.inrialpes.exmo.linkkey.utils.ClassExpressions;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.longs.LongSet;

import java.util.Set;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class PropertyPairsAndClassExpOrder extends PartialOrder {

    @Override
    public boolean contains(LongSet eqPairsA, LongSet inPairsA, Set<IntSet> classes1A, Set<IntSet> classes2A, 
                            LongSet eqPairsB, LongSet inPairsB, Set<IntSet> classes1B, Set<IntSet> classes2B) {
        if (eqPairsB.isEmpty() && inPairsB.isEmpty()) return true;
        return eqPairsA.containsAll(eqPairsB) && inPairsA.containsAll(inPairsB) &&
                ClassExpressions.contains(classes1B, classes1A) && ClassExpressions.contains(classes2B, classes2A);
    }
    
}
