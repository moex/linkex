/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.normalizers;

import fr.inrialpes.exmo.linkkey.utils.trie.Trie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jdavid
 */
public class WordVector {
    
    
    Map<String,Integer> idx = new Trie<>();
    int maxIdx=Integer.MIN_VALUE;
    
    Map<Integer,Map<Integer,int[]>> coocurrences = new HashMap<>();
    Map<Integer,Integer> occ = new HashMap<>();
    
    
    
    private int getId(String s) {
        Integer id = idx.get(s);
        if (id==null) {
            idx.put(s, id=maxIdx);
            maxIdx++;
        }
        return id;
    }
    
    
    public void addText(String s) {
         int w1idx=0;
         ArrayList<String> res = new ArrayList<String>();
	    int oldIdx=0;
	    for (int i=0 ; i < s.length() ; i++ ) {
		if (!Character.isLetterOrDigit(s.charAt(i))) {//if (Character.isWhitespace(s.charAt(i)) || s.charAt(i)==',') {
		    if (oldIdx<i)
			res.add(s.substring(oldIdx, i));
		    oldIdx=i+1;
		}
	    }
	    if (oldIdx==0) return ;//res.add(s);
	    if (oldIdx+1<s.length())
                res.add((s.substring(oldIdx)));
           
            for (String w1 : res) {
                int w1id = getId(w1);
                Integer i = occ.get(w1id);
                if (i==null) {
                    occ.put(w1id, 1);
                }
                else {
                    occ.put(w1id, i+1);
                }
                Map<Integer,int[]> coocW1 = coocurrences.get(w1id);
                if (coocW1==null) {
                    coocurrences.put(w1id, (coocW1=new HashMap<>()));
                }
                int w2idx=0;
                for (String w2 : res) {
                    if (w1idx!=w2idx) {
                        int w2id = getId(w2);
                        int[] v = coocW1.get(w2id);
                        if (v==null) {
                            coocW1.put(w2id,v = new int[2]);
                        }
                        v[0]=Math.abs(w1idx-w2idx);
                        v[1]++;
                    }
                    w2idx++;
                }
                w1idx++;
            }
    }
    
    
    public void finishIndex() {
        
    }
    
    
}
