/* 
 * Copyright (C) 2014-2017 Université Grenoble Alpes
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey.normalizers;

import org.apache.jena.datatypes.BaseDatatype;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Basic String normalizer that splits a string as a bag of sequences of letters
 * or digits, and sorts this bag. URIs are left unchanged.
 *
 * @author jdavid
 */
public class StringNormalizer extends AbstractNormalizer {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String normalize(Object o) {
        
        if (o instanceof XSDDateTime) {
            //System.err.println("date="+((XSDDateTime)o).asCalendar().toInstant().toString());
            XSDDateTime d = ((XSDDateTime) o);
            // convert xsd:datetime with 0 for time part into simple date
            if (d.getNarrowedDatatype().equals(XSDDatatype.XSDdateTime) && d.getTimePart()==0) {
                return LocalDate.of(d.getYears(),d.getMonths(),d.getDays()).toString();
            }
            //return ((XSDDateTime) o).asCalendar().toInstant().toString();
            return  o.toString();
        }
        else if (o instanceof BaseDatatype.TypedValue) {
            BaseDatatype.TypedValue tv = (BaseDatatype.TypedValue) o;
            if ("http://www.w3.org/1999/02/22-rdf-syntax-ns#HTML".equals(tv.datatypeURI)) {
                o = tv.lexicalValue.replaceAll("(?s)<[^>]+>", " ");
                //System.err.println(o);
            }
            else {
                logger.info("TypedValue :"+((BaseDatatype.TypedValue)o).datatypeURI+" - "+o.getClass()+" - "+ ((BaseDatatype.TypedValue)o).lexicalValue);
                return ((BaseDatatype.TypedValue)o).lexicalValue;
            }
        }
        
        // other types (that are not String)
        if (!(o instanceof String)) {
            //System.err.println("NO STRING : "+o.toString());
            return o.toString();
        }
        
        String s = (String) o;
        
        // for ISBN-10 or ISBN-13
        // regular expression from O'reilly regular-expressions-cookbook
        // ^(?:ISBN(?:-1[03])?:?\ )?(?=[0-9X]{10}$|(?=(?:[0-9]+[-\ ]){3})[-\ 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[-\ ]){4})[-\ 0-9]{17}$)(?:97[89][-\ ]?)?[0-9]{1,5}[-\ ]?[0-9]+[-\ ]?[0-9]+[-\ ]?[0-9X]$
        // be careful to apply it before parsing number because an isbn is parsed as a number.
        //if (s.matches("([0-9]+-)+[0-9]+")) {
        if (s.matches("^(?:ISBN(?:-1[03])?:?\\ )?(?=[0-9X]{10}$|(?=(?:[0-9]+[-\\ ]){3})[-\\ 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[-\\ ]){4})[-\\ 0-9]{17}$)(?:97[89][-\\ ]?)?[0-9]{1,5}[-\\ ]?[0-9]+[-\\ ]?[0-9]+[-\\ ]?[0-9X]$")) {
            //System.err.println("ISBN : "+s);
            return s;
        }
        
        // try if the string is a number
        if (s.matches("^-?(([0-9]+)|([0-9]*\\.[0-9]*))$")) {
            try {
                String res = NumberFormat.getNumberInstance().parse(s).toString();
                return res;
            } catch (ParseException e) {}
        }
        
        /*try {
                String res = NumberFormat.getNumberInstance().parse(s).toString();
                System.err.println("Number missed : "+s);
            } catch (ParseException e) {}
        */
        
        //return s;
        if (s.contains("://") || s.contains("/")) {
            try {
                new URI(s);
                return s;
            } catch (URISyntaxException e) {}
        
        }
        s = Normalizer.normalize(s.toLowerCase(), Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
        ArrayList<String> res = new ArrayList<>();
        int oldIdx = 0;
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isLetterOrDigit(s.charAt(i))) {//if (Character.isWhitespace(s.charAt(i)) || s.charAt(i)==',') {
                if (oldIdx < i) {
                    res.add(s.substring(oldIdx, i));
                }
                oldIdx = i + 1;
            }
        }
        if (oldIdx == 0) {
            return s;
        }
        if (oldIdx < s.length()) {
            res.add((s.substring(oldIdx)));
        }

        if (res.isEmpty()) {
            return s;
        }
        Collections.sort(res);
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = res.iterator();
        sb.append(it.next());
        while (it.hasNext()) {
            sb.append(' ');
            sb.append(it.next());
        }
        return sb.toString();//res.toString();
        /*}
	return s;*/

    }

}
