/*
 * Copyright (C) 2019-2020 Basile Legal & Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey;

import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Basile Legal
 */


public class LazyCandidateLinkkey extends CandidateLinkkey{
    
    long specificLinksAddress;
    RandomAccessFile input;
    
    public LazyCandidateLinkkey(LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2, Long specificLinksAddress, RandomAccessFile in) {
        super(eqPairs, inPairs,classes1,classes2);
        this.specificLinksAddress = specificLinksAddress;
        this.input = in;
    }

    LazyCandidateLinkkey(LongSet eqPairs, LongSet inPairs, LongSet links) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public LongSet getSpecificLinks () {
        LongSet set = new LongOpenHashSet();
        try {
            set = this.readLazyLongSet(input, specificLinksAddress);
        } catch (IOException ex) {
            Logger.getLogger(LazyCandidateLinkkey.class.getName()).log(Level.SEVERE, null, ex);
        }
        return set;
    }
    
    public LongSet readLazyLongSet (RandomAccessFile input, Long position) throws IOException {
        input.seek(position);
        input.readLong();
        int nbElem = input.readInt();
        LongSet set = new LongOpenHashSet(nbElem);
        for (int i=0 ; i<nbElem ; i++) {
            set.add(input.readLong());
        }
        return set;
    }
}
