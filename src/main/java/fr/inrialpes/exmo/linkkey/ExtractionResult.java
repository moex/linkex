/*
 * Copyright (C) 2014-2020 Jerome David <jerome.david@univ-grenoble-alpes.fr>
 *
 * This file is part of Linkex.
 *
 * Linkex is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Linkex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrialpes.exmo.linkkey;

import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import fr.inrialpes.exmo.linkkey.eval.SupervisedEvalMeasures;
import fr.inrialpes.exmo.linkkey.utils.Int2Id;
import fr.inrialpes.exmo.linkkey.utils.IntPair;
import it.unimi.dsi.fastutil.ints.*;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;

import java.io.*;
import java.util.*;

/**
 * Represents the lattice of extracted link key candidates.
 * It also contains the property and uri indexes.
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class ExtractionResult {
    
    private final static long serialVersionUID = 1l;
    
    // index that stores properties from dataset 1
    private String[] pIdxDS1;
    // index that stores properties from dataset 2
    private String[] pIdxDS2;
    // index that stores URIs (subjects, classes, object property values) from both datasets
    private String[] uris;
    
    private Set<String> classes1Uris;
    private Set<String> classes2Uris;
    
    private Int2ObjectMap<IntSet> classes2InstancesDS1;
    private Int2ObjectMap<IntSet> classes2InstancesDS2;
    
    private int class1Size;
    private int class2Size;
    
    private IntSet instances1;
    private IntSet instances2;
    
    private int nbCandidates;
            
            
    private CandidateLinkkey top;
    
    private Collection<CandidateLinkkey> values;
    
    private LongSet referenceLinks;
    
    private EvalMeasures unsupervisedEval;
    private SupervisedEvalMeasures supervisedEval;
    
    private Map<String,String> prefixMap;
    
    private String filename;
    private String fullPath;
    
    public ExtractionResult() { 
        classes1Uris = new HashSet();
        classes2Uris = new HashSet();
    }
    
    
    public ExtractionResult(String[] pIdxDS1,String[] pIdxDS2,
            String[] uris, 
            Set<String> classes1Uris, Set<String> classes2Uris, 
            int class1Size, int class2Size, 
            LongSet referenceLinks, Map<String,String> prefixMap) {
        this(new CandidateLinkkey(),
                pIdxDS1,pIdxDS2,uris, 
                classes1Uris, classes2Uris, 
                class1Size, class2Size,
                Int2ObjectMaps.emptyMap(),Int2ObjectMaps.emptyMap(), 
                referenceLinks, prefixMap);
    }
    
    public ExtractionResult(CandidateLinkkey top, 
            String[] pIdxDS1,String[] pIdxDS2,
            String[] uris, 
            Set<String> classes1Uris, Set<String> classes2Uris, 
            int class1Size, int class2Size,
            Int2ObjectMap<IntSet> classes2InstancesDS1, Int2ObjectMap<IntSet> classes2InstancesDS2,
            LongSet referenceLinks, 
            Map<String,String> prefixMap) {
        this.pIdxDS1=pIdxDS1;
        this.pIdxDS2=pIdxDS2;
        this.uris=uris;
        this.classes1Uris=classes1Uris==null?Collections.emptySet():classes1Uris;
        this.classes2Uris=classes2Uris==null?Collections.emptySet():classes2Uris;
        this.class1Size=class1Size;
        this.class2Size=class2Size;
        this.classes2InstancesDS1=classes2InstancesDS1;
        this.classes2InstancesDS2=classes2InstancesDS2;
        this.referenceLinks=referenceLinks;
        this.prefixMap=prefixMap;
        setTop(top);
        
    }
    
    public ExtractionResult(String[] pIdxDS1,String[] pIdxDS2,String[] uris, Set<String> classes1Uris, Set<String> classes2Uris, IntSet instances1, IntSet instances2, LongSet referenceLinks, Map<String,String> prefixMap) {
        this(pIdxDS1,pIdxDS2,uris,classes1Uris,classes2Uris,instances1.size(),instances2.size(),referenceLinks,prefixMap);
        this.instances1=instances1;
        this.instances2=instances2;
    }
    
    public ExtractionResult(CandidateLinkkey top, String[] pIdxDS1,String[] pIdxDS2,String[] uris, Set<String> classes1Uris, Set<String> classes2Uris, IntSet instances1, IntSet instances2, Int2ObjectMap<IntSet> classes2InstancesDS1, Int2ObjectMap<IntSet> classes2InstancesDS2,LongSet referenceLinks, Map<String,String> prefixMap) {
        this(top,pIdxDS1,pIdxDS2,uris,classes1Uris,classes2Uris,instances1.size(),instances2.size(),classes2InstancesDS1, classes2InstancesDS2,referenceLinks,prefixMap);
        this.instances1=instances1;
        this.instances2=instances2;
    }

    
    private final void setTop(CandidateLinkkey root) {
        this.top=root;
        Set<CandidateLinkkey> res = new HashSet<>();
        res.addAll(top.getDescendants());
        res.add(top);
        values=res;
    }

    public String[] getpIdxDS1() {
        return pIdxDS1;
    }

    public String[] getpIdxDS2() {
        return pIdxDS2;
    }

    public String[] getUris() {
        return uris;
    }

    public Set<String> getClasses1Uris() {
        return classes1Uris;
    }

    public Set<String> getClasses2Uris() {
        return classes2Uris;
    }
    
    
    public Int2ObjectMap<IntSet> getClasses2InstancesDS1() {
        return classes2InstancesDS1;
    }

    public Int2ObjectMap<IntSet> getClasses2InstancesDS2() {
        return classes2InstancesDS2;
    }

    public int getNbCandidates() {
        return nbCandidates;
    }

    public Map<String, String> getPrefixMap() {
        return prefixMap;
    }
    
    public CandidateLinkkey getOrAddCandidate(LongSet eqPairs, LongSet inPairs, Set<IntSet> classes1, Set<IntSet> classes2) {
        if (getTop()==null) {
            top=new CandidateLinkkey();
            values.add(top);
        }
        if (!inPairs.isEmpty() && !eqPairs.isEmpty()) {
            inPairs.removeAll(eqPairs);
        }
        
        Iterator<CandidateLinkkey> it = getTop().getChildren().iterator();
        
        while (it.hasNext()) {
            CandidateLinkkey c = it.next();
            if (eqPairs.containsAll(c.getEqPairs()) && inPairs.containsAll(c.getInPairs())) {
                if (eqPairs.size()==c.getEqPairs().size() && inPairs.size()==c.getInPairs().size()) {
                    return c;
                }
                else {
                    it = c.getChildren().iterator();
                }
            }
        }
        CandidateLinkkey c = new CandidateLinkkey(eqPairs,inPairs, classes1, classes2);
        top.addDescendant(c);
        values.add(c);
        return c;
    }

    public int getClass1Size() {
        return class1Size;
    }

    public int getClass2Size() {
        return class2Size;
    }

    public IntSet getInstances1() {
        return instances1;
    }

    public IntSet getInstances2() {
        return instances2;
    }
    
    public Collection<CandidateLinkkey> getCandidates() {
        return values;
    }
    
    public CandidateLinkkey getTop() {
        if (top==null) {
            if (values.isEmpty()) return null;
            top=values.iterator().next().getTop();
        }
        return top;
    }
    
    public CandidateLinkkey getBottom() {
        return getTop().getBottom();
    }

    public LongSet getReferenceLinks() {
        return referenceLinks;
    }

    public EvalMeasures getUnsupervisedEval() {
        if (unsupervisedEval==null) {
            unsupervisedEval = new EvalMeasures(class1Size,class2Size,classes2InstancesDS1,classes2InstancesDS2);
        }
        return unsupervisedEval;
    } 
    
    public SupervisedEvalMeasures getSupervisedEval() {
        if (referenceLinks!=null && referenceLinks.size()>0 && supervisedEval==null) {
            supervisedEval = new SupervisedEvalMeasures(referenceLinks,classes2InstancesDS1,classes2InstancesDS2);
        }
        return supervisedEval;
    }
    
    
    public void write(DataOutput out) throws IOException {
        Int2Id pDS1Reencode = new Int2Id();
        Int2Id pDS2Reencode = new Int2Id();
        Int2Id uriReencode = new Int2Id();
        
        out.writeInt(class1Size);
        out.writeInt(class2Size);
        out.writeInt(classes1Uris.size());
        for (String c : classes1Uris) out.writeUTF(c);
        out.writeInt(classes2Uris.size());
        for (String c : classes2Uris) out.writeUTF(c);
        
        
        out.writeInt(values.size());

        for (CandidateLinkkey c : values) {
            writeLongSet(out,c.getEqPairs(),pDS1Reencode, pDS2Reencode);
            writeLongSet(out,c.getInPairs(),pDS1Reencode, pDS2Reencode);
            writeSetIntSet(out,c.getClasses1(),uriReencode);
            writeSetIntSet(out,c.getClasses2(),uriReencode);
            writeLongSet(out,c.getSpecificLinks(),uriReencode, uriReencode);
        }
        
        writeLongSet(out,referenceLinks,uriReencode, uriReencode);
        
        writeReEncoded(out,pDS1Reencode.getIndex(),pIdxDS1);
        writeReEncoded(out,pDS2Reencode.getIndex(),pIdxDS2);
        writeReEncoded(out,uriReencode.getIndex(),uris);
        out.writeInt(prefixMap.size());
        for (Map.Entry<String,String> e : prefixMap.entrySet()) {
            out.writeUTF(e.getKey());
            out.writeUTF(e.getValue());
        }
    }
    
    public void writeLazy(RandomAccessFile out) throws IOException {
        Int2Id pDS1Reencode = new Int2Id();
        Int2Id pDS2Reencode = new Int2Id();
        Int2Id uriReencode = new Int2Id();
        
        out.writeInt(class1Size);
        out.writeInt(class2Size);
        out.writeInt(classes1Uris.size());
        for (String c : classes1Uris) out.writeUTF(c);
        out.writeInt(classes2Uris.size());
        for (String c : classes2Uris) out.writeUTF(c);
        
        
        out.writeInt(values.size());
        for (CandidateLinkkey c : values) {
            writeLongSet(out,c.getEqPairs(),pDS1Reencode, pDS2Reencode);
            writeLongSet(out,c.getInPairs(),pDS1Reencode, pDS2Reencode);
            writeSetIntSet(out,c.getClasses1(),uriReencode);
            writeSetIntSet(out,c.getClasses2(),uriReencode);
            out.writeLong(out.getFilePointer());
            writeLongSet(out,c.getSpecificLinks(),uriReencode, uriReencode);
        }
        
        writeLongSet(out,referenceLinks,uriReencode, uriReencode);
        
        writeReEncoded(out,pDS1Reencode.getIndex(),pIdxDS1);
        writeReEncoded(out,pDS2Reencode.getIndex(),pIdxDS2);
        writeReEncoded(out,uriReencode.getIndex(),uris);
        out.writeInt(prefixMap.size());
        for (Map.Entry<String,String> e : prefixMap.entrySet()) {
            out.writeUTF(e.getKey());
            out.writeUTF(e.getValue());
        }
    }
    
    
    private static void writeLongSet(DataOutput out, LongSet s, Int2Id idx1, Int2Id idx2) throws IOException {
        out.writeInt(s.size());
        for (long p :s) {
            out.writeLong(getReEncode(p,idx1, idx2 ));
        }
    }
    
    private static void writeSetIntSet(DataOutput out, Set<IntSet> s, Int2Id idx) throws IOException {
        out.writeInt(s.size());
        for (IntSet is : s) {
            out.writeInt(is.size());
            for (int i : is) {
                out.writeInt(idx.getId(i));
            }
        }
    }
    
    private static void writeReEncoded(DataOutput out, Int2IntMap idx, String[] values) throws IOException {
        //out.writeInt(idx.size());
        String[] newValues = new String[idx.size()];
        
        idx.forEach( (o,n) -> {newValues[n] = values[o];});
        
        out.writeInt(newValues.length);
        for (String v : newValues) {
            out.writeUTF(v);
        }
    }
    
    private static long getReEncode(long l, Int2Id idx1, Int2Id idx2) {
        return IntPair.encode(idx1.getId(IntPair.decodeI1(l)), idx2.getId(IntPair.decodeI2(l)));
    }
    
    public void read(String path) throws IOException {
        //try {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(new File(path)));
            read(input);
            input.close();
        /*} catch (IOException ex) {
            ex.printStackTrace();
            RandomAccessFile input = new RandomAccessFile(path, "rw");
            readLazy(input);
        }*/
        setFullPath(path);
    }
    
    public void readMetaData(String path) throws IOException {
        try {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(new File(path)));
            readMetaData(input);
            input.close();
        } catch (IOException ex) {
            RandomAccessFile input = new RandomAccessFile(path, "rw");
            readMetaData(input);
            input.close();
        }
    }


    public void readMetaData(DataInput in) throws IOException {
        class1Size = in.readInt();
        class2Size = in.readInt();
        classes1Uris = new HashSet<>();
        int nb=in.readInt();
        for (int i=0;i<nb;i++) classes1Uris.add(in.readUTF());
        
        nb=in.readInt();
        for (int i=0;i<nb;i++) classes2Uris.add(in.readUTF());
        nbCandidates = in.readInt();
    }

    
    public void read(DataInput in) throws IOException {

        class1Size = in.readInt();
        class2Size = in.readInt();
        int nb=in.readInt();
        for (int i=0;i<nb;i++) classes1Uris.add(in.readUTF());
        nb=in.readInt();
        for (int i=0;i<nb;i++) classes2Uris.add(in.readUTF());
        nbCandidates = in.readInt();
        
        values = new ArrayList<>(nbCandidates);
        for (int i=0 ; i<nbCandidates ; i++) {
            LongSet eqPairs = readLongSet(in);
            LongSet inPairs = readLongSet(in);
            Set<IntSet> classes1 = readSetIntSet(in);
            Set<IntSet> classes2 = readSetIntSet(in);
            LongSet links = readLongSet(in);
            CandidateLinkkey c = new CandidateLinkkey(eqPairs,inPairs,classes1, classes2, links);
            values.add(c);
        }
        // rebuild the hierarchy - not very efficient
        for (CandidateLinkkey c1 : values) {
            for (CandidateLinkkey c2 : values) {
                c1.addDescendant(c2);
            }
        }
        referenceLinks = readLongSet(in);
        
        pIdxDS1 = readArray(in);
        pIdxDS2= readArray(in);
        uris= readArray(in); 
        
        int nbMaps = in.readInt();
        prefixMap= new HashMap<>();
        for (int i=0 ; i<nbMaps ; i++) {
            prefixMap.put(in.readUTF(), in.readUTF());
        }
    }
    
    public void readLazy(RandomAccessFile in) throws IOException {
        class1Size = in.readInt();
        class2Size = in.readInt();
        int nb=in.readInt();
        for (int i=0;i<nb;i++) classes1Uris.add(in.readUTF());
        nb=in.readInt();
        for (int i=0;i<nb;i++) classes2Uris.add(in.readUTF());
        nbCandidates = in.readInt();
        
        values = new ArrayList<>(nbCandidates);
        for (int i=0 ; i<nbCandidates ; i++) {
            LongSet eqPairs = readLongSet(in);
            LongSet inPairs = readLongSet(in);
            Set<IntSet> classes1 = readSetIntSet(in);
            Set<IntSet> classes2 = readSetIntSet(in);
            Long filepointer = in.readLong();
            LongSet links = readLongSet(in);
            LazyCandidateLinkkey c = new LazyCandidateLinkkey(eqPairs, inPairs, classes1, classes2, filepointer, in);
            values.add(c);
        }
        // rebuild the hierarchy - not very efficient
        for (CandidateLinkkey c1 : values) {
            for (CandidateLinkkey c2 : values) {
                c1.addDescendant(c2);
            }
        }
        referenceLinks = readLongSet(in);
        
        pIdxDS1 = readArray(in);
        pIdxDS2= readArray(in);
        uris= readArray(in); 
        
        int nbMaps = in.readInt();
        prefixMap= new HashMap<>();
        for (int i=0 ; i<nbMaps ; i++) {
            prefixMap.put(in.readUTF(), in.readUTF());
        }
    }
    
    private static String[] readArray(DataInput in) throws IOException {
        String[] res = new String [in.readInt()];
        for (int i=0 ; i<res.length ; i++) {
            res[i]=in.readUTF();
        }
        return res;
    }
    
    private static  LongSet readLongSet(DataInput in) throws IOException {
        int nbElem = in.readInt();
        LongSet s = new LongOpenHashSet(nbElem);
        for (int i=0 ; i<nbElem ; i++) {
            s.add(in.readLong());
        }
        return s;
    }
    
    private static  Set<IntSet> readSetIntSet(DataInput in) throws IOException {
        int nbElem = in.readInt();
        Set<IntSet> s = new HashSet();//nbElem);
        for (; nbElem>0 ; nbElem--) {
            int nb2 = in.readInt();
            IntSet is = new IntOpenHashSet();//nb2);
            for (; nb2>0 ; nb2--) {
                is.add(in.readInt());
            }
            s.add(is);
        }
        return s;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }
}
