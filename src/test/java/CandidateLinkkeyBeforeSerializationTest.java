
import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.ExtractionResult;
import fr.inrialpes.exmo.linkkey.LinkkeyDiscoveryAlgorithm;
import fr.inrialpes.exmo.linkkey.LinkkeyExtraction;
import fr.inrialpes.exmo.linkkey.partialorder.PartialOrder;
import fr.inrialpes.exmo.linkkey.utils.IntPair;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class CandidateLinkkeyBeforeSerializationTest {
    
    String OUTPUT_PATH = "/tmp/res.bin";
    String person1Ontology = "http://www.okkam.org/ontology_person1.owl#Person";
    String person2Ontology = "http://www.okkam.org/ontology_person2.owl#Person";
    String firstDataSetFilePath = "";
    String secondDataSetFilePath = "";
    ExtractionResult result = new ExtractionResult();
    
    String keyPair11 = "http://www.okkam.org/ontology_person1.owl#given_name";
    String keyPair12 = "http://www.okkam.org/ontology_person2.owl#given_name";
    String keyPair21 = "http://www.okkam.org/ontology_person1.owl#surname";
    String keyPair22 = "http://www.okkam.org/ontology_person2.owl#surname";
    String keyPair31 = "http://www.okkam.org/ontology_person1.owl#phone_numer";
    String keyPair32 = "http://www.okkam.org/ontology_person2.owl#phone_numer";
    
    CandidateLinkkey candidate1;
    CandidateLinkkey candidate2;
    CandidateLinkkey candidate3;
    
    public CandidateLinkkeyBeforeSerializationTest() {
    }
    
    @Before
    public void setUp() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File firstDataSetFile = new File(classLoader.getResource("person11.rdf").getFile());
        File secondDataSetFile = new File(classLoader.getResource("person12.rdf").getFile());
        this.firstDataSetFilePath = firstDataSetFile.getAbsolutePath();
        this.secondDataSetFilePath = secondDataSetFile.getAbsolutePath();
        
        String[] extractionCommand = String.format(
            "-c1 %s -c2 %s -f bin -o %s %s %s",
            person1Ontology, person2Ontology, OUTPUT_PATH, firstDataSetFilePath, secondDataSetFilePath
        ).split(" ");
        LinkkeyExtraction extraction = new LinkkeyExtraction();
        Map<String, String> prefixMap = new HashMap();
        extraction.parseArguments(extractionCommand);
        LinkkeyDiscoveryAlgorithm disco = extraction.buildDiscovery(prefixMap);
        result = disco.computeLinkkeyWithFCA(prefixMap);
        
        Integer idPair11 = 999;
        Integer idPair12 = 999;
        Integer idPair21 = 999;
        Integer idPair22 = 999;
        Integer idPair31 = 999;
        Integer idPair32 = 999;
       
        for (int i = 0; i < result.getpIdxDS1().length; i++) {
            if (result.getpIdxDS1()[i].equals(keyPair11)) {
                idPair11 = i;
            }
            if (result.getpIdxDS1()[i].equals(keyPair21)) {
                idPair21 = i;
            }
            if (result.getpIdxDS1()[i].equals(keyPair31)) {
                idPair31 = i;
            }
        }

        for (int i = 0; i < result.getpIdxDS2().length; i++) {
            if (result.getpIdxDS2()[i].equals(keyPair12)) {
                idPair12 = i;
            }
            if (result.getpIdxDS2()[i].equals(keyPair22)) {
                idPair22 = i;
            }
            if (result.getpIdxDS2()[i].equals(keyPair32)) {
                idPair32 = i;
            }
        }
        long pair1 = IntPair.encode(idPair11, idPair12);
        long pair2 = IntPair.encode(idPair21, idPair22);
        long pair3 = IntPair.encode(idPair31, idPair32);
        
        
        LongSet candidate1pairs = new LongOpenHashSet();
        candidate1pairs.add(pair1);
        LongSet candidate2pairs = new LongOpenHashSet();
        candidate2pairs.add(pair1);
        candidate2pairs.add(pair2);
        LongSet candidate3pairs = new LongOpenHashSet();
        candidate3pairs.add(pair1);
        candidate3pairs.add(pair2);
        candidate3pairs.add(pair3);
        PartialOrder order = PartialOrder.PROPERTY_PAIRS;
        for (CandidateLinkkey c: result.getCandidates()) {
            if (order.equals(candidate1pairs, candidate1pairs, Collections.emptySet(), Collections.emptySet(), c)) { //if (c.equals(candidate1pairs, candidate1pairs)) {
                candidate1 = c;
            }
            if (order.equals(candidate2pairs, candidate2pairs, Collections.emptySet(), Collections.emptySet(), c)) { //if (c.equals(candidate2pairs, candidate2pairs)) {
                candidate2 = c;
            }
            if (order.equals(candidate3pairs, candidate3pairs, Collections.emptySet(), Collections.emptySet(), c)) { //if (c.equals(candidate3pairs, candidate3pairs)) {
                candidate3 = c;
            }
        }
    }

    @After
    public void tearDown() {
        Runtime r = Runtime.getRuntime();
        try {
            Process p = r.exec("rm " + OUTPUT_PATH);
        } catch (IOException ex) {
            Logger.getLogger(ExtractionJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Test
    public void testTopCandidate () {
        CandidateLinkkey candidate = result.getTop();
        assertEquals(candidate.getInPairs().size(), 0);
        assertEquals(candidate.getEqPairs().size(), 0);
        assertEquals(candidate.getChildren().size(), 8);
        assertEquals(candidate.getParents().size(), 0);
        assertEquals(candidate.getMaxToBottom(), 7);
        // ??
        assertEquals(candidate.getMinToBottom(), 1);
        assertEquals(candidate.getDescendants().size(), 68);
        assertEquals(candidate.getMinToTop(), 0);
        assertEquals(candidate.getMaxToTop(), 0);
        assertEquals(candidate.getAncestors().size(), 0);
        assertEquals(candidate.getSpecificLinks().size(), 0);
//        assertEquals(candidate.getDescendants(), 0);
        assertEquals(candidate.getSpecificInstances1().size(), 0);
        assertEquals(candidate.getSpecificInstances2().size(), 0);
        assertEquals(candidate.getInstances1().size(), 500);
        assertEquals(candidate.getInstances2().size(), 500);
        assertEquals(candidate.getInstances1Size(), 500);
        assertEquals(candidate.getInstances2Size(), 500);
        assertEquals(candidate.getCounts1().size(), 500);
        assertEquals(candidate.getCounts2().size(), 500);
        assertEquals(candidate.getLinksSize(), 10446);
        assertFalse(candidate.moreSpecificThanAndNotEquals(candidate));
        assertFalse(candidate.moreGeneralThanAndNotEquals(candidate));
        assertTrue(candidate.moreSpecificThan(candidate));
        assertTrue(candidate.moreGeneralThan(candidate));
//        assertFalse(candidate.isContained(eqPairs, inPairs))
//        assertFalse(candidate.contains(eqPairs, inPairs))
        assertTrue(candidate.equals(candidate));
        //assertTrue(candidate.equals(candidate.getEqPairs() ,candidate.getInPairs()));
        
    }
    
    @Test
    public void testBottomCandidate () {
        CandidateLinkkey candidate = result.getBottom();
        
        assertEquals(candidate.getInPairs().size(), 8);
        assertEquals(candidate.getEqPairs().size(), 8);
        assertEquals(candidate.getChildren().size(), 0);
//        assertEquals(candidate.getParents().size(), 3);
        assertEquals(candidate.getMaxToBottom(), 0);
        assertEquals(candidate.getMinToBottom(), 0);
        assertEquals(candidate.getDescendants().size(), 0);
        // ??
        assertEquals(candidate.getMinToTop(), 3);
        assertEquals(candidate.getMaxToTop(), 7);
        assertEquals(candidate.getAncestors().size(), 68);
        assertEquals(candidate.getSpecificLinks().size(), 0);
//        assertEquals(candidate.getDescendants(), 0);
        assertEquals(candidate.getSpecificInstances1().size(), 0);
        assertEquals(candidate.getSpecificInstances2().size(), 0);
        assertEquals(candidate.getInstances1().size(), 0);
        assertEquals(candidate.getInstances2().size(), 0);
        assertEquals(candidate.getInstances1Size(), 0);
        assertEquals(candidate.getInstances2Size(), 0);
        assertEquals(candidate.getCounts1().size(), 0);
        assertEquals(candidate.getCounts2().size(), 0);
        assertEquals(candidate.getLinksSize(), 0);
        assertFalse(candidate.moreSpecificThanAndNotEquals(candidate));
        assertFalse(candidate.moreGeneralThanAndNotEquals(candidate));
        assertTrue(candidate.moreSpecificThan(candidate));
        assertTrue(candidate.moreGeneralThan(candidate));
//        assertFalse(candidate.isContained(eqPairs, inPairs))
//        assertFalse(candidate.contains(eqPairs, inPairs))
        assertTrue(candidate.equals(candidate));
        //assertTrue(candidate.equals(candidate.getEqPairs() ,candidate.getInPairs()));
    }
    
    @Test
    public void testCandidate1 () {
        CandidateLinkkey candidate = candidate1;
        
        assertEquals(candidate.getInPairs().size(), 1);
        assertEquals(candidate.getEqPairs().size(), 1);
        assertEquals(candidate.getSpecificLinks().size(), 929);
        assertEquals(candidate.getSpecificInstances1().size(), 322);
        assertEquals(candidate.getSpecificInstances2().size(), 301);
        assertEquals(candidate.getInstances1().size(), 477);
        assertEquals(candidate.getInstances2().size(), 443);
        assertEquals(candidate.getInstances1Size(), 477);
        assertEquals(candidate.getInstances2Size(), 443);
        assertEquals(candidate.getCounts1().size(), 477);
        assertEquals(candidate.getCounts2().size(), 443);
        assertEquals(candidate.getLinksSize(), 1412);
        assertEquals(candidate.getChildren().size(), 5);
        assertEquals(candidate.getParents().size(), 1);
        assertEquals(candidate.getMaxToBottom(), 6);
        assertEquals(candidate.getMinToBottom(), 1);
        assertEquals(candidate.getDescendants().size(), 32);
        // ??
        assertEquals(candidate.getMinToTop(), 1);
        assertEquals(candidate.getMaxToTop(), 1);
        assertEquals(candidate.getAncestors().size(), 1);
        assertFalse(candidate.moreSpecificThanAndNotEquals(candidate));
        assertFalse(candidate.moreGeneralThanAndNotEquals(candidate));
        assertTrue(candidate.moreSpecificThan(candidate));
        assertTrue(candidate.moreGeneralThan(candidate));
//        assertFalse(candidate.isContained(eqPairs, inPairs))
//        assertFalse(candidate.contains(eqPairs, inPairs))
        assertTrue(candidate.equals(candidate));
        //assertTrue(candidate.equals(candidate.getEqPairs() ,candidate.getInPairs()));
    }
    
    @Test
    public void testCandidate2 () {
        CandidateLinkkey candidate = candidate2;
        
        assertEquals(candidate.getInPairs().size(), 2);
        assertEquals(candidate.getEqPairs().size(), 2);
        assertEquals(candidate.getSpecificLinks().size(), 0);
        assertEquals(candidate.getSpecificInstances1().size(), 0);
        assertEquals(candidate.getSpecificInstances2().size(), 0);
        assertEquals(candidate.getInstances1().size(), 361);
        assertEquals(candidate.getInstances2().size(), 361);
        assertEquals(candidate.getInstances1Size(), 361);
        assertEquals(candidate.getInstances2Size(), 361);
        assertEquals(candidate.getCounts1().size(), 361);
        assertEquals(candidate.getCounts2().size(), 361);
        assertEquals(candidate.getLinksSize(), 361);
        assertEquals(candidate.getChildren().size(), 4);
        assertEquals(candidate.getParents().size(), 2);
        assertEquals(candidate.getMaxToBottom(), 5);
        assertEquals(candidate.getMinToBottom(), 1);
        assertEquals(candidate.getDescendants().size(), 16);
        assertEquals(candidate.getMinToTop(), 2);
        assertEquals(candidate.getMaxToTop(), 2);
        assertEquals(candidate.getAncestors().size(), 3);
        assertFalse(candidate.moreSpecificThanAndNotEquals(candidate));
        assertFalse(candidate.moreGeneralThanAndNotEquals(candidate));
        assertTrue(candidate.moreSpecificThan(candidate));
        assertTrue(candidate.moreGeneralThan(candidate));
//        assertFalse(candidate.isContained(eqPairs, inPairs))
//        assertFalse(candidate.contains(eqPairs, inPairs))
        assertTrue(candidate.equals(candidate));
        //assertTrue(candidate.equals(candidate.getEqPairs() ,candidate.getInPairs()));
    }
    
    @Test
    public void testCandidate3 () {
        CandidateLinkkey candidate = candidate3;
        
        assertEquals(candidate.getInPairs().size(), 3);
        assertEquals(candidate.getEqPairs().size(), 3);
        assertEquals(candidate.getSpecificLinks().size(), 0);
        assertEquals(candidate.getSpecificInstances1().size(), 0);
        assertEquals(candidate.getSpecificInstances2().size(), 0);
        assertEquals(candidate.getInstances1().size(), 313);
        assertEquals(candidate.getInstances2().size(), 313);
        assertEquals(candidate.getInstances1Size(), 313);
        assertEquals(candidate.getInstances2Size(), 313);
        assertEquals(candidate.getCounts1().size(), 313);
        assertEquals(candidate.getCounts2().size(), 313);
        assertEquals(candidate.getLinksSize(), 313);
        assertEquals(candidate.getChildren().size(), 3);
        assertEquals(candidate.getParents().size(), 3);
        assertEquals(candidate.getMaxToBottom(), 4);
        assertEquals(candidate.getMinToBottom(), 1);
        assertEquals(candidate.getDescendants().size(), 8);
        assertEquals(candidate.getMinToTop(), 3);
        assertEquals(candidate.getMaxToTop(), 3);
        assertEquals(candidate.getAncestors().size(), 7);
        assertFalse(candidate.moreSpecificThanAndNotEquals(candidate));
        assertFalse(candidate.moreGeneralThanAndNotEquals(candidate));
        assertTrue(candidate.moreSpecificThan(candidate));
        assertTrue(candidate.moreGeneralThan(candidate));
//        assertFalse(candidate.isContained(eqPairs, inPairs))
//        assertFalse(candidate.contains(eqPairs, inPairs))
        assertTrue(candidate.equals(candidate));
        //assertTrue(candidate.equals(candidate.getEqPairs() ,candidate.getInPairs()));
    }
      
}
