
import fr.inrialpes.exmo.linkkey.CandidateLinkkey;
import fr.inrialpes.exmo.linkkey.ExtractionResult;
import fr.inrialpes.exmo.linkkey.LinkkeyDiscoveryAlgorithm;
import fr.inrialpes.exmo.linkkey.LinkkeyExtraction;
import fr.inrialpes.exmo.linkkey.eval.EvalMeasures;
import fr.inrialpes.exmo.linkkey.utils.LinkkeyDiscComparator;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.RandomAccessFile;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class ExtractionJUnitTest {
    
    private String OUTPUT_PATH = "/tmp/res.bin";
    private String person1OntologyPrefix = "http://www.okkam.org/ontology_person1.owl";
    private String person2OntologyPrefix = "http://www.okkam.org/ontology_person2.owl";
    private String person1OntologyPerson = "http://www.okkam.org/ontology_person1.owl#Person";
    private String person2OntologyPerson = "http://www.okkam.org/ontology_person2.owl#Person";
    private String firstDataSetFilePath = "";
    private String secondDataSetFilePath = "";
    private String[] extractionCommand;
    
    public ExtractionJUnitTest() {}
    
    @Before
    public void setUp() {
        ClassLoader classLoader = getClass().getClassLoader();
        File firstDataSetFile = new File(classLoader.getResource("person11.rdf").getFile());
        File secondDataSetFile = new File(classLoader.getResource("person12.rdf").getFile());
        firstDataSetFilePath = firstDataSetFile.getAbsolutePath();
        secondDataSetFilePath = secondDataSetFile.getAbsolutePath();
        extractionCommand = String.format(
            "-c1 %s -c2 %s -f bin -o %s %s %s",
            person1OntologyPerson, person2OntologyPerson, OUTPUT_PATH, firstDataSetFilePath, secondDataSetFilePath
        ).split(" ");
    }
    
    @After
    public void tearDown() throws IOException {
        Runtime r = Runtime.getRuntime();
        Process p = r.exec("rm " + OUTPUT_PATH);
    }

    @Test
    public void testReadMain() throws IOException {
        extractionCommand = String.format(
            "-c1 %s -c2 %s -f bin -o %s %s %s",
            person1OntologyPerson, person2OntologyPerson, OUTPUT_PATH, firstDataSetFilePath, secondDataSetFilePath
        ).split(" ");
        LinkkeyExtraction.main(extractionCommand);
        ExtractionResult res = new ExtractionResult();
        ObjectInputStream outPutStream = new ObjectInputStream(new FileInputStream(new File(OUTPUT_PATH)));
        res.read(outPutStream);
        
        assertEquals(res.getCandidates().size(), 69);
        assertEquals(res.getUris().length, 1000);
        assertEquals(res.getClasses1Uris().size(), 1);
        assertEquals(res.getClasses1Uris().contains(person1OntologyPerson), true);
        assertEquals(res.getClasses2Uris().size(), 1);
        assertEquals(res.getClasses2Uris().contains(person2OntologyPerson), true);
        assertEquals(res.getPrefixMap().size(), 8);
        assertEquals(res.getPrefixMap().get("person2"), "http://www.okkam.org/ontology_person2.owl#");
        assertEquals(res.getPrefixMap().get("person1"), "http://www.okkam.org/ontology_person1.owl#");
        assertEquals(res.getPrefixMap().get("rdf"), "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        assertEquals(res.getPrefixMap().get("PREFIX"), "http://www.okkam.org/oaie/person2-");
        assertEquals(res.getPrefixMap().get("owl"), "http://www.w3.org/2002/07/owl#");
        assertEquals(res.getPrefixMap().get("owl2xml"), "http://www.w3.org/2006/12/owl2-xml#");
        assertEquals(res.getPrefixMap().get("xsd"), "http://www.w3.org/2001/XMLSchema#");
        assertEquals(res.getPrefixMap().get("rdfs"), "http://www.w3.org/2000/01/rdf-schema#");
        assertEquals(res.getpIdxDS1().length, 6);
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#date_of_birth"));
        assertEquals(res.getpIdxDS2().length, 6);
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#date_of_birth"));
    }
    
    @Test
    public void testReadExtract() throws IOException {
        extractionCommand = String.format(
            "-c1 %s -c2 %s -f bin -o %s %s %s",
            person1OntologyPerson, person2OntologyPerson, OUTPUT_PATH, firstDataSetFilePath, secondDataSetFilePath
        ).split(" ");
        new LinkkeyExtraction().extract(extractionCommand);
        ExtractionResult res = new ExtractionResult();
        ObjectInputStream outPutStream = new ObjectInputStream(new FileInputStream(new File(OUTPUT_PATH)));
        res.read(outPutStream);
        
        assertEquals(res.getCandidates().size(), 69);
        assertEquals(res.getUris().length, 1000);
        assertEquals(res.getClasses1Uris().size(), 1);
        assertEquals(res.getClasses1Uris().contains(person1OntologyPerson), true);
        assertEquals(res.getClasses2Uris().size(), 1);
        assertEquals(res.getClasses2Uris().contains(person2OntologyPerson), true);
        assertEquals(res.getPrefixMap().size(), 8);
        assertEquals(res.getPrefixMap().get("person2"), "http://www.okkam.org/ontology_person2.owl#");
        assertEquals(res.getPrefixMap().get("person1"), "http://www.okkam.org/ontology_person1.owl#");
        assertEquals(res.getPrefixMap().get("rdf"), "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        assertEquals(res.getPrefixMap().get("PREFIX"), "http://www.okkam.org/oaie/person2-");
        assertEquals(res.getPrefixMap().get("owl"), "http://www.w3.org/2002/07/owl#");
        assertEquals(res.getPrefixMap().get("owl2xml"), "http://www.w3.org/2006/12/owl2-xml#");
        assertEquals(res.getPrefixMap().get("xsd"), "http://www.w3.org/2001/XMLSchema#");
        assertEquals(res.getPrefixMap().get("rdfs"), "http://www.w3.org/2000/01/rdf-schema#");
        assertEquals(res.getpIdxDS1().length, 6);
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#date_of_birth"));
        assertEquals(res.getpIdxDS2().length, 6);
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#date_of_birth"));
       
    }
    
        @Test
    public void testLazyReadMain() throws IOException {
        extractionCommand = String.format(
            "-c1 %s -c2 %s -f bin -l -o %s %s %s",
            person1OntologyPerson, person2OntologyPerson, OUTPUT_PATH, firstDataSetFilePath, secondDataSetFilePath
        ).split(" ");
        new LinkkeyExtraction().extract(extractionCommand);
        ExtractionResult res = new ExtractionResult();
        RandomAccessFile file = new RandomAccessFile(OUTPUT_PATH, "rw");
        res.readLazy(file);
        
        assertEquals(res.getCandidates().size(), 69);
        assertEquals(res.getUris().length, 1000);
        assertEquals(res.getClasses1Uris().size(), 1);
        assertEquals(res.getClasses1Uris().contains(person1OntologyPerson), true);
        assertEquals(res.getClasses2Uris().size(), 1);
        assertEquals(res.getClasses2Uris().contains(person2OntologyPerson), true);
        assertEquals(res.getPrefixMap().size(), 8);
        assertEquals(res.getPrefixMap().get("person2"), "http://www.okkam.org/ontology_person2.owl#");
        assertEquals(res.getPrefixMap().get("person1"), "http://www.okkam.org/ontology_person1.owl#");
        assertEquals(res.getPrefixMap().get("rdf"), "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        assertEquals(res.getPrefixMap().get("PREFIX"), "http://www.okkam.org/oaie/person2-");
        assertEquals(res.getPrefixMap().get("owl"), "http://www.w3.org/2002/07/owl#");
        assertEquals(res.getPrefixMap().get("owl2xml"), "http://www.w3.org/2006/12/owl2-xml#");
        assertEquals(res.getPrefixMap().get("xsd"), "http://www.w3.org/2001/XMLSchema#");
        assertEquals(res.getPrefixMap().get("rdfs"), "http://www.w3.org/2000/01/rdf-schema#");
        assertEquals(res.getpIdxDS1().length, 6);
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#date_of_birth"));
        assertEquals(res.getpIdxDS2().length, 6);
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#date_of_birth"));
    }
    
    @Test
    public void testLazyReadExtract() throws IOException {
        extractionCommand = String.format(
            "-c1 %s -c2 %s -f bin -l -o %s %s %s",
            person1OntologyPerson, person2OntologyPerson, OUTPUT_PATH, firstDataSetFilePath, secondDataSetFilePath
        ).split(" ");
        new LinkkeyExtraction().extract(extractionCommand);
        ExtractionResult res = new ExtractionResult();
        RandomAccessFile file = new RandomAccessFile(OUTPUT_PATH, "rw");
        res.readLazy(file);
        
        assertEquals(res.getCandidates().size(), 69);
        assertEquals(res.getUris().length, 1000);
        assertEquals(res.getClasses1Uris().size(), 1);
        assertEquals(res.getClasses1Uris().contains(person1OntologyPerson), true);
        assertEquals(res.getClasses2Uris().size(), 1);
        assertEquals(res.getClasses2Uris().contains(person2OntologyPerson), true);
        assertEquals(res.getPrefixMap().size(), 8);
        assertEquals(res.getPrefixMap().get("person2"), "http://www.okkam.org/ontology_person2.owl#");
        assertEquals(res.getPrefixMap().get("person1"), "http://www.okkam.org/ontology_person1.owl#");
        assertEquals(res.getPrefixMap().get("rdf"), "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        assertEquals(res.getPrefixMap().get("PREFIX"), "http://www.okkam.org/oaie/person2-");
        assertEquals(res.getPrefixMap().get("owl"), "http://www.w3.org/2002/07/owl#");
        assertEquals(res.getPrefixMap().get("owl2xml"), "http://www.w3.org/2006/12/owl2-xml#");
        assertEquals(res.getPrefixMap().get("xsd"), "http://www.w3.org/2001/XMLSchema#");
        assertEquals(res.getPrefixMap().get("rdfs"), "http://www.w3.org/2000/01/rdf-schema#");
        assertEquals(res.getpIdxDS1().length, 6);
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#date_of_birth"));
        assertEquals(res.getpIdxDS2().length, 6);
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#date_of_birth"));
       
    }
    
    @Test
    public void testDiscoveryAlgorythm () throws IOException {
        LinkkeyExtraction extraction = new LinkkeyExtraction();
        Map<String, String> prefixMap = new HashMap();
        extraction.parseArguments(extractionCommand);
        LinkkeyDiscoveryAlgorithm disco = extraction.buildDiscovery(prefixMap);
        
        assertEquals(disco.getPDS1().length, 14);
        assertEquals(disco.getPDS1()[0].startsWith(person1OntologyPrefix), true);
        assertEquals(disco.getPDS2().length, 13);
        assertEquals(disco.getPDS2()[0].startsWith(person2OntologyPrefix), true);
        // why subject null and size 500 ?
        assertEquals(disco.getSubjectsDS1(), null);
        assertEquals(disco.getSubjectsDS2(), null);
        assertEquals(disco.getSubjectsDS1Size(), 500);
        assertEquals(disco.getSubjectsDS2Size(), 500);
        assertEquals(disco.getTypesDS1().size(), 1);
        assertEquals(disco.getTypesDS1().contains(person1OntologyPerson), true);
        assertEquals(disco.getTypesDS2().size(), 1);
        assertEquals(disco.getTypesDS2().contains(person2OntologyPerson), true);
        assertEquals(disco.getUriIdx().size(), 3006);
        assertEquals(disco.getValueIdx().size(), 3568);
    }
    @Test
    public void testPrefixMap () throws IOException {
        LinkkeyExtraction extraction = new LinkkeyExtraction();
        Map<String, String> prefixMap = new HashMap();
        extraction.parseArguments(extractionCommand);
        LinkkeyDiscoveryAlgorithm disco = extraction.buildDiscovery(prefixMap);
        
        assertEquals(prefixMap.size(), 8);
        assertEquals(prefixMap.get("person2"), "http://www.okkam.org/ontology_person2.owl#");
        assertEquals(prefixMap.get("person1"), "http://www.okkam.org/ontology_person1.owl#");
        assertEquals(prefixMap.get("rdf"), "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        assertEquals(prefixMap.get("PREFIX"), "http://www.okkam.org/oaie/person2-");
        assertEquals(prefixMap.get("owl"), "http://www.w3.org/2002/07/owl#");
        assertEquals(prefixMap.get("owl2xml"), "http://www.w3.org/2006/12/owl2-xml#");
        assertEquals(prefixMap.get("xsd"), "http://www.w3.org/2001/XMLSchema#");
        assertEquals(prefixMap.get("rdfs"), "http://www.w3.org/2000/01/rdf-schema#");
    }
    
    @Test
    public void testWriteExtract () throws IOException {
        LinkkeyExtraction extraction = new LinkkeyExtraction();
        Map<String, String> prefixMap = new HashMap();
        extraction.parseArguments(extractionCommand);
        LinkkeyDiscoveryAlgorithm disco = extraction.buildDiscovery(prefixMap);
        ExtractionResult res = disco.computeLinkkeyWithFCA(prefixMap);
        
        assertEquals(res.getCandidates().size(), 69);
        assertEquals(res.getUris().length, 3006);
        assertEquals(res.getClasses1Uris().size(), 1);
        assertEquals(res.getClasses1Uris().contains(person1OntologyPerson), true);
        assertEquals(res.getClasses2Uris().size(), 1);
        assertEquals(res.getClasses2Uris().contains(person2OntologyPerson), true);
        assertEquals(res.getPrefixMap().size(), 8);
        assertEquals(res.getPrefixMap().get("person2"), "http://www.okkam.org/ontology_person2.owl#");
        assertEquals(res.getPrefixMap().get("person1"), "http://www.okkam.org/ontology_person1.owl#");
        assertEquals(res.getPrefixMap().get("rdf"), "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        assertEquals(res.getPrefixMap().get("PREFIX"), "http://www.okkam.org/oaie/person2-");
        assertEquals(res.getPrefixMap().get("owl"), "http://www.w3.org/2002/07/owl#");
        assertEquals(res.getPrefixMap().get("owl2xml"), "http://www.w3.org/2006/12/owl2-xml#");
        assertEquals(res.getPrefixMap().get("xsd"), "http://www.w3.org/2001/XMLSchema#");
        assertEquals(res.getPrefixMap().get("rdfs"), "http://www.w3.org/2000/01/rdf-schema#");
        assertEquals(res.getpIdxDS1().length, 14);
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#has_address"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#date_of_birth"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#given_name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#is_in_state"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#name"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#is_in_suburb"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#postcode"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#house_number"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS1(), "http://www.okkam.org/ontology_person1.owl#street"));
        assertEquals(res.getpIdxDS2().length, 13);
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#state"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#suburb"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#postcode"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#house_number"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#street"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#has_address"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#age"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#phone_numer"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#soc_sec_id"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#date_of_birth"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#surname"));
        assertTrue(ArrayUtils.contains(res.getpIdxDS2(), "http://www.okkam.org/ontology_person2.owl#given_name"));
       
    }
    
    @Test
    public void testt () throws IOException {
        LinkkeyExtraction extraction = new LinkkeyExtraction();
        Map<String, String> prefixMap = new HashMap();
        extraction.parseArguments(extractionCommand);
        LinkkeyDiscoveryAlgorithm disco = extraction.buildDiscovery(prefixMap);
        ExtractionResult res = disco.computeLinkkeyWithFCA(prefixMap);
        EvalMeasures unSupervisedEval = new EvalMeasures(res.getClass1Size(), res.getClass2Size());
        Collection<CandidateLinkkey> rules = new TreeSet(new LinkkeyDiscComparator(unSupervisedEval)).descendingSet();
        rules.addAll(res.getCandidates());
        
        assertEquals(res.getCandidates().size(), 69);
    }
    
}
